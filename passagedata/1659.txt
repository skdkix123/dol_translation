<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<set $oceanbreezejoboffer to 1>>
<<npc Sam>><<person1>>You walk up to the overweight <<if $pronoun is "m">>man<<else>>woman<</if>> behind the counter. <<He>> smiles at you.
"Aren't you a cutie! What will it be?"
<br><br>

You ask if <<he>> has any work for you. <<His>> smile broadens. "As a matter of fact, I need a cute <<if $player.gender_appearance is "m">>waiter.<<else>>waitress.<</if>> The pay is £5 an hour, plus tips. Just a warning though: some customers might try to grope you. Please be patient with them; they don't mean nothing by it! We're always understaffed, so you can work whenever we're open."

<br><br>

<<link [[Take job|Ocean Breeze Take Job]]>><</link>>
<br>
<<link [[Refuse job|Ocean Breeze]]>><<endevent>><</link>>
<br>