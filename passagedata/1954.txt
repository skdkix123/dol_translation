<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">><<pass 5>>
<<person1>><<He>> hands you the order slip. "Just show them that. Thanks."
<br><br>
<<generate2>><<person2>>
You walk quickly to the corner shop.
<<if $weather is "clear">>
Aside from kids buying ice-creams and slushies, it's quiet. People are probably indoors or out at pubs watching the game. The kids leave with their sugar fix.
<<elseif $weather is "rain">>
It's empty. Between the awful weather and the game, everyone's stayed home.
<br>
"Is it raining out?" the shop attendant asks.
<br>
Stood dripping in the doorway, you're not sure if <<hes>> serious.
<br>
<<elseif $weather is "overcast">>
It's empty. People are probably indoors watching the game.
<</if>>
<br>
You present the order slip to the <<person>> behind the counter.
<br><br>

"Okay," <<he>> says. "That's alcohol. I'll need to see some ID."
<br>
<<if $id is 0>>
	"It's not for me. I'm just picking it up."
	<br>
	"Sorry," <<he>> shrugs.
	<br>
	"But... They asked me to pick it up for them."
	<br>
	"Sorry, but..." <<he>> glances around the shop. It's empty now. "Unless you can prove your age some other way."
	<br>
	"Prove it?"
	<br>
	<<if $player.gender_appearance is "f">>
		"Let me check your tits?"
		<br>
		<<if $exhibitionism gte 35>><<link [[Allow|Domus Errand Proof]]>><<set $phase to 1>><</link>><</if>>
		<br>
		<<link [[Refuse|Domus Errand Proof]]>><<set $phase to 0>><</link>>
		<br>
	<<else>>
		"Let me check your dick?"
		<br><br>
		<<if $exhibitionism gte 35>>
			<<if $worn.genitals.type.includes("chastity")>>
			You cannot comply due to your chastity device.
			<<else>>
			<<link [[Allow|Domus Errand Proof]]>><<set $phase to 2>><</link>>
			<</if>>
			<br>
		<</if>>
		<<link [[Refuse|Domus Errand Proof]]>><<set $phase to 0>><</link>>
		<br>
	<</if>>
<<else>>
	You pass over your ID card. <<He>> looks at it for a moment, hands it back and then passes over a large crate of beers.
	<br>
	"Have a nice day."
	<br><br>
	<<link [[Next|Domus Errand End]]>><<set $phase to 1>><</link>>
	<br>
<</if>>