<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>><<generate2>><<generate3>><<generate4>><<generate5>><<generate6>>

Winter shows you to a small room. "You can get changed in here," <<he>> says. <<Hes>> smiling. "Forgive me. I can't wait to see the horse in use." <<He>> leaves to prepare the device.
<br><br>
<<strip>>

You wear the rags, as taut and revealing as you remember. You wonder how you look. You'll soon have an audience.
<<exhibitionism1>>

Winter returns. <<He>> holds something at <<his>> side. "Perfect." <<He>> pauses. "I'd like to propose something. I don't want you to feel pressured." <<He>> holds the object out in front of you. It's a whip. "There are degrees of authenticity. If it's okay with you, I'd like to weigh you down as they would have in the past. And whip you. Not too hard, but enough to impress."
<br><br>

<<link [[Accept|Museum Horse Safe Word]]>><<bind>><<npcincr Winter love 3>><</link>><<gpain>><<garousal>><<ggwillpower>><<gglove>><<ltrauma>>
<br>
<<link [[Refuse|Museum Horse Light]]>><<bind>><<npcincr Winter love 1>><</link>><<glove>>
<br>