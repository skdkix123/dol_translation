<<set $outside to 0>><<set $location to "cafe">><<effects>>

You chat with other members of the cafe's staff. You recognise many, but there are a lot of new faces. Even with Sam mobilising the entire staff for this event, they're kept busy and rarely have time to talk. They don't seem to mind. Sam's joy is infectious.
<br><br>

<<link [[Next|Chef Opening End]]>><</link>>
<br>