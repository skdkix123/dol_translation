<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<if $rng gte 61>>
	You stay behind the curtain, remaining as still and quiet as possible. After thirty minutes, the <<person>> gets up, stretches, and walks to the kitchen. You start creeping out, but notice <<he>> left <<his>> phone lying on the sofa.
	<br><br>
	<<link [[Take it|Domus Street]]>><<crimeup 30>><<endevent>><<set $blackmoney += 30>><</link>><<crime>>
	<br>
	<<link [[Leave|Domus Street]]>><<endevent>><</link>>
	<br>
<<else>>
	You stay behind the curtain, remaining as still and quiet as possible. After thirty minutes, the <<person>> gets up, stretches, and walks to the kitchen. You creep outside.
	<br><br>
	<<link [[Leave|Domus Street]]>><<endevent>><</link>>
	<br>
<</if>>