<<set $outside to 0>><<set $location to "compound">><<effects>>
You successfully pick the lock and enter the building.
<<set $compoundcentre to 1>><<set $compoundalarm += 1>>
<<if $skulduggery lt 400>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>
<br><br>
The inside is dominated by a large lift. You see no way to operate it, but a laptop sits on a table in the corner beside a small cannister.
<br><br>
<<if $compoundspray isnot 1>>
	<<link [[Take them and leave|Elk Compound Interior Take]]>><<set $blackmoney += 100>><<crimeup 100>><<set $spraymax += 1>><<spray 5>><</link>><<crime>><<gspraymax>>
	<br>
<<else>>
	<<link [[Take them and leave|Elk Compound Interior Take]]>><<set $blackmoney += 100>><<crimeup 100>><<spray 1>><</link>><<crime>><<gspray>>
	<br>
<</if>>
<<link [[Leave|Elk Compound]]>><</link>>
<br>