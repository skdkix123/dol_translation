<<effects>>

<<if $mathsstimbeg isnot 1>><<set $mathsstimbeg to 1>>
You drop to your knees and rest your forehead on the ground. "Pl-please," you sob. "Don't hurt me. I don't have any money and I really need it."
<br><br>

The goons shift uncomfortably. "<<pShes>> just a kid," one of them says. "<<pShe>> worth the fuss?"
<br><br>

"Don't let us catch you again," the dealer says. "Or all the crying in the world won't save you." They depart, leaving you alone in the alley.
<br><br>

<<link [[Next|Oxford Street]]>><<endevent>><</link>>
<br>

<<else>>

You drop to your knees and rest your forehead on the ground. "Pl-please," you sob. "Don't hurt me. I don't have any money and I really need it."
<br><br>

The dealer barks laughter. "We're not falling for that," <<he>> says, nodding to <<his>> goons. You're grabbed from behind. Your <<bind>>arms are bound, and you are pushed down the alleyway until you come to a black car with an open boot. They shove you in, and slam it shut behind you.
<br><br>

<<link [[Next|Maths Boot]]>><</link>>
<br>

<</if>>