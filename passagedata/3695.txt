<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You're in a thin, long bedroom. A little natural light enters the room from a high window, beneath which lies the bed itself. Near the door is a weapon rack, carrying metal rods, maces, and whips. A shield rests on the floor beside it.
<br><br>
On the opposite end of the room is a drawn curtain. You hear running water. Then someone starts to sing.
<br><br>
You don't recognise the language, but you recognise the voice. It's Jordan.
<br><br>
<<link [[Peek behind the curtain|Temple Jordan Peek]]>><<trauma -6>><<grace -5>><</link>><<llgrace>><<ltrauma>>
<br>
<<set $skulduggerydifficulty to 300>>
<<link [[Sneak out|Temple Jordan Sneak]]>><</link>><<skulduggerydifficulty>>
<br>