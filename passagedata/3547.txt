<<effects>>

<<generate2>><<generate3>>One of the bouncers, a <<person2>><<personcomma>> taps the <<person1>><<person>> on the shoulder. "Could you take your hands off the <<girl>>, <<if $pronoun is "m">>sir<<else>>madam<</if>>," <<person2>><<he>> says. "<<pShes>> a club employee, and this isn't that sort of establishment."
<br><br>

<<person1>>"Not that sort of establishment?" the <<person>> scoffs. <<He>> releases you though, and wanders down the street.
<br><br>

The bouncers lead you into the safety of the club.
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><</link>>
<br>