<<effects>>
<<person1>>
"We could have taken them," Avery says as you return to the dance floor, now the winners by default. "But this is better."
<br><br>
<<earnFeat "Ballroom Show-off">>
You and Avery continue to dance until the party winds down. Avery bids <<his>> acquaintances farewell, and together you leave the mansion.
<br><br>

<<link [[Next|Avery Party End]]>><</link>>