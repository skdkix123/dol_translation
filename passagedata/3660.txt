<<effects>>

<<generate1>><<person1>>
Stern-faced <<monks>> cluster you and the other victims together. A <<priest>> stands in the middle of the room, wearing robes of pitch black.
<br><br>
"Congratulations my children," <<he>> drawls. "You are pure once more. Thank you for your service." A <<monk>> steps behind you and unties your gag. <<He>> also removes your collar. "A warning," the <<priest>> continues. "Breathe not a word of this to anyone. Not your friends. Not Jordan. Not your parents, should you be so blessed. I'd hate to consider you impure again." The dark-haired $temple_wall_victim whimpers.
<br><br>
<<if $player.virginity.penile isnot true or $player.virginity.vaginal isnot true>>
	You've gained the <span class="blue">Chastity Vow</span> trait.
	<br><br>
<</if>>
<<set $worn.face.type.push("broken")>>
<<faceruined>>
<<set $worn.neck.type.push("broken")>>
<<neckruined>>
<<unset $temple_arcade_phase>>
<<unset $temple_wall_victim>>
<<set $player.virginity.temple to true>>
<br><br>
<<clotheson>>
<<endevent>>

<<pass 20>>
You're led through the tunnel and back to the temple. The others are silent the whole way.
<br><br>

<<link [[Next|Temple]]>><</link>>
<br>