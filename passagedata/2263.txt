<<effects>>

<<if $submissive gte 1150>>
	"W-Would you like to buy anything?" you ask.
<<elseif $submissive lte 850>>
	"Hurry up," you say. "You're getting in the way of my customers."
<<else>>
	"Does anything interest you?" you ask.
<</if>>
<br><br>

The <<person>> stops in front of the <<print setup.plants[$stall_plant].plural>>. "How much for $stall_amount of these?"
<br><br>

<<stall_trust>>
<br><br>

<<stall_sell_actions>>