<<effects>>

You try lick its pussy despite the violent thrusting.

<<if $oralskill gte 400>>
	<<set $livestock_horse to 2>>
	It's difficult, but <span class="green">you manage</span> to get the occasional lick in. It's salty. <<deviancy4>>

	Its whole body shudders in orgasm, and it collapses to the grass. The other horses crowd around. They jostle with each other, hoping for a turn.
	<br><br>

	The satisfied horse lies on the grass, eyes shut though still breathing heavily.
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<<else>>

	Its violent thrusting makes it difficult, <span class="red">and you don't manage</span> to get so much as a lick in before it knocks you on your back.
	<<gpain>><<pain 6>> <<deviancy4>>
	<br><br>
	It continues thrusting into empty air, and neighs in frustration.
	<br><br>

	The gate opens, and several farmhands rush in, drawn by its shout and wielding cattle prods. They surround the agitated horse. It rears onto its hind legs and tries to kick at the closest. One of the others sneaks up on it, and sticks a needle in its rear. Its eyes shut, and its collapses in a heap.
	<br><br>

	The farmhands leave the field, shutting the gate behind them. The other horses crowd around the fallen, as if to make sure it's alright.

	<<set $oralskill += 5>>
	<span class="gold">You feel more confident in your ability to give pleasure with your mouth.</span>
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<</if>>