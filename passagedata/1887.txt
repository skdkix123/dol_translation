<<set $outside to 0>><<set $location to "pub">><<dockeffects>><<effects>>

<<if $physique gte 12000>>

You stand in the middle of one of the fights and manage to hold a sailor and docker away from each other. A couple of the more level heads on both sides watch you and, inspired by your act, intervene themselves. They manage to calm things enough that security can form a line between the dockers and sailors.

<<else>>

You try separate one of the fights, but just get knocked aside. A couple of the more level heads on both sides watch you and, inspired by your act, intervene themselves. They manage to calm things enough that security can form a line between the dockers and sailors.

<</if>>

"Out!" the pub owner shouts, gesturing at you and the other dockers. "You lot first. Out!"
<br><br>

You leave with your laughing colleagues.
<br><br>

<<dockpuboptions>>