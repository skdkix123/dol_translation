<<set $outside to 0>><<set $location to "hospital">><<effects>>

You are inside the hospital foyer. <<if $psych is 1>>You see a sign directing to Harper's office. <</if>><<if $weekday is 6 and $harpervisit isnot 1 and $psych is 1>>You're in time for your appointment.<<else>>You don't have an appointment right now.<</if>>
<br><br>

<<if $stress gte 10000>>
<<passouthospital>>
<<else>>
<<if $exposed gte 1>>
You are hiding under a hospital bed, hoping no one spots you in this state of undress. You see a cart full of sheets not far away that you could use to cover yourself. You could wait until the coast looks clear then make a break for it. Alternatively, you could inch the bed closer to the cart to try to remain concealed from view.
<br><br>

<<link [[Make a break for it (0:05)|Hospital Exhibition]]>><<set $phase to 0>><<pass 5>><</link>>
<br>
<<link [[Slide the bed closer (0:10)|Hospital Exhibition]]>><<set $phase to 1>><<pass 10>><</link>>
<br><br>

<<else>>

<<if $psych is 1 and $harpervisit isnot 1 and $exposed lte 0 and $weekday is 6>>
<<link [[Doctor Harper's office|Doctor Harper's Office]]>><<set $harpervisit to 1>><</link>>
<br>
<</if>>
<<if $vaginalchastityparasite isnot 0>>
<span class="pink">You feel $vaginalchastityparasite squirming inside your vagina.</span>
<br>
<</if>>
<<if $penilechastityparasite is 0 and $vaginalchastityparasite is 0 and $analchastityparasite is 0 and $parasite.clit.name is undefined and $parasite.penis.name is undefined and $parasite.nipples.name is undefined and $parasite.bottom.name is undefined and $parasite.left_ear.name is undefined and $parasite.right_ear.name is undefined>>
<<else>>
<<link [[Inquire about body parasite removal (0:10)|Hospital Parasite]]>><<pass 10>><</link>>
<br>
<</if>>
<<if $breastsize lte 11>>
<<link [[Inquire about breast enlargement (0:10)|Hospital Breast Enlargement]]>><<pass 10>><</link>>
<br>
<</if>>
<<if $breastsize gte 1>>
<<link [[Inquire about breast reduction (0:10)|Hospital Breast Reduction]]>><<pass 10>><</link>>
<br>
<</if>>
<<if $penisexist is 1 and $penissize lt $penissizemax>>
<<link [[Inquire about penis enlargement (0:10)|Hospital Penis Enlargement]]>><<pass 10>><</link>>
<br>
<</if>>
<<if $penisexist is 1 and $penissize gt $penissizemin>>
<<link [[Inquire about penis reduction (0:10)|Hospital Penis Reduction]]>><<pass 10>><</link>>
<br>
<</if>>
<<set _pregnancy to $sexStats.anus.pregnancy>>
<<if _pregnancy.seenDoctor is 1>>
	<<link [[Inquire about movement in your stomach (0:10)|Pregnancy Introduction]]>><<pass 10>><</link>>
	<br>
<</if>>
<<if _pregnancy.seenDoctor is 3 or (_pregnancy.motherStatus is 2 and _pregnancy.seenDoctor gt 3) or $container.portable.value gt 0>>
	<<if $deviancy gte 75>><<set _pregnancy.namesChildren to true>><<else>><<set _pregnancy.namesChildren to false>><</if>>
	<<if _pregnancy.namesChildren is true>>
		<<link [[Inquire about your pregnancy and children (0:10)|Pregnancy Discussion]]>><<pass 10>><</link>>
		<br>
	<<else>>
		<<link [[Inquire about the parasites inside you (0:10)|Pregnancy Discussion]]>><<pass 10>><</link>>
		<br>
	<</if>>
<</if>>
<<for _e to 0; _e lt $bodypart_number; _e++>>
	<<activebodypart>>
	<<if $skin[_active_bodypart].pen is "tattoo">>
		<<link [[Inquire about tattoo removal (0:10)|Hospital Tattoo Removal]]>><<pass 10>><</link>>
		<br>
		<<break>>
	<</if>>
<</for>>

<<link [[Pharmacy (0:10)|Pharmacy]]>><<pass 10>><</link>>
<br>

<<link [[Go outside (0:01)->Hospital front]]>><<pass 1>><</link>>
<br>

<</if>>
<</if>>