<<effects>>

<<if $rng gte 51>>
	You rise to your feet. The dogs almost knock you back down in their eagerness to keep licking, but you manage to remain upright.
	<br><br>

	You walk to the edge of the crowd. At first you're not sure if they'll let you leave, but then they step aside, giving you an avenue through. They even grab the dogs by their collars.
	<br><br>

	The crowd follows you the rest of the way. Your legs move quicker as you reach the park. Some follow you in, but you soon lose them in the maze of tall bushes and trees.
	<br><br>

	<<cream_end>>

	<<link [[Next|Park]]>><<endevent>><</link>>
	<br>
<<else>>
	You try to climb to your feet, <span class="red">but one of the dogs jumps atop you,</span> a different hunger on its mind.
	<br><br>

	The crowd continue to watch. Many look horrified, but others push to the front, eager for a closer look.
	<br><br>

	<<link [[Next|Cream Ex Dog Rape]]>><<set $molestationstart to 1>><</link>>

<</if>>