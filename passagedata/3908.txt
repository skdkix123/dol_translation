<<set $outside to 0>><<set $location to "home">><<effects>>

You sneak from your bedroom and hide around a corner in the hall. You wait. <<He>> doesn't come out. You sneak back to your room and peek through the gap in the door. Kylar isn't there, and the window is wide open.
<br><br>
<<endevent>>
<<link [[Bedroom|Bedroom]]>><</link>>
<br>