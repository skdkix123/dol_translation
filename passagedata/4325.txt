<<effects>>
<<if $trauma gte 4000>>
	Despite a terrible fear that this'll only make things worse, you tell the <<person>> about your situation.
<<else>>
	You tell the <<person>> about your situation.
<</if>>
About how a bully wrote on you, and is coercing you to get it made into a tattoo.
<<if $pain gte 100>>
	You can't stop the tears.
<</if>>
<br><br>
<<He>> listens without speaking, but <<his>> expression becomes serious. <<He>> glances up and out the window, where Whitney and <<his>> friend maintain their vigil.
<br><br>
"Please take a seat," <<he>> says. "I'll sort this out."
<br><br>
<<link [[Next|Bully Tattoo Tell 2]]>><</link>>
<br>