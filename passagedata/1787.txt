<<set $outside to 0>><<set $location to "town">><<water>><<effects>><<set $bus to "danube">>

You slip into the fountain and hide beneath the surface of the water. A <<person>> appears above you and sits on the rim. <<He>> doesn't look down, but you hope <<he>> doesn't plan on staying there long. You don't know how long you could hold your breath.
<<pass 3>>
<br><br>

<<if $swimmingskill gte random(1, 800)>>

You <span class="green">succeed</span> in holding your breath. After a few minutes <<he>> leaves. You come up to the surface and gasp for air. <<He>> left an unopened bottle of wine on the side of the fountain. It looks fancy.
<br><br>

<<link [[Take it|Danube Street]]>><<set $blackmoney += 30>><<crimeup 30>><<endevent>><</link>><<crime>>
<br>
<<link [[Leave|Danube Street]]>><<endevent>><</link>>
<br>

<<else>>

A few minutes pass and <<he>> shows no sign of moving. You <span class="red">fail</span> to hold your breath. You know you can't keep this up for much longer.
<br><br>

<<link [[Dive through the drain|Danube House Dive]]>><<endevent>><</link>>
<br>
<<link [[Come up for air|Danube House Sneak 2]]>><</link>>
<br>

<</if>>