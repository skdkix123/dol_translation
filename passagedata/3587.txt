<<set $outside to 1>><<set $location to "temple">><<temple_effects>><<effects>>
You run right, and find another wall. This one is colder. Something heavy slams behind you. You try to turn, but a hard spike pokes your hip. You try to turn the other way, but only find another spike.
<<ggpain>><<pain 8>>
<br><br>
Afraid of moving and unaware of where you are, you lean into the cold surface. You're trapped.
<br><br>
You hear muffled laughter, then a grinding sound. "I said it was a dead end <<girlcomma>>" the <<person1>><<monk>> says. Arms grasp your shoulders and tug you backward.
<br><br>
<<link [[Next|Temple Garden Rape]]>><<set $molestationstart to 1>><</link>>