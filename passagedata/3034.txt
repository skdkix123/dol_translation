<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>><<set $bus to "boys">>
You are in the boy's toilets.
<br><br>
<<if $stress gte 10000>>
	<<link [[Everything fades to black...|School Passout]]>><</link>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip isnot 1>>
		<<eventsschooltoilets>>
	<<else>>
		<<storeactions "schoolboys">>
		<<link [[Masturbate|School Boy's Toilets Masturbation]]>><<set $masturbationstart to 1>><</link>>
		<br>
		<<if $facesemen gte 1 or $facegoo gte 1 or $skin.left_cheek.pen is "pen" or $skin.right_cheek.pen is "pen" or $skin.forehead.pen is "pen">>
			<<link [[Wash Face (0:05)|School Boy's Wash]]>><<pass 5>><</link>>
			<br>
		<</if>>
		<<if $mouthsemen gte 1 or $mouthgoo gte 1>>
		<<link [[Rinse Mouth (0:05)|School Boy's Rinse]]>><<pass 5>><</link>>
		<br>
		<</if>>
		<<storeon "schoolboys" "check">>
		<<if _store_check isnot 1>>
			<<link [[Leave|Hallways]]>><</link>>
		<<elseif $exposed gte 2 and $exhibitionism gte 55>>
			<<link [[Sneak Out|Hallways Sneak]]>><<set $phase to 2>><</link>><<exhibitionist4>>
		<<elseif $exposed is 1 and $exhibitionism gte 35>>
			<<link [[Sneak Out|Hallways Sneak]]>><<set $phase to 1>><</link>><<exhibitionist3>>
		<<elseif $exposed lte 0>>
			<<link [[Leave|Hallways]]>><</link>>
		<</if>>
	<</if>>
<</if>>
<<set $eventskip to 0>>
<<storecleanup>>