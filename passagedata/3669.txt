<<temple_effects>><<effects>>

The temple is ancient, and filled with secret passages. You know of one.
<br><br>

You enter a nearby hallway. You drop to your knees and search for a few moments, until you find a tiny crack along the wall. You dig your nails into the crack, and open a door, its exterior built to resemble the surrounding stone bricks.
<br><br>

You make sure the door shuts quietly behind you, then crawl up the passageway. You don't have to crawl long before you reach another door. You push it open and drop to the floor.
<br><br>

You're in a small hexagonal room, the edges ringed with steps leading nowhere. A single window decorates each wall, far above. A cushion sits in the middle.
<br><br>

The air is thick with the smell of a sweet incense. You can't see where its being burned.
<br><br>
<<endevent>>
<<link [[Pray|Temple Prayer]]>><<set $temple_sneak to 1>><</link>> | <span class="blue">???</span>
<br>