<<effects>>
You tense as your foot comes down again, the coals now hotter still.
<<pain 4>>
<br><br>
<<if ($willpower / 10) gte (($pain - 100) + random(1, 100))>>
	<span class="green">You manage to keep your cool.</span>
	<<gwillpower>><<willpower 1>>
	<br><br>
	<<link [[Continue|Temple Firewalk 7]]>><</link>><<gpain>>
	<br>
	<<link [[Jump off|Temple Firewalk Jump]]>><</link>>
	<br>
<<else>>
	<span class="red">You feel an incredible heat. Like your feet are on fire.</span> You leap to safety. The <<person2>><<monk>> and initiates applaud your attempt. "So close," you hear one whisper.
	<<ggwillpower>><<willpower 10>>
	<br><br>
	<<link [[Next|Temple Firewalk Jump]]>><</link>>
	<br>
<</if>>