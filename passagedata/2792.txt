<<set $outside to 1>><<set $location to "school">><<schooleffects>><<effects>>

You start checking the lockers, and soon find one that has been left unlocked. You hide inside.
<br><br>

<<if $rng gte 81>>

You peek through the slits and see two groups of students pass right by you. <<generatey1>><<person1>>"I almost forgot," a <<person>> says. "I need to get something from my locker." <<He>> walks back to your hiding place, and reaches to open it.
<br><br>

<<He>> faces <<his>> friends as the locker door swings open, and fails to spot you stood exposed right in front of <<himstop>> "It's just my homework." You duck as <<he>> probes above you, still not looking. <<He>> grabs a folder and pulls away, shutting the door after <<himstop>> You hadn't realised you were holding your breath.
<<gstress>><<garousal>><<stress 6>><<arousal 600>>
<br><br>

Once their voices subside, you gently push the door. It doesn't budge. You push harder. It still doesn't move. <<He>> locked it this time. Though your movement is restricted, you feel around the darkness for something that could help, and realise there's a cool draft around your feet. Investigating further, you find that the back of the locker is missing, and you can feel the brick wall of the school. The lower portion of which is coming loose.
<br><br>
<<endevent>>

	<<if $tentacledisable is "f">>
<<link [[Kick through|Hallways Exposed Tentacles]]>><<set $molestationstart to 1>><</link>>
<br>
	<<else>>
You break away the brickwork, and the draft increases in strength. Knocking away all the loose bricks leaves a hole maybe just big enough for you. You put one leg through, then the other, then slowly wiggle your way further down.
<br><br>

<<link [[Next|Industrial Drain]]>><<set $eventskip to 1>><</link>>
<br>
	<</if>>

<<else>>

You peek through the slits and see two groups of students pass right by you. You wait for their voices to fade.
<br><br>

<<link [[Leave|Hallways]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>

<</if>>