<<set $outside to 0>><<set $location to "arcade">><<effects>>

You've pushed yourself too much.
<br><br>
<<passout>>
<<set $danger to random(1, 10000)>>
<<if $danger gte (9900 - $allure)>>
	<<if $exposed gte 1 or $daystate is "night" or $daystate is "dawn">>
	<<link [[Wake up|Arcade Trash]]>><</link>>
	<<else>>
	<<link [[Wake up|Arcade Crane]]>><</link>>
	<</if>>
<<else>>
<<ambulance>>
<</if>>

<<pass 1 hour>>

<<set $trauma +=10>><<set $stress -= 2000>>