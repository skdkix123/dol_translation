<<effects>>
<<generate2>><<person2>>
You follow Jordan into the garden. You follow past beds of flowers, and between hedge rows, until you reach a wide and barren space. A jovial <<monk>> agitates a coal fire. Jordan whispers in <<his>> ear, and the <<monk>> smiles.
<br><br>

The <<monk>> produces a bell from <<his>> habit. <<He>> rings it twice. Three initiates arrive from different directions. They discard their gardening gloves and grab shovels, before piling more coal onto the fire.
<br><br>

Jordan watches, impassive, as the initiates work. The <<monk>> pokes a pair of bellows into the fire, and pumps. The flames spread.
<br><br>

<<link [[Next|Temple Firewalk]]>><</link>>
<br>