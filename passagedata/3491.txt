<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You call for security. The <<person1>><<person>> releases you and waves them away before they arrive. "Sorry," <<he>> says. <<Hes>> now dressed in everyday clothes. "I thought you'd be up for it. They loved that little routine. Tipped generously," <<he>> reaches into <<his>> pocket and pulls out some cash. "Only fair you share some." <<He>> places it on the counter and leaves without another word.
<br><br>

<<set $tipmod to 0.8>>
<<tipset "serving">>
<<set $tip += 500>>
<<if $drinksservedstat gte 10000>>
<<set $tip += 200>>
<</if>> <<tipreceive>>
<br><br>

<<clotheson>>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>