<<temple_effects>><<effects>>

The presence rushes closer, until it washes over you. It caresses your body and mind, seeping inwards, exploring without barrier. It whispers. You realise you're screaming.
<br><br>

It leaves as fast as it arrives, taking the void with it. You're sat on the cushion in the chamber. Your heart thuds rapidly in your chest, yet you feel more capable.
<br><br>

<<prayerend>>