<<set $outside to 0>><<set $location to "temple">><<effects>>
<<npc River>><<person1>>
<<if $submissive gte 1150>>
	"Those rods look scary," you say. "What are they for?"
<<elseif $submissive lte 850>>
	"You have some nasty looking weapons," you say.
<<else>>
	"I didn't know the <<monks>> carried weapons," you say.
<</if>>
<br><br>
One of the <<monks>> smiles. "I don't know what you mean," <<he>> says. "River scared them with <<his>> rolling pin."
<br><br>
You try to probe as they lead you back to the kitchen, but to no avail.
<br><br>
<<endevent>>
<<link [[Next|Soup Kitchen]]>><</link>>
<br>