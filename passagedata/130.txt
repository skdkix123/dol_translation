<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">>
You take <<his>> arm and <<he>> helps lift you to your feet. "Thank you," you say. "I'll be okay now."
<br><br>
<<He>> glances around. "Here," <<he>> hands you a small cylinder. "Be discreet. It's not legal, and there's only enough for one use."
<<gspraymax>><<set $spraymax to 1>><<spray 5>>
<br><br>
"I need to get going. You be careful."
<br><br>
<<link [[Next|Domus Street]]>><<endevent>><<set $eventskip to 1>><</link>>