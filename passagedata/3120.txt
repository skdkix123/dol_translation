<<effects>>

Their bodies shudder in unison as they reach orgasm.
<br><br>

<<if $enemyanger gte ($enemyangermax / 5) * 3>>
<<person2>>The <<person>> drops to <<his>> knees and leans against the wall, a rapturous look on <<his>> face.<<person1>> The <<person>> recovers quickly however. <<He>> speaks in a mocking tone as <<he>> unties you. "We were going to give you something nice to wear, but I don't see why we should if you're going to be so disrespectful." With that, <<he>> shoves you outside, slamming the door behind you.
<br><br>
<<elseif $enemyanger gte 1>>
<<person2>>The <<person>> drops to <<his>> knees and leans against the wall, a rapturous look on <<his>> face. <<person1>> The <<person>> recovers quickly however. <<He>> throws a couple of towels at you then shoves you out outside, slamming the door behind you.
<br><br>

<<upperwear 3>>
<<lowerwear 3>>
<<else>>
<<person2>>The <<person>> drops to <<his>> knees and leans against the wall, a rapturous look on <<his>> face.<<person1>> The <<person>> recovers quickly however. <<He>> smiles, "We have just the thing." After a brief rummage through an assortment of garments <<he>> produces a sundress. <<He>> cuts your bonds and throws the dress over your head before shoving you through the door.
<br><br>
<<unbind>>

<<upperwear 1>>

<</if>>

<<tearful>> you struggle to your feet. You know you're not out of danger yet.
<br><br>

<<endcombat>>
<<set $stress -= 1000>>
<<commercialquick>>