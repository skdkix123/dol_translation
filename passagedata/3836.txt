<<effects>>

You and Avery return to the main hall. No one mentions the incident, but more than a few guests leer in your direction.
<br><br>
The party winds down, and you and Avery leave the mansion.
<br><br>

<<link [[Next|Avery Party End]]>><</link>>
<br>