<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $submissive gte 1150>>
"Please stop," you say. "You might hurt someone."
<<elseif $submissive lte 850>>
"Stop, cowards," you say. "This isn't fair."
<<else>>
"Stop," you say. "You'll get in trouble."
<</if>>
<br><br>

Whitney's laughter redoubles. "Fucking what?" <<he>> howls, and throws another rock. It collides with a drainpipe, <span class="red">knocking it to the ground.</span>
<br><br>
<<His>> laughter subsides, and <<he>> pauses. "It's time to move on," <<he>> says. "It'll be more fun elsewhere." <<He>> grasps your arm and pulls you from the garden. <<His>> friends fall in behind you. "Where to next?" <<he>> muses. <<His>> gaze fixes on a house with a particularly extravagant display.
<<if $halloween_trick_NPC>>
The one you visited with Robin
<</if>>
<br><br>

<<link [[Next|Whitney Trick 5]]>><<endevent>><</link>>
<br>