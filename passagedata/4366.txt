<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<set $sewersfeeding to 1>>
<<if $breastfeedingdisable is "f" and $pronoun is "f">>
	Morgan loosens <<his>> sash, and exposes <<his>> breast from underneath <<his>> tattered and discoloured gown. "Come, dear. I know you're a bit old for this, but it was always your favourite."
	<br><br>
	<<link [[Feed|Sewers Breastfeed]]>><<npcincr Morgan love 1>><<npcincr Morgan dom 1>><</link>>
	<br>
	<<link [[Refuse|Sewers Breastfeed Refuse]]>><<npcincr Morgan love -1>><<npcincr Morgan dom -1>><</link>>
	<br>
<<else>>
	"You must be hungry dear," Morgan says. "I have just the thing. Tea and scones." <<He>> disappears around a corner for a moment, before emerging with a basket in hand. <<He>> empties it in front of you. Three boiled rats, and a chipped teacup containing a tarry substance. <<if $NPCList[0].penis isnot "none">>A mysterious white fluid floats on the surface.<</if>>
	<br><br>
	<<link [[Eat|Sewers Rats]]>><<trauma 6>><<stress 6>><<npcincr Morgan love 1>><<npcincr Morgan dom 1>><</link>><<gtrauma>><<gstress>>
	<br>
	<<set $skulduggerydifficulty to 200>>
	<<link [[Pretend to eat|Sewers Rats Pretend]]>><<npcincr Morgan dom -1>><</link>><<skulduggerydifficulty>>
	<br>
	<<link [[Refuse|Sewers Spank]]>><<npcincr Morgan love -1>><<npcincr Morgan dom -1>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>