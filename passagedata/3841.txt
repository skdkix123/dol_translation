<<effects>>

<<if $submissive gte 1150>>
	"I-I'm sorry," you say. "But I'd only let Avery down."
<<elseif $submissive lte 850>>
	"No," you say. "I'm not a performing monkey."
<<else>>
	"I'm just dancing for fun," you say. "A competition is too stressful."
<</if>>
<br><br>

The guests sigh their disappointment. "That's a shame," the <<person2>><<person>> says. "You're quite good at this." Avery looks annoyed, but doesn't say anything. You and <<person1>><<he>> continue to dance until the party winds down. Avery bids <<his>> acquaintances farewell, and together you leave the mansion.
<br><br>

<<link [[Next|Avery Party End]]>><</link>>