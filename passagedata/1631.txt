<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<npc Sam>><<person1>>
You look around the room. Sam stares back, looking more excited than you've ever seen <<himstop>>
<br><br>
You talk about Sam, and all the work <<he>> put in to make the cafe what it is. You thank <<him>> for giving the opportunity to develop your talents, and thank the audience for making <<his>> dreams come true.
<br><br>
The audience applaud. Sam starts crying.
<br><br>
You step away from the podium, and the party begins in earnest. What do you want to do?
<br><br>
<<endevent>>
<<chefspeechoptions>>