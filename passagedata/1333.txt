<<set $outside to 1>><<set $location to "town">><<effects>>
You are on the rooftop of a commercial building. You can access the shopping centre from here. Buildings are closely packed, crossing between them should be no problem.
<br><br>
<<if $stress gte 10000>>
	<<passoutalley>>
<<else>>
	<<if $exposed gte 1>>
		<<exhibitionismroof>>
		<br><br>
		<<link [[Climb down the ladder (0:02)->Commercial alleyways]]>><<pass 2>><</link>>
		<br>
		<<link [[Shopping centre (0:02)->Shopping Centre Top]]>><<pass 2>><</link>>
		<br><br>
	<<else>>
		<br><br>
		<<link [[Climb down the ladder (0:02)->Commercial alleyways]]>><<pass 2>><</link>>
		<br>
		<<link [[Shopping centre (0:02)->Shopping Centre Top]]>><<pass 2>><</link>>
		<br><br>
	<</if>>
<</if>>