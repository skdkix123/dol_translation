<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
Jordan gives you a solemn nod. "Please come with me."
<br><br>
<<He>> leads you out of the main hall and into a small room. "There's a ritual to this, nothing complicated. I'll prepare while you undress."
<br><br>

You remove your clothing, wishing it wasn't quite so cold in here. Jordan rubs white powder between <<his>> hands, then kneels in front of you.
<<if ($phase is 1 or $phase is 2) and $worn.genitals.type.includes("chastity") and $worn.genitals.origin is "temple">>
<<He>> holds a strange rod against your $worn.genitals.name, and the device clatters to the floor.
<</if>>
<<if $penisexist is 1>>
	<<He>> holds your penis between both forefingers, and stares at it intently, gently manipulating the length. You try to distract yourself, but you soon feel it hardening under <<his>> scrutiny.
<</if>>
<<if $vaginaexist is 1>>
	<<He>> places <<his>> thumbs against your pussy and gently opens your
	<<if $vaginasemen + $vaginagoo gte 2>>
		<<if $vaginasemen + $vaginagoo gte 6>>
			lips. Cum bursts out, spraying across Jordan's face and running in streams down both <<his>> arms.
			<<He>> falls back in shock, hurriedly wiping cum from <<his>> eyes and mouth.
			<br>
			<<He>> looks at you.
		<<elseif $vaginasemen + $vaginagoo gte 4>>
			lips. A thick stream of lewd fluids gushes out. Some runs down Jordan's arm and quickly disappears up <<his>> sleeve.
			<<He>> jumps back with a yelp, shaking <<his>> arms in alarm.
			<br>
		<<else>>
			lips. Cum runs out in a thin stream down you legs. Jordan withdraws <<his>> hands in surprise.
			<br>
		<</if>>
		"That's a bad sign," <<he>> mutters.
		<br><br>
	<<else>>
		lips, giving <<him>> a clear view inside.
		<br>
	<</if>>
	You don't want to admit it, but your body responds lewdly to being examined like this.
<</if>>
<br><br>
The fondling continues for a few minutes. Finally, <<he>> finishes <<his>> work. <<He>> stands back and looks at you.
<<arousal 1000>>
<<if $phase is 2>>
	<<if $player.virginity.penile is true and $player.virginity.vaginal is true or $player.virginity.temple is true>>

		"You are unblemished, and may it stay that way."
		<<if $worn.genitals.type.includes("chastity") and $worn.genitals.origin is "temple">>
			With one anxious motion, <<he>> returns your $worn.genitals.name to its place, guarding your crotch.
		<</if>>
		<br><br>

		<<He>> turns away to let you dress.
			<<if $grace gte 90>>
				Without looking, <<he>> places an envelope on the shelf beside you. "Your monthly allowance. Your behaviour has been remarkable. <span class="green">You're a credit to our faith, and every one of us could learn from you.</span> Thank you."
				<br><br>
				You open the envelope. There's <span class="gold">£<<print ($grace * 40)>></span> inside.
				<<set $money += ($grace * 4000)>>
			<<elseif $grace gte 50>>
				Without looking, <<he>> places an envelope on the shelf beside you. "Your monthly allowance. <span class="teal">You've been an upstanding member of our faith,</span> and deserve every penny."
				<br><br>
				You open the envelope. There's <span class="gold">£<<print ($grace * 40)>></span> inside.
				<<set $money += ($grace * 4000)>>
			<<elseif $grace gte 1>>
				Without looking, <<he>> places an envelope on the shelf beside you. "Your monthly allowance. <span class="lblue">You've been an upstanding member of our faith.</span> There is room for improvement, but such is true of us all. We should strive to be better each day."
				<br><br>
				You open the envelope. There's <span class="gold">£<<print ($grace * 40)>></span> inside.
				<<set $money += ($grace * 4000)>>
			<<else>>
				<span class="red">"Though pure, your behaviour hasn't been appropriate for a member of our faith,"</span> <<he>> says. "We know you can do better. As such, we've decided to withhold your allowance this month. I hope I can pay you next time."
				<br><br>
			<</if>>

		<br><br>
		<<clotheson>>
		<<endevent>>
		<<link [[Next|Temple]]>><</link>>
	<<else>>
		<<set $templetest to "failed">>
		"I'm sorry, but you are spoiled." <<His>> voice shakes slightly, you're not sure if you imagined it. "You must be purified. I will summon the specialists. Please cooperate with them." <<He>> produces a small bell from <<his>> robes, and rings.
		<br><br>
		<<endevent>>
		<<link [[Next|Temple Arcade Intro]]>><</link>>
		<br>

	<</if>>
<<elseif $phase is 1>>
	<<if $player.virginity.penile is true and $player.virginity.vaginal is true>>
		<<inittemple>>
		"You are unblemished, and may it stay that way. You are now an initiate of the temple. Welcome, <<if $player.gender_appearance is "m">>brother.<<else>>sister.<</if>>"
		<<if $worn.genitals.type.includes("chastity") and $worn.genitals.origin is "temple">>
			With one anxious motion, <<he>> returns your $worn.genitals.name to its place, guarding your crotch.
		<</if>>
		<br><br>
		<<He>> turns away to let you dress. "Initiates are tasked with menial work around the temple proper. Cleaning, gardening, and servicing your elders. In return you have full access to the temple grounds, with the exception of areas behind doors painted with a red cross."
		<br><br>
		"We have some other rules. No stealing. No violence. No sex, <span class="pink">everyone has their chastity checked monthly.</span> No refusing your elders. If someone behaves in an improper manner towards you, tolerate it until you can inform me. Dealing with that is one of my responsibilities."
		<br><br>
		"I'll let the others know you've joined us," <<he>> continues, smiling. "I'm pleased to have you."
		<br><br>
		<<clotheson>>
		<<endevent>>
		<<link [[Next|Temple]]>><</link>>
	<<else>>
		<<set $templetest to "failed">>
		"I'm sorry, but you are spoiled." <<His>> voice shakes slightly, you're not sure if you imagined it. "You can still become an initiate, but you must pass the trial of purity." <<He>> walks out of the room, but stops in the doorway. "I'm sorry you were taken while still so young." <<He>> disappears around the corner.
		<br><br>
		<<link [[Next|Temple]]>><<endevent>><</link>>
	<</if>>

<<else>>
	<<if $player.virginity.penile is true and $player.virginity.vaginal is true>>
		"You are unblemished, and may it stay that way. Please be patient, I'll return shortly with your belt." <<He>> leaves you briefly, and returns with the
		<<if $player.gender isnot "m">>
			metallic device. Without a word <<he>> kneels once more and wraps it round your waist. You hear a loud click, and Jordan steps back to admire <<his>> handiwork.
			<br><br>
			You look down to examine it yourself. It doesn't feel as tight or uncomfortable as you expected, you wonder if it's on right. You try to slip your fingers beneath it, but have no success. You push down on the belt with both hands, but it doesn't give at all.
			<br><br>
			<<genitalswear 1>>
			<<set $worn.genitals.origin to "temple">>

			"Everything is satisfactory," <<he>> says. "If there are any problems, come see me." <<He>> leaves you alone in the room.
			<br><br>
			<<clotheson>>
			<<link [[Next|Temple]]>><<endevent>><</link>>
		<<else>>
			metallic belt and cage.
			<br><br>
			<<He>> asks, "Thanks to your <<genitalscomma 2>> you have two options. You could wear this cage and it would provide protection for just your <<genitals 2>> and be secured at the base of your penis,however, it will be unable to protect you from sodomy."
			<br><br>
			After giving you a chance to examine it, <<he>> continues, "Your other option would be this chastity belt, it will be able to protect your <<genitals 2>> on top of it being able to fit a shield to protect you from sodomy. It's been designed to both prevent rubbing and to hold a flaccid penis gently in place. As much as this is the best way to protect you, others may mistake you for a girl."
			<br><br>

			<<link [[Ask for the chastity cage|Temple Male Chastity Fitting]]>><<genitalswear 2>><</link>>
			<br>
			<<link [[Ask for the chastity belt|Temple Male Chastity Fitting]]>><<genitalswear 1>><</link>>
		<</if>>
	<<else>>
		<<set $templetest to "failed">>
		"I'm sorry, but you are spoiled." <<His>> voice shakes slightly, you're not sure if you imagined it. "We can still fit you with a belt, but you'll need to pay for it." <<He>> walks out of the room, but stops in the doorway. "I'm sorry you were taken while still so young." <<He>> disappears around the corner.
		<br><br>
		<<link [[Next|Temple]]>><<endevent>><</link>>
	<</if>>
<</if>>