<<effects>>

<<if $phase is 0>>
You hand the <<person>> £5. "Thank you," <<he>> says. <<He>> still looks anxious.
<br><br>

<<elseif $phase is 1>>
You hand the <<person>> £25. "Thank you," <<he>> says, smiling.
<br><br>

<<else>>
You hand the <<person>> £100. <<He>> gasps. "Thank you so much!" <<he>> says, hugging you.
<br><br>

<</if>>

<<endevent>>
<<link [[Next|Orphanage]]>><</link>>
<br>