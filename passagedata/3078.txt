<<effects>>

You reach into the seaweed,

<<if $rng gte 51 or ($museumAntiques.antiques.antiquecoppercompass isnot "found" and $museumAntiques.antiques.antiquecoppercompass isnot "talk" and $museumAntiques.antiques.antiquecoppercompass isnot "museum")>>
	and wrap your fingers around something cold and hard. It's a compass. The glass is covered in grime, but beneath the point still turns north. Someone might be interested in this.
	<<set $antiquemoney += 50>><<museumAntiqueStatus "antiquecoppercompass" "found">>
	<br><br>
	<<destinationeventend>>
<<else>>
	but withdraw your fingers at a sharp, pinching pain. A crab hangs by one of its pincers.
	<<gstress>><<gpain>><<pain 6>><<stress 6>>
	<br><br>
	<<destinationeventend>>
<</if>>