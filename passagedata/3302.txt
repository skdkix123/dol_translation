<<set $outside to 1>><<effects>>
<<pass 3>>
<<if $policemolestation lt 1>><<set $policemolestation to 1>><</if>>
<<if $phase is 1>>
	You nod your head in the affirmative. It's a split second decision but you know it's the right one. If you protest, if you challenge, then the officers might arrest you and take you somewhere far less public. Far less safe.
	<br><br>
<</if>>
Before you know it, you are stood with your feet apart and your arms <<if $leftarm isnot "bound" and $rightarm isnot "bound">>out wide<<else>>still bound behind you<</if>> as cold latex gloves pat down every inch of your body. A number of people stop what they are doing to watch you and the officers. You blush, feeling a little humiliated.
<br><br>
The hands quickly sweep your arms, legs and torso<<if $blackmoney gte 100>>, but somehow they completely miss the £<<print $blackmoney>> worth of stolen goods you are carrying<</if>>.
They focus much more attention on checking your chest and the area between your legs. You hear a click from the camera of the <<person3>><<personstop>>
<br><br>
The two police back off - one of them winks.
<br>
<<person2>>"Okay, that's all. I guess you're not who we're looking for," the <<person>> announces. "Sorry for the inconvenience and thank you for your cooperation."
<br>
The officers climb back into their car and drive away.
<br><br>
You quickly tidy yourself up and leave.
<br>
<<garousal>><<arousal 500>>
<br><br>
<<endevent>>
<<destinationeventend>>