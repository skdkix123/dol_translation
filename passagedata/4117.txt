<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 80 and $robinhugbreak gte 1>>
You cling tightly to Robin and nearly tackle <<him>> to the bed as you whimper and cry loudly.
<br><br>

"Wh-what's wrong!?" Robin yells, horrified at your behaviour. "Oh no, something happened again, didn't it?" <<He>> hugs you back, doing <<his>> best to give comfort as you unload everything that happened to you.
<br><br>

"This is really bad!" Robin declares with a serious face. "We need to tell somebody!" Robin has a far-off expression as <<he>> tries to soothe you.
<br><br><br><br>
<<robinhug>>

<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 80>><<set $robinhugbreak to 1>>
You cling tight to Robin and nearly tackle <<him>> to the bed as you whimper and cry loudly.
<br><br>
"Wh-what's wrong!?" Robin yells, horrified at your behaviour. <<He>> wraps <<his>> arms around you, looking like <<he>> is on the verge of a panic attack as <<he>> tries to figure out how to respond to your distress.
<br><br>
"Th-that's awful!" Robin responds with a horrified face. "This… we've got to tell somebody!" <<He>> says, stroking your back as <<he>> gives more soothing promises about how to deal with the problem.
<br><br>
<<robinhug>>

<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 40 and $robinhugbreak gte 1>>
You cling tight to Robin and nearly tackle <<him>> to the bed as you whimper and cry loudly.
<br><br>
"Wh-what's wrong!?" Robin yells, horrified at your behaviour. "Not again! What happened!?" <<He>> hugs you back, doing <<his>> best to comfort you as you unload everything that happened to you.
<br><br>
"This is really bad!" Robin declares, looking serious. "We really do need to tell somebody!" Robin has a far-off expression as <<he>> tries to soothe you.
<br><br>
<<robinhug>>

<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 40 or $robinbaileyhelp gte 1>>
<<set $robinhugbreak to 1>>
You cling tight to Robin and nearly tackle <<him>> to the bed as you whimper and cry loudly.
<br><br>
"Wh-what's wrong!?" Robin yells, horrified at your behaviour. <<He>> wraps <<his>> arms around you and looks uneasily about the room, but <<he>> allows you to continue. <<He>> tries to soothe you as you manage to talk past your tears and unload everything that has happened to you.
<br><br>
"Th-that's awful!" Robin responds with a horrified face. "This… we've got to tell somebody!" <<He>> tries to struggle out of your grip, but you cling tight to <<himstop>> Eventually, <<he>> relents and stays until you finish crying.
<br><br>
<<robinhug>>

<<else>>
<<set $robinhugbreak to 1>>
You cling tight to Robin and nearly tackle <<him>> to the bed as you whimper and cry.
<br><br>
"Wh-what's wrong!?" Robin yells, horrified at your behaviour. You are ashamed to show this kind of face to <<himcomma>> but you have been pushed too far. There is no putting you back together at this point.
<br><br>
Robin struggles out of your grip and stands up. "W-wait here! I'll go get someone to help!" With that, <<he>> runs from the room, leaving you feeling abandoned. You trusted <<him>> to show your vulnerable side, and now <<hes>> probably about to bring Bailey in here to see you in this state.
<br><br>
<<link [[Stay|Robin Hug Break 2]]>><<set $phase to 1>><<trauma 6>><<stress 6>><<npcincr Robin love 1>><</link>><<gtrauma>><<gstress>><<glove>>
<br>
<<link [[Go to your room (0:05)|Robin Hug Break 2]]>><<pass 5>><<set $phase to 2>><<npcincr Robin love -1>><</link>><<llove>>
<br>
<<link [[Run away|Robin Hug Break 2]]>><<set $phase to 0>><<npcincr Robin love -1>><</link>><<llove>>
<br>
<</if>>