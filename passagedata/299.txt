<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You return to the patient's area, taking cover in alcoves whenever frantic security run by. You arrive, and behold a scene of chaos. Cell doors stand open, their occupants loose. Some members of security have already fallen, their uniforms stripped. Several patients assault each one. Their colleagues try to rescue them, but their batons are no match for the patient's numbers and lust. Many of the patients satisfy themselves with the fallen security personnel, but others fuck each other on tables, chairs or just the floor.
<br><br>
You look out the window. The towers are unmanned and the patrols missing as all hands arrive to contain the threat. Doctor Harper enters from a door opposite, surrounded by security. Now's your chance.
<br><br>
You run through the front door unobstructed, and across the grounds. You climb the fence and escape into the forest.
<br><br>
<<endevent>>
<<set $asylumescaped to 1>><<set $asylumgateescape to 1>>
<<link [[Next|Forest]]>><<set $forest to 100>><</link>>
<br>