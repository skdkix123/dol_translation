"Certainly <<if $player.gender_appearance is "m">>sir<<else>>madam<</if>>. We'll have the crate delivered. Fill it as soon as you're ready for repairs. Please bear in mind that it will take a day before we get them back to you.
Please make sure you don't buy too many new clothes once you send them to us, or our driver will have to take them back as a donation."
<br><br>

<<set _value to 0>>
<<for _items range $wardrobe>>
	<<for _i to 0; _i lt _items.length; _i++>>
		<<if _items[_i].outfitSecondary isnot undefined>>
			<<continue>>
		<</if>>
		<<set _value += Math.floor(_items[_i].cost * (1 - (_items[_i].integrity / _items[_i].integrity_max)) * 1.25)>>
	<</for>>
<</for>>
You think it will cost you
<<if _value gt 5000>>
	£<<print ((_value / 100) - 50).toFixed(2)>>
<<else>>
	nothing
<</if>>
currently with the deposit reducing the cost.

<br><br>
<<if $money gte 5000>>
	<<link [[Confirm the repair and pay the £50 deposit|Tailor Monthly Driver]]>>
		<<set $money -= 5000>>
		<<set $tailorMonthlyService to "repair">>
	<</link>>
<<else>>
	Unable to pay the deposit.
<</if>>
<br>
<<link [[Back out of the repair|Tailor Shop]]>><<endevent>><</link>>