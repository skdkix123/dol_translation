<<set $outside to 1>><<effects>>
<<if $submissive gte 1150>>
	"P-please don't," you whisper. "I have money."
<<elseif $submissive lte 850>>
	"I know how this works," you whisper. "Just walk away."
<<else>>
	"I know how this works," you whisper. "Just walk away."
<</if>>
<br><br>
You hold out the money. The <<person1>><<person>> takes it, glances around, then nods at the <<person2>><<personstop>> <<person1>>"Seems you're not who we're looking for after all," <<he>> announces. "Thank you for cooperating." The officers climb back into their car and drive away.
<br><br>
<<endevent>>
<<destinationeventend>>