<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<set $forest to 50>><<set $bus to "lakecampsite">>
<<if $daystate is "night">>
	<<if $weather isnot "rain" and $hour gte 18 and $lakecouple isnot 1>>
		You are at the Campsite. A couple cuddle beside a small fire. There is a tent set up nearby.
	<<elseif $weather isnot "rain" and $hour lte 18>>
		You are at the Campsite. Embers burn beside a tent.
	<<else>>
		You are at the Campsite. An unlit fireplace sits in the centre of the clearing. The rain may have driven off campers.
	<</if>>
<<elseif $daystate is "dawn">>
	You are at the Campsite. It looks like it was used recently.
<<else>>
	You are at the Campsite. An unlit fireplace sits in the centre of the clearing.
<</if>>
<br><br>
<<if $weather isnot "rain" and $hour gte 18 and $exposed gte 1 and $lakecouple isnot 1>>
	<<if $exhibitionism gte 75>>
	You keep low and stay among the trees. You really wish to show your <<lewdness>>, but you restrain yourself from doing so.
	<<else>>
	You keep low and stay among the trees to keep your <<lewdness>> from being seen.
	<</if>>
	<br><br>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte 10000>>
	<<passoutlake>>
<<elseif $foresthunt gte 10>>
	<<foresthunt>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0 and $laketeenspresent isnot 1>>
	<<eventlake>>
<<else>>
	<<lakereturnjourney>>
	<<if $weather isnot "rain" and $hour gte 18 and $exposed lte 0 and $lakecouple isnot 1>>
		<<link [[Approach the couple|Lake Couple]]>><</link>>
		<br>
	<</if>>
	<<link [[North to fishing rock (0:10)|Lake Fishing Rock]]>><<pass 10>><</link>>
	<br><br>
	<<foresticon>><<link [[Forest (0:10)|Forest]]>><<pass 10>><</link>>
	<br>
	<<set $eventskip to 0>>
<</if>>