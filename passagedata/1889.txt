<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>><<fameprostitution 30>>
	Applause fills the pub. The sailors clink and swig their free drinks. "This is the life," the <<person>> sighs.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<tearful>> you climb down from the table. Your colleagues pat your back. "Thanks for taking one for the team," a <<generate1>><<person1>><<person>> says.
	<br><br>
	"One?" a <<generate2>><<person2>><<person>> interjects. "You mean six." The <<person1>><<person>> glares at <<person2>><<himstop>>
	<br><br>
	You leave the pub with your colleagues.
	<br><br>
	<<dockpuboptions>>
<<elseif $enemyhealth lte 0>>
	You shove the <<person>> away from you. <<He>> stumbles into another patron, who punches <<him>> in the jaw. Someone throws a glass. Then, someone throws a table. The pub breaks into a brawl.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	Someone grasps your shoulders and hauls you to your feet. It's one of your colleagues. <<generate1>><<person1>><<He>> looks amused. <<tearful>> you keep low and escape the chaos with your fellow dockers.
	<br><br>
	<<dockpuboptions>>
<<else>>
	The sailors seem reluctant to stop, but your colleagues arrive at your side and help you to your feet.
	<<lcool>><<dockstatus -1>>
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<tearful>> you leave the pub with the other dockers. The sailors heckle you.
	<br><br>
	<<dockpuboptions>>
<</if>>