<<widget "eventsdance">><<nobr>>
	<<set $dangerevent to random(1, 100)>>
	<<if $dancelesson is "barre">>
		<<endevent>><<generate1>><<person1>>Charlie announces that they need to clear some room and asks you and a <<person>> to put away the portable barre. You manage to carry it out of the studio and into a room filled with other equipment. As you put it down however, you hear the door shut behind you. <span class="red">You turn to find the <<person>> advancing on you, lust in <<his>> eyes.</span>
		<br><br>
		<<link [[Next|Dance Studio Molestation]]>><<set $molestationstart to 1>><</link>>
	<<elseif $dancelesson is "duo">>
		You don't think <<he>> has to keep <<his>> hand that close to your butt. Or pull your body that close against <<his>> for that matter. <span class="blue">You can feel <<his>> hardened <<if $NPCList[1].penis is "clothed">>penis<<else>>nipples<</if>> pressing against you.</span>
		<br><br>
		<<link [[Endure|Dance Studio Duo]]>><<set $phase to 0>><</link>><<garousal>>
		<br>
		<<link [[Push away|Dance Studio Duo]]>><<set $phase to 1>><</link>>
		<br>
		<<if $promiscuity gte 15>>
			<<link [[Reciprocate|Dance Studio Duo]]>><<set $phase to 2>><</link>><<promiscuous2>>
			<br>
		<</if>>
	<<elseif $dancelesson is "airbarre">>
		The barre gives way slightly, enough to disrupt your delicate balance. Instinct causes you to twist in an attempt to save yourself from falling. You succeed, but the barre becomes wrapped around your waist.
		<br><br>
		<<if $worn.lower.type.includes("naked")>>
			<<if !$worn.under_lower.type.includes("naked")>>
				You try to struggle back upright, but with no success. Worse, you start to slip despite the barre coiled tightly around your body. You fall onto your back with a thud.
				<<if $uncomfortable.nude is false>>
				As you regain your bearings, you see your $worn.under_lower.name suspended in the air above you. You take your time and wear a confident smile as you retrieve them, despite the whispers and giggling all around you.
				<<garousal>>
				<<else>>
				As you regain your bearings, you see your $worn.under_lower.name suspended in the air above you. Flustered at your <<genitals>> being exposed in the middle of class like this, you hasten to recover your $worn.under_lower.name as quickly as you can, conscious of the whispers and giggling all around you.
				<<gtrauma>><<gstress>><<garousal>>
				<</if>>
			<<else>>
				<<if $uncomfortable.nude is false>>
				You try to struggle back upright, but with no success. Worse, you start to slip despite the barre coiled tightly around your body. You fall onto your back with a thud. Your legs spread apart, giving the whole room a clear view of your <<genitalsstop>> You feign horror, unbothered by the whispers and giggling all around you.
				<<garousal>>
				<<else>>
				You try to struggle back upright, but with no success. Worse, you start to slip despite the barre coiled tightly around your body. You fall onto your back with a thud. Your legs spread apart, giving the whole room a clear view of your <<undies>> and causing your face to flush red with embarrassment. You hasten to cover up as quickly as you can, conscious of the whispers and giggling all around you.
				<<gtrauma>><<gstress>><<garousal>>
				<</if>>
			<</if>>
		<<elseif $danceskill gte 600>>
			You skilfully shift your weight and ride the momentum back into an upright position. That was close!
		<<else>>

			<<if $uncomfortable.underwear is false>>
			You try to struggle back upright, but with no success. Worse, you start to slip despite the barre coiled tightly around your body. You fall onto your back with a thud. As you regain your bearings, you see your <<bottoms>> suspended in the air above you. You take your time and wear a confident smile as you retrieve them, despite the whispers and giggling all around you. You wouldn't mind showing off even more.
			<<else>>
			You try to struggle back upright, but with no success. Worse, you start to slip despite the barre coiled tightly around your body. You fall onto your back with a thud. As you regain your bearings, you see your <<bottoms>> suspended in the air above you. Flustered at your <<undies>> being exposed in the middle of class like this, you hasten to recover your <<bottoms>> as soon as you can, conscious of the whispers and giggling all around you.
			<<gtrauma>><<gstress>><<garousal>>
			<</if>>

		<</if>>
		<br><br>
		<<link [[Next|Dancing Lesson]]>><<endevent>><</link>>
	<<elseif $dancelesson is "instructor">>
		<<He>> presses <<his>> body against your back and holds your hands in <<his>> own so that <<he>> can more directly demonstrate to you. Despite <<him>> making no overtly sexual moves, you can't help but feel a little turned on with <<his>> toned body so intimately close to yours.
		<<garousal>><<arousal 300>>
		<br><br>
		<<link [[Meek Compliment|Dance Studio Instructor]]>><</link>>
		<br>
		<<link [[Confident Brag|Dance Studio Instructor]]>><<set $phase to 1>><</link>>
		<br>
		<<if $promiscuity gte 15>>
			<<link [[Flirt|Dance Studio Instructor]]>><<set $phase to 2>><</link>><<promiscuous2>>
			<br>
		<</if>>
	<<else>>
		The receptionist peeks her head through the door and looks at Charlie, who seems to understand her meaning. "I need to take five." <<he>> announces. "Keep practising, I won't be long."
		<br><br>
		<<endevent>>
		<<if $dancestudioanger gte 10>>
			<<set $dancestudioanger -= 10>>
			<<generate1>><<generate2>><<generate3>><<generate4>><<generate5>><<generate6>><<person1>>The sound of the door shutting is more ominous than usual. You soon realise why, the rest of the room is all but silent. Everyone else is looking at you. A <<person>> speaks <span class="red">"We've had enough of your shit. Time for a real lesson."</span> They rush toward you.
			<br><br>
			<<link [[Next|Dance Studio Strip]]>><<set $molestationstart to 1>><<set $timer to 10>><</link>>
			<br>
		<<else>>
			How do you want to conduct yourself while Charlie is away?
			<br><br>
			<<link [[Focus on practising|Dancing Lesson No Instructor]]>><</link>>
			<br>
			<<link [[Help other students|Dancing Lesson No Instructor]]>><<set $phase to 1>><</link>>
			<br>
			<<if $trauma gte 1 and $danceskill gte 200>>
				<<link [[Showboat your skills in front of less experienced students|Dancing Lesson No Instructor]]>><<set $phase to 2>><</link>><<lstress>>
				<br>
			<</if>>
		<</if>>
	<</if>>
<</nobr>><</widget>>