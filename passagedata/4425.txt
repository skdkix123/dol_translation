<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewerslake">>
<<set $sewersevent -= 1>>
You are in a cavern near the old sewers, beside a pool of water. The pool stretches into darkness.
<br><br>
<<if $sewersantiquecrystal isnot 1>>
	A rough crystal lies washed up on the shore.
	<br><br>
<</if>>
<<if $sewersspray isnot 1>>
	A strange cylinder lies on the shore, clutched by a glove.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
	<</if>>
	<<if $sewersspray isnot 1>>
		<<link [[Take the cylinder|Sewers Lake Cylinder]]>><<set $sewersspray to 1>><<set $spraymax += 1>><<spray 5>><</link>><<gspraymax>>
		<br>
	<</if>>
	<<if $sewersantiquecrystal isnot 1>>
		<<link [[Take the crystal|Sewers Lake]]>><<arousal 600>><<set $sewersantiquecrystal to 1>><<set $antiquemoney += 200>><<museumAntiqueStatus "antiquecrystal" "found">><</link>><<garousal>>
		<br><br>
	<</if>>
	<<link [[Wet tunnel (0:05)|Sewers Waterfall]]>><<pass 5>><</link>>
	<br><br>
<</if>>
<<set $eventskip to 0>>