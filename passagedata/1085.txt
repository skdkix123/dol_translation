<<effects>>

Despite the danger presented by his violent thrusting, you lean forward and try to get a taste.

<<if $oralskill gte 400>>
	<<set $livestock_horse to 2>>
	<span class="green">You manage</span> to get the occasional lick in. It's salty. <<deviancy4>>

	His enormous body shudders in orgasm, and he falls to the grass. The other centaur crowd around, each wanting a turn.
	<br><br>
	"Do me next."
	<br>
	"Fuck off, I need this more."
	<br>
	"Wish I could lick my own pussy."
	<br><br>

	The satisfied centaur lies on the grass, eyes shut though still breathing heavily.
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<<else>>

	However, <span class="red">you don't manage</span> to get a single lick in before you're shoved to the grass.
	<<gpain>><<pain 6>> <<deviancy4>>
	<br><br>
	The centaur thrusts into empty air. He shouts in frustration and tries to reach beneath him. He can't reach that far back.
	<br><br>

	The gate opens, and several farmhands rush in, drawn by his shout and wielding cattle prods. They surround the agitated centaur. "No!" he shouts, rearing and kicking at the closest. "I need it-" he cuts off as one of the surrounding farmhands sticks a needle in his rear. His eyes shut, and he collapses in a heap.
	<br><br>

	The farmhands leave the field, shutting the gate behind them. The other centaur look amused by the ordeal, but still crowd around to make sure he's alright.

	<<set $oralskill += 5>>
	<span class="gold">You feel more confident in your ability to give pleasure with your mouth.</span>
	<br><br>

	<<link [[Next|Livestock Field]]>><</link>>
	<br>

<</if>>