<<effects>>

It's dark, with only a little natural light entering through a small window. You can make out a decaying cabinet. Perhaps there are clothes inside.
<br><br>

You open the doors, and cough at the scatter of dust. Something heavy thuds to the ground. As the dust clears you make out a metallic glint. It's a grenade.
<br><br>

<<set $antiquemoney += 80>><<museumAntiqueStatus "antiquegrenade" "found">>

You almost panic, but the pin is still in. It also looks old. So old that it can't still be live, surely. A collector would be interested in this.
<br><br>

<<link [[Next|Riding School Lesson Building 2]]>><</link>>
<br>