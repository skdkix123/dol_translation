<<effects>>

<<if $finish is 1>>

The <<person1>><<person>> steps away from you. <<endevent>><<npc Briar>><<person1>>"Why are you stopping?" Briar storms onto the stage. <<He>> glares at you, then seems to remember where <<he>> is. <<He>> smiles at the audience. "Apologies my friends, but that will be all for today." <<He>> ushers you off the stage.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<person1>>"Unprofessional," <<he>> says once you're off the stage and out of sight. "Don't expect payment."
<<npcincr Briar love -1>>
<br><br>

<<elseif $enemyarousal gte $enemyarousalmax>>

<<ejaculation>>

	<<if $phase is 0>>
	<<person1>>The <<person>> waves at the audience, then jumps from the stage. The audience cheer and applaud.
	<<else>>
	<<person1>>"M-more," you whimper. They leave you lying on the stage. The audience cheer and applaud as you stand and bow.
	<</if>>
	<br><br>

"Thank you to the fine <<if $pronoun is "m">>gentleman<<else>>lady<</if>> who joined me today," you say. "I hope you enjoyed your time here." You walk off the stage.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<person1>>Briar's waiting for you. "Nice work," <<he>> says.

	<<if $phase is 0>>
	<<else>>
	"Try to pick the right person next time, darling," <<he>> says. "The lights are obnoxious but you should be able to see."
	<</if>>
<<He>> hands you the £600 <<he>> promised.
<<set $money += 60000>><<npcincr Briar love 1>>
<br><br>

<<elseif $enemyhealth lte 0>>

You knock the <<person>> away from you and escape from the stage.
<br><br>

<<clotheson>>
<<endcombat>>

<<npc Briar>><<person1>>Briar catches you as you pass. "Not so quick," <<he>> says. "Listen." You realise the audience are cheering. "That was unexpected, but they enjoyed it." <<He>> hands you the £600 <<he>> promised.
<<set $money += 60000>><<npcincr Briar love 1>>
<br><br>

<</if>>
<<endevent>>
You return to the dressing room.
<br><br>

<<link [[Next|Brothel Dressing Room]]>><</link>>