<<set $outside to 0>><<set $location to "museum">><<effects>>

<<npc Winter>><<person1>>Winter holds the silver ring up to the light and examines it. "Common jewellery," <<he>> says. "And of enduring metal. There's a lot of these around. This one was worn by someone who lived here. There's value there, I think. Thanks for bringing it."
<br><br>

<<link [[Next|Museum]]>><<endevent>><</link>>