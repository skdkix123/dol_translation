<<set $outside to 1>><<set $location to "beach">><<effects>>
<<endevent>><<npc Robin>><<person1>>
"I'll pay," you say. Robin shakes <<his>> head, but can't seem to form words. You gently push <<him>> aside and hand your money to the police.
<br><br>
Once they're gone, Robin bursts into tears and buries <<his>> head in your shoulder. "I-I," <<he>> sobs. "I was so scared. Thank you." <<He>> calms down a bit <<he>> puts the money back beneath the stand.
<br><br>
<<endevent>>
<<link [[Offer help (0:30)|Robin's Lemonade Help]]>><<npcincr Robin love 1>><<pass 30>><</link>><<glove>>
<br>
<<link [[Leave|Beach]]>><</link>>
<br>