<<effects>>

<<if $athletics gte 800>>
	You run after Alex, <span class="green">and manage to catch up</span> halfway through the field. <<He>> speeds up when <<he>> sees you, but so do you. You're level with each other right up to the forest's edge.
	<br><br>
	
	With the victor unclear, you both keep running, deeper into the forest. Until Alex lags behind just a little, then a little more. <<He>> comes to a stop.<<npcincr Alex dom -1>><<ldom>>
	<br><br>
	
	"Fuck," <<he>> says, leaning against a tree and panting. "I'm faster than all my siblings. Good jo-" <<Hes>> interrupted by <span class="red">a growl</span> from a nearby bush, and pushes <<himself>> away from the tree.
	
	
	
<<else>>
	You run after Alex, <span class="red">but <<hes>> too far ahead,</span> and you can't catch up. <<He>> disappears into the forest.
	<br><br>
	
	You find <<him>> resting against a tree with the brim of <<his>> hat pulled over <<his>> face.
	<br><br>
	<<He>> pushes it up. "What took you?" <<he>> asks. "Almost thought you weren't-" <<Hes>> interrupted by <span class="red">a growl</span> from a nearby bush, and leaps to <<his>> feet.
<</if>>

<<farm_forest_event>>