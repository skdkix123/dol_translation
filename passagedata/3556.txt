<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	The <<person>> falls on top of you, panting. "I bet those <<monks>> would love a piece of you too," <<he>> says. "Thanks babe."
	<br><br>
	Satisfied, the <<person>> leaves the room. <<He>> pokes <<his>> head around the corner first as a precaution. <<tearful>> you follow suit.
	<br><br>
<<elseif $enemyhealth lte 0>>
	You shove the <<person>> onto another sack. This one contains harder material if <<his>> shout of pain is anything to go by. <<tearful>> you dash from the room and back up the stairs.
	<br><br>
<<elseif $rescue is 1 and $alarm is 1>>
	You hear steps thunder down the stairs outside. The <<person>> tries to flee, but gets <<his>> legs tangled in the sack. <<He>> tumbles to the floor.
	<br><br>
	<<generate2>><<person2>>A <<monk>> runs into the room, takes one look at the scene, and grasps the <<person1>><<persons>> arm. "Time for discipline," <<person2>><<he>> growls. <<He>> glares at you. "Don't think you're getting away with it either. I've got my eye on you." <<He>> hauls the <<person1>><<person>> from the room.
	<<lgrace>><<grace -1>>
	<br><br>
	<<tearful>> you climb back up the stairs.
	<br><br>
<<else>>
	The <<person>> backs away from you, looking dejected. <<He>> leaves the room, poking <<his>> head around the corner first as a precaution. <<tearful>> you follow suit.
	<br><br>
<</if>>
<<clotheson>>
<<endcombat>>
<<link [[Next|Temple]]>><</link>>