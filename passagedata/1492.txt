<<set $outside to 1>><<set $location to "brothel">><<effects>>
You're sat down on a bench, and hear the engine start. The hood is pulled off you.<<npc Briar>><<generate2>><<generate3>>
<<if $brothelintro is 1>>
	On the seat opposite sits Briar, owner of the brothel. <<person1>><<Hes>> flanked by a <<person2>><<person>> and <<person3>><<personstop>><<person1>>
	<br><br>
<<else>>
	<<set $brothelintro to 1>>
	<<if $pronoun is "m">>
		On the seat opposite sits a man wearing a blue suit, minus a shirt, showing off his well-toned chest and abdomen. His long chestnut hair hangs loose.
	<<else>>
		On the seat opposite sits a woman wearing an expensive-looking red gown with a plunging neckline, revealing a portion of her pert breasts. Her long chestnut hair hangs loose.
	<</if>>
	<br><br>
	<<Hes>> flanked by a <<person2>><<person>> and <<person3>><<personstop>><<person1>> <<He>> stares at you with hazel eyes.
	<br><br>
	"So you're the waif that's been causing me problems. I'm Briar, owner of the finest establishment in town." <<He>> leans back on the bench.
<</if>>
"I hear you've been selling yourself in the pub. This is a problem. You see, I own all of the whores on this side of town. And what has your little ass been up to? Whoring, of course. That means I own you. And yet, I haven't received a penny. That changes now."
<br><br>
The van halts and the <<person2>><<person>> and <<person3>><<person>> haul you out. They lead you to a staircase beside a building, descending underground. If you want to escape, now's your only chance. There are three of them however.<<person1>>
<br><br>
<<link [[Go with them|Brothel Punishment 2]]>><</link>>
<br>
<<link [[Fight|Brothel Punishment Fight]]>><<set $fightstart to 1>><<set $submissive -= 10>><</link>>
<br>