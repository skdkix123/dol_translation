<<set $location to "forest">><<set $outside to 1>><<effects>>

You shake your head. The <<person>> smiles. "You'll like it," <<he>> says. "I promise. All the troubles of the world will flow away." <<He>> holds the pill closer.
<br><br>
<<link [[Eat the Pill|Pub White Pill]]>><</link>>
<br>
<<link [[Refuse again|Pub Pill Refuse]]>><</link>>
<br>