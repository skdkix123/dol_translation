<<effects>>

<<if $athletics gte random(1, 1000)>>
	Knowing you have little time to decide, you run out of your hiding place towards a nearby alleyway. <span class="green">You make it.</span> You just hope no one spotted you.
	<br><br>

	<<destinationeventend>>
<<else>>
	Knowing you have little time to decide, you run out of your hiding place towards a nearby alleyway. <span class="red">You trip,</span> and fall to your knees with your <<bottom>> stuck in the air. Between your legs you see the bus pull up. Its occupants point in your direction, looking excited and reaching for their phones.
	<<fameexhibitionism 20>>
	<br><br>

	You jump to your feet. <<covered>> You continue to the alleyway, and can only hope no one caught you on film.
	<br><br>

	<<destinationeventend>>
<</if>>