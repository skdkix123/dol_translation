<<effects>>

<<set $seductiondifficulty to 4000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>>
	<span class="gold">You feel more confident in your powers of seduction.</span>
	<br><br>
<</if>>
<<seductionskilluse>>

You glance over your shoulder to make sure there's no one around, then lean against the fence in a mating posture. That should get <<farm_his horse>> attention.<<deviancy2>>

<<if $seductionrating gte $seductionrequired>>

	The <<farm_text horse>> slows when <<farm_he horse>> sees you, considers a moment, then <span class="green">runs in your direction.</span>
	<br><br>
	
	<<link [[Next|Farm Horses Seduce Success]]>><</link>>
	<br>

<<else>>

	The <<farm_text horse>> slows when <<farm_he horse>> sees you, considers a moment, then <span class="red">picks up the pace once more.</span>
	<br><br>

	<<link [[Next|Farm Horses Seduce Failure]]>><</link>>
	<br>

<</if>>