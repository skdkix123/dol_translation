<<widget "eventsswimming">><<nobr>>

<<if $rng gte 91 and $leftarm is "bound" and $rightarm is "bound">>
	<<npc Mason>><<person1>>
	Mason motions for you to come closer. You <<if $feetuse is "bound">>butterfly <</if>>kick your way to the side of the pool. "Why aren't you using your arms?" <<He>> notices your bindings. "Oh! Can you even get those off on your own? I think I have some scissors."
	<br><br>

	<<link [[Be freed|Events Swimming Freed]]>><<unbind>><</link>>
	<br>
	<<link [[Refuse|Events Swimming Freed Refuse]]>><</link>>
	<br>

<<elseif $rng gte 81>>

	<<generates1>><<generates2>>A fight breaks out on the other side of the pool. As Mason swims over to break it up, a <<person1>><<person>> and <<person2>><<person>> swim up to you, and trap you between them. The <<person>> starts fondling your butt from behind while the <<person1>><<person>> speaks. "You look lonely, <<bitchstop>> Don't worry, we're here to keep you company."
	<br><br>

	<<link [[Shove them away|Swimming Lesson Shove]]>><</link>>
	<br>

	<<if $cool lt 80>>
		<<link [[Endure it|Events Swimming Molestation]]>><<set $molestationstart to 1>><</link>><<gtrauma>><<gstress>><<garousal>>
		<br>
	<<else>>
		<<link [[Endure it|Events Swimming Endure]]>><<trauma 2>><<stress 2>><<arousal 200>><</link>><<gtrauma>><<gstress>><<garousal>>
		<br>
	<</if>>

<<elseif $rng gte 71>>

	<<set $rng to random(1, 100)>>
	<<npc Mason>><<generates2>><<person2>>
	<br><br>
	Mason points at a <<person>> near you. "You. Climb out, I want to see your diving technique."
	<br><br>

	<<if $rng % 5 == 0 and $swimnudecounter gte 3>>/*1 in 5 chance*/
		The <<person>> swims to the edge and climbs out. <<He>> goes to the diving board. A number of people stop to watch.
		<br><br>

		<<link [[Next|Events Swimming NPC Dive]]>><<set $phase to 3>><</link>>
		<br>

	<<elseif $rng % 5 == 0>>
		The <<person>> swims to the edge and whispers something to Mason. Mason flushes, says something back, and leaves. The <<person>> carries on swimming. A few moments later you hear a loud whistle. Mason calls everyone to the edge.
		<br><br>

		"Class! Everyone here! it's time for a demonstration."
		<br><br>

		<<link [[Next|Events Swimming NPC Dive]]>><<set $phase to 2>><</link>>
		<br>

	<<elseif $rng % 10>>/*9 in 10 chance*/
		The <<person>> swims to the edge and climbs out. <<He>> goes to the diving board. A number of people stop to watch.
		<br><br>

		<<link [[Next|Events Swimming NPC Dive]]>><<set $phase to 1>><</link>>
		<br>

	<<else>>

		The <<person>> refuses. Mason tries to persuade <<him>> and finally threatens <<him>> with detention, but <<he>> will not dive.
		<br><br>

		<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>

	<</if>>

<<elseif $rng gte 61>>
	<<set $rng to random(1, 100)>>
	<<if $exposed gte 1>>
		<<npc Mason>><<person1>>Mason points at you. "Climb out, I want to see your diving technique." Face flushing, you remind <<him>> that you're naked beneath the water. <<He>> looks away, embarrassed. "You still need to dive," <<he>> says. "I'll try to distract the other students."
		<br><br>

		<<He>> stands beside the pool opposite the diving platform. "Everyone, it's time for a demonstration." You wait until everyone is facing away from you, then <<nervously>> climb out.

		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
			<<generates2>><<person2>>As you approach the diving platform however, a <<person>> turns. You cover your body as <<he>> shouts to <<his>> friends, attracting more attention. Mason tries to control the situation, but its no use. The whole class is soon staring at you.
			<<gtrauma>><<trauma 2>><<gstress>><<stress 2>>
			<br><br>
			<<if $exhibitionism gte 75>>
				<<link [[Flaunt|Events Swimming Flaunt]]>><</link>><<exhibitionist5>>
				<br>
			<</if>>
			<<link [[Dive anyway|Events Swimming Lewd Dive]]>><<trauma 2>><<stress 2>><</link>><<gtrauma>><<gstress>>
			<br>
			<<link [[Run Away|Event Swimming Run]]>><<detention 1>><</link>><<gdelinquency>>
			<br>
		<<else>>
			You glance at the rest of the class, and see their backs still turned. You dive <<swimmingtext>> into the water.
			<<gstress>><<stress 2>><<gswimming>><<swimmingskilluse>>
			<br><br>
			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
		<</if>>

	<<else>>
		<<set $rng to random(1, 100)>>
		<<if $school gte 1600>>
			<<set $rng to random(1, 100)>>
			<<if $rng gte 81>>
			  <<npc Mason>><<person1>>Mason points at you. "Climb out, I want to see your diving technique." You climb out into the chilly air, and walk to the edge of the diving platform. You dive <<swimmingtext>> into the water.
			  <br><br>
			  You come back up to the surface. Mason gives you a thumbs up, then directs <<his>> attention elsewhere. You feel like something is amiss, and then you realise, your <<undies>> <<underlowerplural>> gone!<<endevent>>
			  <br><br>
			  <<generates1>><<person1>><<underlowerstrip>>
			  You look around and see your clothes floating in the water. You swim towards them, but a <<person>> beats you to it. <<He>> holds them out of your reach and smiles at you. "Stripping in public? Who'd have known a nerd like you would be so perverted." <<He>> examines your exposed body, then checks if Mason is still distracted. "I wanna have some fun with you. You better do what I want or you'll be spending the rest of the lesson naked."
			  <br><br>
			  <<link [[Accept|Events Swimming Molestation Single]]>><<set $molestationstart to 1>><</link>>
			  <br>
			  <<link [[Refuse|Events Swimming Refuse]]>><</link>>
			  <br>
			<<else>>
				<<npc Mason>><<person1>>Mason points at you. "Climb out, I want to see your diving technique." You climb out into the chilly air, and walk to the edge of the diving platform. You dive <<swimmingtext>> into the water.
				<br><br>
				You come back up to the surface. Mason gives you a thumbs up, then directs <<his>> attention elsewhere. You feel like something is amiss, and then you realise, your <<undies>> <<underlowerplural>> gone! Seeing your clothes floating several feet away, you quickly swim over and cover up. You glance around the pool, you don't think anybody noticed.
				<<gstress>><<stress 2>><<gswimming>><<swimmingskilluse>>
				<br><br>
				<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
			<</if>>

		<<else>>
			<<npc Mason>><<person1>>Mason points at you. "Climb out, I want to see your diving technique." You climb out into the chilly air, and walk to the edge of the diving platform. You dive <<swimmingtext>> into the water.
			<br><br>
			You come back up to the surface. Mason gives you a thumbs up, then directs <<his>> attention elsewhere. You feel like something is amiss, and then you realise, your <<undies>> <<underlowerplural>> gone! Seeing your clothes floating several feet away, you quickly swim over and cover up. You glance around the pool, you don't think anybody noticed.
			<<gstress>><<stress 2>><<gswimming>><<swimmingskilluse>>
			<br><br>
			<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
		<</if>>
	<</if>>
<<elseif $rng gte 41>>
	<<generates1>><<person1>>A <<person>> seems to be following you around the pool. Whenever you look at <<him>> however, <<he>> averts <<his>> gaze.
	<br><br>

	<<link [[Confront|Events Swimming Stalk Confront]]>><<detention 1>><</link>><<gdelinquency>>
	<br>
	<<link [[Ignore|Events Swimming Stalk Ignore]]>><</link>>
	<br>

<<elseif $rng gte 21>>
	<<generates1>><<generates2>><<generates3>>A <<person1>><<personcomma>> <<person2>><<personcomma>> and <<person3>><<person>> start picking on you, following and touching you beneath the water. You could try to swim away from them, but it might make things worse if you fail.
	<br><br>

	<<link [[Swim Away|Events Swimming Swim Away]]>><</link>><<swimmingdifficulty 1 800>>
	<br>
	<<link [[Endure It|Evens Swimming Endure]]>><<stress 2>><<trauma 2>><<arousal 200>><</link>><<gtrauma>><<gstress>><<garousal>>
	<br>

<<else>>
	<<if $worn.lower.type.includes("naked")>>
		<<set $pain += 20>>
		You feel a sharp pain in your butt. You floated too close to a skimmer and were pinched by the flap. You manage to not cry out and attract more attention to yourself.
		<<gstress>><<stress 2>>
		<br><br>
		<<link [[Next|Swimming Lesson]]>><</link>>
	<<else>>
		<<set $worn.lower.integrity -= 4>>
		<<exposure>>
		Something tugs at your $worn.lower.name. You floated too close to a skimmer and your clothes are caught in its flap.
		<<integritycheck>>
		<<exposure>>
		<<if $worn.lower.type.includes("naked")>>
			You move away, but as you do so your clothing is cruelly ripped free from your body, disappearing through the skimmer in an instant, leaving you <<if $worn.upper.type.includes("naked")>>completely bare.<<else>>bare from the waist down.<</if>>
		<<else>>
			You move away, and your $worn.lower.name <<lowerplural>> torn free of the skimmer, intact but worse for wear.
		<</if>>
		<br><br>
		<<link [[Next|Swimming Lesson]]>><</link>>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "eventsswimmingsafe">><<nobr>>
<<if $rng gte 81>>
	<<npc Mason>><<person1>>Mason has the class practise the front crawl. <<He>> patrols the outskirts of the pool, giving pointers to those who seem to be struggling.
	<br><br>
	<<if $swimmingskill lte 100>>
		You're not very good at swimming, and struggle to even stay above the surface of the water.
	<<elseif $swimmingskill lte 300>>
		You're not a very fast swimmer, and need to take frequent breaks due to the strain on your muscles.
	<<elseif $swimmingskill lte 500>>
		You're not a bad swimmer, but you've a long way to go.
	<<elseif $swimmingskill lte 900>>
		You're a pretty good swimmer, among the best in the class.
	<<else>>
		You're a very good swimmer, easily the best in the class.
	<</if>>
	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 61>>
	<<npc Mason>><<person1>>Mason has the class practise the breaststroke. <<He>> patrols the outskirts of the pool, giving pointers to those who seem to be struggling.
	<br><br>
	<<if $swimmingskill lte 100>>
		You're not very good at swimming, and struggle to even stay above the surface of the water.
	<<elseif $swimmingskill lte 300>>
		You're not a very fast swimmer, and need to take frequent breaks due to the strain on your muscles.
	<<elseif $swimmingskill lte 500>>
		You're not a bad swimmer, but you've a long way to go.
	<<elseif $swimmingskill lte 900>>
		You're a pretty good swimmer, among the best in the class.
	<<else>>
		You're a very good swimmer, easily the best in the class.
	<</if>>
	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 41>>
	<<npc Mason>><<person1>>Mason has the class practise the backstroke. <<He>> patrols the outskirts of the pool, giving pointers to those who seem to be struggling.
	<br><br>
	<<if $swimmingskill lte 100>>
		You're not very good at swimming, and struggle to even stay above the surface of the water.
	<<elseif $swimmingskill lte 300>>
		You're not a very fast swimmer, and need to take frequent breaks due to the strain on your muscles.
	<<elseif $swimmingskill lte 500>>
		You're not a bad swimmer, but you've a long way to go.
	<<elseif $swimmingskill lte 900>>
		You're a pretty good swimmer, among the best in the class.
	<<else>>
		You're a very good swimmer, easily the best in the class.
	<</if>>
	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 21>>
	<<npc Mason>><<person1>>Mason has the class practise the butterfly stroke. <<He>> patrols the outskirts of the pool, giving pointers to those who seem to be struggling.
	<br><br>
	<<if $swimmingskill lte 100>>
		You're not very good at swimming, and struggle to even stay above the surface of the water.
	<<elseif $swimmingskill lte 300>>
		You're not a very fast swimmer, and need to take frequent breaks due to the strain on your muscles.
	<<elseif $swimmingskill lte 500>>
		You're not a bad swimmer, but you've a long way to go.
	<<elseif $swimmingskill lte 900>>
		You're a pretty good swimmer, among the best in the class.
	<<else>>
		You're a very good swimmer, easily the best in the class.
	<</if>>
	<br><br>
	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<<else>>
	<<npc Mason>><<person1>>Mason has the class practise diving. <<He>> watches from the outskirts of the pool, giving pointers to those who seem to be struggling.
	<br><br>

	<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
	<br>

<</if>>
<</nobr>><</widget>>

<<widget "schoolpoolexposed">><<nobr>>
<<if $schoolstate is "fifth" and $swimnudecounter gte 1>>
	You step into the pool room. <<covered>>
	<br><br>
<<else>>
	<<npc Mason>><<generatey2>><<generatey3>>You take a deep breath, and walk into the pool room with your <<lewdness>> on display. A <<person2>><<person>> sees you first. <<He>> clutches <<his>> hands to <<his>> face and squeals. The rest of the class follow suit, with varying reactions ranging from glee to embarrassment and amusement to displeasure in the case of the more prudish students.
	<<if $penisexist is 1>>
		<<if $penissize is 4>>
			The girls eyes seem glued to your <<genitalscomma>> and a few start exchanging excited whispers with each other. A few of the boys are clearly less than gleeful at your naked presence.
			<<incgpenisinsecurity>>
			<<status +10>><<gcool>>
		<<elseif $penissize lte 1>>
			A few of the girls exchange whispers and start giggling to themselves, and some of the boys are sporting rather smug expressions.
			<<incgpenisinsecurity>>
		<</if>>
	<</if>>
	<br><br>
	<<person1>>Mason has <<his>> back to you. <<He>> turns, then quickly averts <<his>> eyes. "Your swimsuit," <<he>> manages. "Go get it."
	<<detention 6>>
	<br><br>
	"Oh?" you say. You lift one hand to your brow, as if scanning the horizon. "I wonder where I left it." The class erupts into laughter.
	<br><br>
	Mason refuses to look at you. <<Hes>> blushing. "Get back in the changing room. P-please."
	<br><br>
	<<link [[Boy's changing room|School Boy Changing Room]]>><<endevent>><</link>>
	<br>
	<<link [[Girl's changing room|School Girl Changing Room]]>><<endevent>><</link>>
	<br>
<</if>>
<</nobr>><</widget>>