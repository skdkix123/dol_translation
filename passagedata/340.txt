<<set $outside to 1>><<set $location to "cabin">><<effects>>
You get on your hands and knees and start tugging up weeds at their roots. Some of them are really stuck in there.
<<physique 6>><<set $edengarden += 1>>
<br><br>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>
	<<if $rng gte 51 and $tentacledisable is "f" and $hallucinations gte 2>>
		The vines around you animate into life. You try to stand but they wrap around your wrists, forcing you to remain on your knees.
		<br><br>
		<<link [[Struggle|Clearing Weeding Tentacles]]>><<set $molestationstart to 1>><</link>>
		<br>
		<<link [[Soothe|Clearing Weeding Soothe]]>><</link>><<tendingdifficulty 1 1200>>
		<br>
	<<elseif ($hour gt 6 and $hour lt 11) or $hour gt 14>>
		<<npc Eden>><<person1>>Eden creeps up behind you, making you jump in fright. <<He>> leans on your back, preventing you from standing up. "Why don't you stay right there," <<he>> says, reaching around you and fondling your <<groinstop>>
		<br><br>
		<<link [[Let Eden continue|Clearing Fondle]]>><<npcincr Eden love 1>><<set $submissive += 1>><<npcincr Eden dom 1>><</link>><<garousal>>
		<br>
		<<link [[Shove Eden off|Clearing Shove]]>><<npcincr Eden love -1>><<set $submissive -= 1>><<npcincr Eden dom -1>><</link>>
		<br>
	<<else>>
		You work for a few hours. You stand back to admire the result. It looks much better than when you started.
		<br><br>
		<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
		<br>
	<</if>>
<<else>>
	You work for a few hours. You stand back to admire the result. It looks much better than when you started.
	<br><br>
	<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
	<br>
<</if>>