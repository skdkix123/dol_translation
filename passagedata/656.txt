<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $submissive gte 1150>>
"Leave us alone," you say. "We only want to eat in peace."
<br><br>
<<elseif $submissive lte 850>>
"Leave," you say. "Before I change my mind and feed you to my family." The wolves growl.
<br><br>
<<else>>
"Leave," you say. "I can't hold them back forever." The wolves growl.
<br><br>
<</if>>

The hunters nod and march from the camp with wolves nipping at their heels. The wolves rush to the deer carcass once they are out of earshot.
<br><br>

<<endevent>>

	The black wolf keeps the choice parts for itself. You rummage in a bag left behind by the hunters and find some canned food that suits you better.
	<<stress -12>><<lstress>>
	<br><br>

	<<if $wolfpackspray isnot 1>>
	<<set $wolfpackspray to 1>>
	You find a strange cylinder among their things. It looks like a charge for your pepper spray, but with an exposed computer chip at the base. You put it in.
	<<gspraymax>><<set $spraymax += 1>><<spray 5>>
	<br><br>
	<</if>>

	The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
	<br><br>

<<wolfpackhuntoptions>>