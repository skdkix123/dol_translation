<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewersmud">>
<<set $sewersevent -= 1>>
You are in the old sewers. You're up to your ankles in mud.
<br><br>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
		<<link [[Hide in the mud|Sewers Mud Hide]]>><</link>>
		<br>
	<</if>>
	<<link [[Fungus-coated tunnel (0:05)|Sewers Shrooms]]>><<pass 5>><</link>>
	<br>
	<<link [[Algae-coated tunnel (0:05)|Sewers Algae]]>><<pass 5>><</link>>
	<br>
	<<link [[Tunnel filled with rubbish (0:05)|Sewers Commercial]]>><<pass 5>><</link>>
	<br><br>
<</if>>