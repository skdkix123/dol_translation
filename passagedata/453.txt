<<effects>>
<<set $trance to 0>>
<<if $vorestage gte 7>>
	<<tearful>> you struggle as the snake starts to move. It journeys for several minutes until with a great heave the snake pushes you up and through its gullet once more, expelling you from its body.<<pass 10>>
	<br><br>
	<<endcombat>>
	<<if $swarmdisable is "f">>
		You land in a pit of thousands of tiny snakes.
		<br><br>
		<<link [[Next|Forest Snake Swarm]]>><<set $molestationstart to 1>><</link>>
	<<else>>
		The snake slithers away. It took you deeper into the forest.
		<br><br>
		<<clotheson>>
		<<link [[Next|Forest]]>><<set $eventskip to 1>><<set $forest += 20>><</link>>
		<br>
	<</if>>
<<else>>
	<<tearful>> you escape from the snake's maw.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
	<br>
<</if>>