<<set $outside to 0>><<set $location to "town">><<effects>>
You find a spare display board and drag it behind your own. You pin blank pieces of paper to it and write "Pretty flowers!" on each of them. Then you wait for your chance.
<br><br>

The <<person2>><<person>> starts chatting with the display owner to their other side. You creep over with the new display board and try to place it in front of <<his>> own.

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<<set $sciencefaircheat += 10>>
	<span class="green"><<He>> doesn't notice.</span> And <<he>> doesn't look behind when <<he>> stops chatting.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>

	<<person1>>Leighton and Sirris arrive to find the display pretty, but vacuous. Leighton comments as much before moving on. The <<person2>><<person>> looks confused and angry, until <<he>> turns. <<His>> face turns white. In <<his>> haste to move the fake board <<he>> trips and tumbles to the ground. <<He>> scrambles to <<his>> feet and tries to plead <<his>> case to Sirris, who seems sympathetic. Leighton doesn't care.
	<br><br>

<<else>>
	<span class="red">You bump the display, jostling <<his>> project and drawing <<his>> attention.</span> <<His>> mouth gapes when <<he>> sees you. <<He>> guards <<his>> display more vigilantly from then on.
	<<status -10>><<lcool>>
	<br><br>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>

	<<person2>>Leighton and Sirris arrive to examine the <<persons>> project. Not even Leighton can find much wrong with it. <<person1>><<He>> comments as much before moving on. The <<person2>><<person>> looks relieved as the teachers approach yours.
	<br><br>

<</if>>

<<link [[Next|Science Fair Display]]>><</link>>
<br>