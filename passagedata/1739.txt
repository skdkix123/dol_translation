<<set $outside to 0>><<set $location to "compound">><<effects>>
<<if $compoundlie is 1>>
	"I work for the government," you say. "You better let me go."
	<br><br>
	<<He>> looks at a screen you can't see. "Liar." <<He>> turns a dial on the machine and an excruciating pain surges through you. It lasts only a moment, but leaves you shaking. "I don't want to do that again. So don't lie to me."
	<br><br>
<<elseif $compoundlie is 2>>
	"I got lost," you say. "I was just looking for a way out."
	<br><br>
	<<He>> turns the dial once more and the pain surges through you again. "This could be so much easier for you," <<he>> says.
	<br><br>
<<elseif $compoundlie is 3>>
	"I'm looking for my ball," <<he>> interrupts you with another surge of pain.
	<br><br>
	"I'm not falling for that one," <<he>> says.
	<br><br>
<<elseif $compoundlie is 4>>
	"I'm giving you the chance to let me go," you say. "Before I break out and kick your ass."
	<br><br>
	<<He>> laughs as <<he>> turns the dial. "I don't even need to check the screen this time."
<<else>>
	"You've hurt me too much," you say. "I can't remember anything."
	<br><br>
	"That's bad news for you," <<he>> says, turning the dial. "I can be here a while."
<</if>>
<<compoundoptions>>