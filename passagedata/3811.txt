<<effects>>

<<person1>>You and Avery enter a great hall. A band plays in the corner. Well-dressed guests chat in groups as servants weave through them, holding trays of drinks. A few nod at Avery.

<<if $famesocial gte 1000>>
	More nod at you.
<<elseif $famesocial gte 600>>
	As many nod at you.
<<elseif $famesocial gte 400>>
	Some even nod at you.
<<else>>
<</if>>
<br><br>

You accompany Avery as <<he>> schmoozes. How do you want to behave?
<br><br>

<<link [[Cute|Avery Party Cute]]>><<sub 1>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
<br>
<<link [[Polite|Avery Party Polite]]>><<npcincr Avery love 3>><<set $endear += 10>><</link>><<gglove>>
<br>
<<link [[Aloof|Avery Party Aloof]]>><<def 1>><<npcincr Avery love 5>><<set $endear -= 5>><</link>><<ggglove>><<lendear>>
<br>