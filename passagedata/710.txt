<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

	<<beastejaculation>>

	<<if $worn.upper.sleeve_img is 1>>
		Satisfied, the <<farm_text dog>> leaves the coop as the chickens step away from the fence, freeing your arms.
		<<unbind>>
	<<else>>
		Satisfied, the <<farm_text dog>> leaves the coop, leaving you to slump to the ground.
	<</if>>
	<<lrespect>><<farm_dogs -1>>
	<br><br>

	<<tearful>> you finish your work in the coop.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<elseif $enemyhealth lte 0>>

	<<if $worn.upper.sleeve_img is 1>>
		The <<farm_text dog>> turns and runs. The chickens follow, releasing your arms.
		<<unbind>>
	<<else>>
		The <<farm_text dog>> turns and runs as you pull yourself to your feet.
	<</if>>

	<<tearful>> you finished your work in the coop.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<link [[Next|Farm Work]]>><</link>>
	<br>

<<else>><<set $rescued += 1>>
	<<endcombat>>
	<<npc Alex>><<person1>>
	"I'm coming!" It's Alex. The <<farm_text dog>> backs away at the sound of <<his>> voice.
	
	
	<<if $worn.upper.sleeve_img is 1>>
		The chickens follow suit.
		<<unbind>>
	<<else>>
	<</if>>
	<br><br>
	
	<<tearful>> you pull yourself to your feet, just as Alex arrives at the entrance of the coop. "Bad dog!" <<he>> says, grasping the beast's collar. "No supper for you tonight." The <<farm_text dog>>'s ears fall limp.<<lrespect>><<farm_dogs -1>>
	<br><br>

	<<clotheson>>
	
	<<if $exposed gte 1 and $farm_naked isnot 1>>
		Alex grabs you some towels to cover with.
	<</if>>
	"I'm sorry," <<he>> says. "The dogs can be spirited. Good thing I heard you."<<gdom>><<npcincr Alex dom 1>>
	<<if $exposed gte 1>>
		<<glust>><<npcincr Alex lust 1>>
		<<if $farm_naked isnot 1>>
			<<towelup>>
		<</if>>
	<</if>>
	<br><br>
	
	<<link [[Thank Alex|Farm Coop Thank]]>><<npcincr Alex dom 1>><<npcincr Alex love 1>><</link>><<gdom>><<glove>>
	<br>
	<<link [[Get angry|Farm Coop Angry]]>><<npcincr Alex dom -1>><<npcincr Alex love -1>><</link>><<llove>><<ldom>>
	<br>
	<<link [[Say nothing|Farm Coop Silent]]>><</link>>
	<br>

<</if>>