<<effects>>

The tendrils pull away from you, disappearing into cracks in the concrete floor.
<br><br>

<<tearful>> you look around. You're in a dark corridor. The conveyor belt moves again above you. You search for a way out, and find a hatch in the wall. You pry it open, and squeeze outside. It snaps shut behind you. You're on Elk Street.
<br><br>

<<clotheson>>
<<endcombat>>
<<set $stress -= 2000>>
<<link [[Next|Elk Street]]>><<set $eventskip to 1>><</link>>
<br>