<<effects>>

<<if $submissive gte 1150>>
	"D-do you like it?" you ask. "You can look closer if you want."
<<elseif $submissive lte 850>>
	"If you want to look," you say. "Just ask."
<<else>>
	"Do you like it?" you ask.
<</if>>
You turn so <<he>> can get a better look.
<<promiscuity2>>

<<if $rng gte 81>>
	"You're a slutty one," <<he>> laughs. "Just how I like em."
<<elseif $rng gte 61>>
	"You go careful wearing that," <<he>> says. "Someone might get the wrong idea."
<<elseif $rng gte 41>>
	<<He>> blushes and looks away.
<<elseif $rng gte 21>>
	"I didn't know they made sluts as cute as you," <<he>> says.
<<else>>
	<<He>> barks a nervous laugh before continuing on.
<</if>>
<br><br>

<<endevent>>
<<destinationeventend>>