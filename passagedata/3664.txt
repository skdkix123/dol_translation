<<temple_effects>><<effects>>
You explore the previously-forbidden parts of the temple. Upstairs are living quarters for the monks and nuns. Outside, behind the temple building, is a large garden where initiates tend beds of flowers. A hedge maze trails away into the forest.
<br><br>
You find several doors painted with red crosses. Jordan warned you not to enter them, but they're all sealed shut anyway. Except for one. You find it in the back corner of the building. A <<generate1>><<person1>>stern-faced <<monk>> stands in front of it. <<His>> eyes are shut, but you feel <<him>> watching you.
<br><br>
<<link [[Next|Temple Prayer Explore 2]]>><</link>>
<br>