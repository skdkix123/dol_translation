<<set $outside to 0>><<set $location to "tentworld">><<effects>>
You are on the tentacle plains. Tendrils growing from the ground strain to reach your <<genitalscomma>> caressing your thighs and
whatever else they can reach. The air is sweet.
<<set $drugged += 10>><<ggarousal>><<arousal 1200>>
<br><br>
<<if $phase is 1>>
	You walk north.
	<br><br>
<</if>>
<<if $phase is 2>>
	You walk south.
	<br><br>
<</if>>
<<if $phase is 3>>
	You walk east.
	<br><br>
<</if>>
<<if $phase is 4>>
	You walk west.
	<br><br>
<</if>>
<<tentaclewolf>>
<<set $phase to 0>>
<<if $stress gte 10000>>
	<<passouttentacleworld>>
<<elseif $arousal gte $arousalmax and $eventskip isnot 1>>
	<<orgasmpassage>>
	Energised, the tentacles around you dart higher than normal. They wrap around your arms and drag you to the ground.
	<br><br>
	<<link [[Next|Tentacle Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	<<if $tentnorth is 0 and $tenteast is 0>>
		<<if $tentacleEntrance is "mirror">>
			You stand in front of your mirror.
			<br>
			<<link [[Walk through|Tentacle Home Return]]>><</link>>
		<<else>>
			The door to the asylum is nearby.
			<br>
			<<link [[Return to asylum|Tentacle Return]]>><</link>>
		<</if>>
		<br><br>
	<</if>>
	<<if $tentnorth is $tentportalnorth and $tenteast is $tentportaleast>>
		<<if $wolfgirl gte 6 and $syndromewolves gte 1>>
			You see another door in the distance, similar to the one that led you here. It's surrounded by tentacles. The door is wide open, and the forest lies on the other side, promising escape. You see a black blur shift before snapping and snarling at one of the tendrils.
			<br><br>
			<<link [[Run for the door|Tentacle Wolf Escape]]>><</link>>
			<br><br>
		<<else>>
			A door stands open here. There's a forest beyond.
			<br>
			<<link [[Enter|Tentacle Escape]]>><</link>>
			<br><br>
		<</if>>
	<</if>>
	<<if $tentnorth gte 5 or $tentnorth lte -5 or $tenteast gte 5 or $tenteast lte -5>>
		The plains stretch to the horizon. You don't want to wander too far from the portal.
		<br><br>
	<</if>>
	<<if $tentnorth is 4 and $tenteast is 4 and $tentspray isnot 1>>
		A red light hovers in the air at chest-height, held aloft by thin tendrils.
		<br>
		<<link [[Touch|Tentacle Plains Spray]]>><<set $spraymax += 1>><<set $tentspray to 1>><<spray 5>><</link>><<gspraymax>>
		<br><br>
	<</if>>
	<<if $tentnorth lt 5>>
		<<link [[North (0:10)|Tentacle Plains]]>><<pass 10>><<set $tentnorth += 1>><<set $phase to 1>><</link>>
		<br>
	<</if>>
	<<if $tentnorth gt -5>>
		<<link [[South (0:10)|Tentacle Plains]]>><<pass 10>><<set $tentnorth -= 1>><<set $phase to 2>><</link>>
		<br>
	<</if>>
	<<if $tenteast lt 5>>
		<<link [[East (0:10)|Tentacle Plains]]>><<pass 10>><<set $tenteast += 1>><<set $phase to 3>><</link>>
		<br>
	<</if>>
	<<if $tenteast gt -5>>
		<<link [[West (0:10)|Tentacle Plains]]>><<pass 10>><<set $tenteast -= 1>><<set $phase to 4>><</link>>
		<br>
	<</if>>
	<<if $demon gte 6 and $tentacleadmire gte 1>>
		<<link [[Offer yourself|Tentacle Plains Admire]]>><</link>> | <span class="red">Demon</span>
		<br>
	<<elseif $demon gte 6>>
		<<link [[Admire|Tentacle Plains Admire]]>><</link>> | <span class="red">Demon</span>
		<br>
	<</if>>
	<<if $tentaclewolf gte 2>>
		<<link [[Howl|Tentacle Plains Howl]]>><</link>> | <span class="red">Wolf <<girl>></span>
		<br>
	<</if>>
	<<if $angel gte 6 and $tentacleangel gte 2>>
		<<link [[Resist|Tentacle Plains Resist]]>><</link>> | <span class="gold">Angel</span>
		<br>
	<<elseif $angel gte 6>>
		<<link [[Focus|Tentacle Plains Resist]]>><</link>> | <span class="gold">Angel</span>
		<br>
	<</if>>
	<<if $fallenangel gte 2>>
		<<link [[Despair|Tentacle Plains Despair]]>><</link>> | <span class="black">Fallen Angel</span>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>