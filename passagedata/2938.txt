<<effects>>
<<bind>>
<<if $phase is 0>>

Your arms are bound, and you are pushed down the alleyway until you come to a black car with an open boot. They shove you in, and slam it shut behind you.
<br><br>

<<else>>
You scream. One of the goons steps forward and gags your mouth with <<his>> hand. Two more grasp your arms, and bind them behind your back. You are pushed down the alleyway until you come to a black car with an open boot. They shove you in, and slam it shut behind you.
<br><br>

<</if>>

<<link [[Next|Maths Boot]]>><</link>>
<br>