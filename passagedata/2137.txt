<<effects>>
<<set $baileydefeatedchain to 0>>
<<npc Bailey>><<person1>>With your head covered by a hood and your arms bound behind your back you are led down a flight of stairs and along a tunnel. You trip a couple of times, prompting Bailey to drag you back to your feet and slap your face in anger.
<<pass 20>>
<br><br>

<<if $rentsale is 3>><<set $rentsale to 0>>

After journeying for some time you hear another set of footsteps.
<<generate2>><<person2>>"Hand <<phim>> over," says a <<if $pronoun is "m">>man's<<else>>woman's<</if>> voice.
<br><br>

"Money first," responds Bailey. "Will I get <<phim>> back?"
<br><br>

"You won't want <<phim>> when we're done."
<br><br>

You hear a rustle of paper before being dragged further along the tunnel.
<br><br>

<<link [[Next|Underground Intro]]>><</link>>
<br>

<<elseif $rentsale is 2>><<set $rentsale += 1>>
	<<generate2>><<generate3>><<person2>>
	You journey for some time, until you hear the sound of engines up ahead. The sound echoes, becoming closer, until you hear it all around you. "Good stock," says an unfamiliar voice. "Would make a fine breeder."
	<br>

	"The money," Bailey responds, impatient.
	<br>
	"Ah. Here." There's a rustle to one side, then someone tugs on your leash, pulling you forward.
	<br>
	"A pleasure," Bailey says, already walking away.
	<br><br>

	"Watch the step," says the unfamiliar voice. Whatever you've stepped onto is rumbling. "Sit." You fumble around your knees, and find what feels like a bench. You sit, then hear the slam of doors, and a few moment later you lurch as the floor moves beneath you. You're in a vehicle.
	<br><br>

	<<link [[Next|Street Van Bailey]]>><</link>>
	<br>

<<elseif $rentsale is 1>><<set $rentsale += 1>>

After journeying for some time you emerge outside. You hear wind rustling through leaves. Bailey lifts you and hooks your bindings on something behind you, leaving you suspended in the air. "Don't go anywhere, you'll be picked up shortly," <<he>> says. You hear <<him>> walk away, leaving you alone.
<br><br>
<<endevent>>
<<npc Eden>><<person1>>
You aren't left hanging there for long. Your hood is yanked off your head as a <<if $pronoun is "m">>man's<<else>>woman's<</if>> voice appears beside you.

	<<if $edenfreedom gte 2 and $edendays gte 8>>
	"I trusted you," Eden says as <<he>> cuts your bindings. You fall to the floor in a heap. "This is what happens if you don't obey me. It's for your own protection." <<He>> pulls you to your feet. "Let's go home."
	<br><br>

	<<link [[Next (0:30)|Eden Recaptured]]>><<pass 30>><</link>>
	<br>

	<<elseif $edenfreedom is 1 and $edendays gte 2>>

	"I trusted you," Eden says as <<he>> cuts your bindings. You fall to the floor in a heap. "This is what happens if you don't obey me. It's for your own protection." <<He>> pulls you to your feet. "Let's go home."
	<br><br>

	<<link [[Next (0:30)|Eden Recaptured]]>><<pass 30>><</link>>
	<br>

	<<elseif $edenfreedom gte 1>>

	"I knew you'd end up like this. The town is dangerous," Eden says, cutting your bindings. <<He>> catches you as you fall. "Let's go home."
	<br><br>

	<<He>> takes you back to <<his>> cabin.
	<br><br>
	<<endevent>>
	<<link [[Next (0:30)|Eden Cabin]]>><<pass 30>><</link>>
	<br>

	<<else>>

		<<if $hunterintro is 1>>
	"I knew I'd get you back."
		<<else>>
	"I'm not used to my prey being so helpless. Not that I'm complaining."
		<</if>>
<<He>> crept up on you without making a sound.
<br><br>

	<<endevent>>

	<<link [[Next|Forest Hunter Intro]]>><</link>>
	<br>

	<</if>>

<<else>>
<<set $rentsale to 1>>

After journeying for some time you hear another set of footsteps. <<generate2>><<person2>>"This is the merchandise?" says a <<if $pronoun is "m">>man's<<else>>woman's<</if>> voice.
<br><br>

"No, I'm just taking my <<girl>> for a walk," says Bailey, <<his>> temper barely concealed beneath the sarcasm. "Just give me the cash. And I want it back in one piece, you're not paying enough..." <<He>> presses something against your mouth and your consciousness fades.
<br><br>
<<arousal 6000>>
<<link [[Next|Danube Meal]]>><<endevent>><</link>>
<br>

<</if>>