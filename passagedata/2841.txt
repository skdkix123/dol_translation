<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 1>><<scienceskill>>
You settle down at a desk with a science textbook and read through the material.
<<elseif $phase is 2>><<mathsskill>>
You settle down at a desk with a maths textbook and read through the material.
<<elseif $phase is 3>><<englishskill>>
You settle down at a desk with an English textbook and read through the material.
<<elseif $phase is 4>><<historyskill>>
You settle down at a desk with a history textbook and read through the material.
<</if>>
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $schoolday is 1 and $schoolstate isnot "early" and $schoolstate isnot "late">>
		<<if $rng gte 51>>
		<<generatey1>><<generatey2>>While you are minding your own business, a <<person1>><<person>> and <<person2>><<person>> start harassing you.
		<br><br>

		<<link [[Ignore them|School Library Harass]]>><<set $phase to 0>><<trauma 2>><<stress 2>><</link>><<gtrauma>><<gstress>>
		<br>
		<<link [[Ask the librarian for help|School Library Harass]]>><<set $phase to 1>><<status -10>><</link>><<lcool>>
		<br>
		<<else>>
		<<set $worn.lower.integrity -= 10>>
			<<if $worn.lower.integrity lte 0>>
			As you rise from the chair, you hear a tear. Your $worn.lower.name <<lowerplural>> caught on the desk. You try to free them, but <<loweritis>> torn clean off your body, exposing your <<undiesstop>>
			<<lowerruined>><<trauma 3>>
			<br><br>
			<<elseif $worn.lower.integrity gte 0>>
			As you rise from the seat, you hear a tear. Your $worn.lower.name <<lowerplural>> caught on the desk.
			<br><br>
			<</if>>
		<<link [[Next|School Library]]>><</link>>
		<br>
		<</if>>
	<<else>>
	<<link [[Next|School Library]]>><</link>>
	<br>
	<</if>>