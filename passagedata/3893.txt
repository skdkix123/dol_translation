<<set $outside to 0>><<set $location to "school">><<effects>>

<<if $phase is 0>>

<<elseif $phase is 1>>

	<<if $submissive gte 1150>>
	"D-don't hurt me," you say.
	<<elseif $submissive lte 850>>
	"I demand you let me go," you say. "This instant."
	<<else>>
	"Please let me go," you say. "I won't tell anyone."
	<</if>>
<br><br>

	<<if $rng gte 51>>
	"It's okay," <<he>> says. "I'm nervous too."
	<br><br>
	<<else>>
	<<kylargag>>
	<</if>>

<<elseif $phase is 2>>

<<kylarangry>>
<<kylargag>>

<</if>>

<<He>> stands and hauls the sheet off the large object. It's a great white cake. A wedding cake. <<He>> beams. "It's the big day." <<He>> drops to <<his>> knees again. "The vicar will be here soon." <<He>> touches the chains binding you. "D-do you think, it would be okay if we got a head start? On the honeymoon?"
<br><br>

<<if $mouthuse isnot "gagged">>
<<link [[Plead|Kylar Basement 3]]>><<set $phase to 1>><</link>>
<br>
<<link [[Get angry|Kylar Basement 3]]>><<set $phase to 2>><</link>>
<br>
<</if>>

<<link [[Remain silent|Kylar Basement 3]]>><<set $phase to 0>><</link>>
<br>