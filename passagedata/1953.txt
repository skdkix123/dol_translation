<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<pass 20>>
You head back to the bathroom to dress and recover.
<<clotheson>>
<<endcombat>>
When you open the bathroom door, you find a large bowl. They left you £120 cash, a
<<print either("a 'get well soon' card,","a single broken rose,","a thank-you card,","sketch from your rape,")>>
and some expensive looking jewellery. <<tearful>> you leave.
<<lstress>><<stress -1>>
<<set $money += 12000>><<set $blackmoney to 50>>
<br><br>
<<link [[Next|Domus Street]]>><<endevent>><</link>>
<br>