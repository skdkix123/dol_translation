<<if $enemyhealth lte 0>>

The <<person>> winces in pain. "<<pShe>> is a feisty one," <<he>> says.
<br><br>

<<person2>><<person>>"That's enough," says the <<personstop>> <<He>> grasps your arm and drags you from the room. "That'll be all," <<he>> says when you're alone. "I didn't want a scene like this. Take your money and go." <<He>> hands you £60.
<br><br>

<<set $money += 6000>>

<<tearful>> you return to the pantry and put your clothes back on.

<<clotheson>>
<<endcombat>>

<br><br>

<<link [[Next|Danube Street]]>><<storeon "danubeparty">><</link>>
<br>

<<elseif $enemyarousal gte $enemyarousalmax>>
<<ejaculation>><<famerape 30>>

<<He>> turns to the <<person2>><<person>> "How much for this one?" <<person1>><<he>> says.
<br><br>

"I'm only renting <<phimstop>>"
<br><br>

"Pity."
<br><br>

The <<person2>><<person>> takes you by your arm and drags you from the room. "That'll be all," <<he>> says when you're alone. "Thanks for being a good sport. Here's a little something extra." <<He>> hands you £120.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $money += 12000>>
<<endevent>>

<<tearful>> you return to the pantry and put your clothes back on.
<br><br>
<<pass 1 hour>>
<<link [[Next|Danube Street]]>><<storeon "danubeparty">><</link>>
<br>

<</if>>