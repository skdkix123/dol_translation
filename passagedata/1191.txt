<<effects>>

"That's not what we agreed to" says Remy from the door.
<br>
"I-I," the doctor stutters. "There was an accident. I apologise. I trust my next visit is still scheduled?"
<br>
Remy laughs and takes your leash. "It is. Until we meet again."
<br><br>

<<link [[Next|Livestock Job End]]>><<endevent>><</link>>
<br>