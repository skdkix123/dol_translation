<<set $outside to 0>><<effects>>

	<<if $submissive gte 1150>>
	"Thank you," you say. "But we can walk the rest of the way."
	<<elseif $submissive lte 850>>
	"No," you say. "We're almost there anyway."
	<<else>>
	"It's okay," you say. "We can walk the rest of the way."
	<</if>>
<br><br>

<<if $NPCName[$NPCNameList.indexOf("Avery")].rage gte random(20, 100)>><<set $averyragerevealed to 1>>

Avery flashes Robin a dark look. <<He>> stares forward and taps <<his>> steering wheel then, having made up <<his>> mind, opens the door and steps out. "I gave you a polite offer," <<he>> snarls as <<he>> steps toward you. "Get in the fucking car." <<He>> grasps your arm and tries to pull you with <<himstop>> <<He>> ignores Robin, who stands beside you clutching <<endevent>><<npc Robin>><<person1>><<his>> school bag.
<br><br>

<<endevent>>
<<npc Avery>><<person1>>

	<<if $NPCName[$NPCNameList.indexOf("Robin")].dom gte 91>>
		<<set $robinaverybeat to 1>>

		Avery looks over your shoulder. <<His>> rage falters a moment, and a school bag hurtles into <<his>> face. With a cry, <<he>> releases your arm and falls back against <<his>> car. Robin steps forward, retrieves <<endevent>><<npc Robin>><<person1>><<his>> bag, and brings it down on Avery's head. The weighty books inside make a solid impact. "Leave <<phim>> alone!" Robin shouts. "<<pShes>> mine!"
		<br><br>

		<<endevent>><<npc Avery>><<person1>>

		Avery holds <<his>> arms up for protection and scrambles into <<his>> vehicle. <<He>> glares at Robin. "You'll regret that. I promise." <<His>> car screeches away.
		<br><br>

		<<endevent>><<npc Robin>><<person1>>

		Robin wraps <<his>> arms around you and squeezes. <<Hes>> trembling. <<He>> doesn't stop squeezing until Avery is out of sight.
		<br><br>

		<<link [[Thank Robin|Avery Walk Thank]]>><<npcincr Robin love 3>><</link>><<gglove>>
		<br>
		<<link [[Tease Robin|Avery Walk Tease]]>><<npcincr Robin love 1>><<npcincr Robin lust 1>><</link>><<glove>><<glust>>
		<br>

	<<else>>

		<<link [[Comply|Avery Walk Comply]]>><<npcincr Avery rage -5>><</link>><<larage>>
		<br>
		<<link [[Refuse|Avery Walk Refuse 2]]>><<npcincr Avery rage 5>><</link>><<garage>>
		<br>
	<</if>>

<<else>>

Avery flashes Robin a dark look, but smiles. It doesn't reach <<his>> eyes. "Suit yourself." <<He>> drives away.
<<llove>><<npcincr Avery love -1>>
<br><br>

"Sorry," Robin sounds relieved. "I hope you didn't refuse for my sake. <<He>> does scare me a little."
<<glove>><<npcincr Robin love 1>>
<br><br>

Robin's mood brightens as you walk the rest of the way to school.
<br><br>

<<link [[Next|School Front Playground]]>><<endevent>><</link>>
<br>

<</if>>