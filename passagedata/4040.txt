<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Robin")].dom gte 20>>
	You walk up to Robin and the <<person2>><<personstop>> "Why don't the three of us find somewhere private?," you ask. "The orphans will be okay for a few minutes."
	<<promiscuity3>>

	The <<person>> looks eager, but Robin keeps <<person1>><<his>> eyes locked on <<his>> feet <span class="red">and shakes <<his>> head.</span> "I don't want to."
	<br><br>

	You take Robin's hand and try to pull <<him>> away. The <<person2>><<person>> grasps <<person1>><<his>> other arm and tugs. "Mine," the <<person2>><<person>> growls. "Fuck off."
	<br><br>

	You tussle back and forth, until Robin snatches <<person1>><<his>> arm away from the <<person2>><<personstop>> The sudden release almost topples you both. Robin clutches your waist. <<person1>><<Hes>> trembling.
	<br><br>

	The <<person2>><<person>> glares as you leave the property, but doesn't say anything more.
	<br><br>

	<<link [[Next|Robin Trick 6]]>><<endevent>><<pass 30>><</link>>
	<br>
<<else>>
	<<set $seductiondifficulty to 8000>>
	<<seductioncheck>>
	<br><br>
	<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
	<br><br>

	You walk up to Robin and the <<person2>><<personstop>> "Why don't the three of us find somewhere private?," you ask. "The orphans will be okay for a few minutes."
	<<promiscuity3>>

	<<if $seductionrating gte $seductionrequired>>

	"Sure," the <<person>> says. Robin keeps <<person1>><<his>> eyes locked on <<his>> feet, <span class="green">but nods.</span>
	<br><br>

	You tell the orphans you won't be long, before being led into the house. You pass a noisy room. It sounds like there's a party inside. You enter a ground-level bedroom, and the <<person2>><<person>> closes the door behind you.
	<br><br>

	Despite <<his>> initial interest in Robin, it's you <<he>> pounces on.
	<br><br>

	<<link [[Next|Robin Trick Sex]]>><<set $sexstart to 1>><</link>>
	<br>

	<<else>>

	"Sure," the <<person>> says. Robin keeps <<person1>><<his>> eyes locked on <<his>> feet <span class="red">and shakes <<his>> head.</span>
	<br><br>

	You take Robin's hand and try to pull <<him>> away. The <<person2>><<person>> grasps <<person1>><<his>> other arm and tugs. "Mine," the <<person2>><<person>> growls. "Fuck off."
	<br><br>

	You tussle back and forth, until Robin snatches <<person1>><<his>> arm away from the <<person2>><<personstop>> The sudden release almost topples you both. Robin clutches your waist. <<person1>><<Hes>> trembling.
	<br><br>

	The <<person2>><<person>> glares as you leave the property, but doesn't say anything more.
	<br><br>

	<<link [[Next|Robin Trick 6]]>><<endevent>><<pass 30>><</link>>
	<br>

	<</if>>
<</if>>