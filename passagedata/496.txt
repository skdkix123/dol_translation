<<effects>>

The walls close in, squeezing you. You're pushed upwards by the pressure. You slide from the plant and land on the bed of vines with a squelch.
<br><br>

You lay naked on the ground, sticky with slime and twitching with arousal. 
<br><br>

<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>