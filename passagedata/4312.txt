<<effects>>
You twist around and shove Whitney away from you, enduring the pain as <<his>> hand tugs free of your hair. <<His>> friends' laughter redoubles. "You're lucky I'm in a good mood," <<He>> says.
<br><br>
<<He>> and <<his>> friends walk down the hallway, leaving you to unruffle your hair.
<br><br>
<<endevent>>
<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>