<<effects>>

<<if $physique gte random(1, 24000)>>
	You swing the shovel at the creature, and <span class="green">smack it aside with a crunch.</span> It lands and darts into a bush before you can get a good look at it.
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	The shovel hits, <span class="red">but the creature moves with such force that your impromptu weapon is batted aside.</span> It latches onto your face. You can't see. You try to pull the creature off, but you struggle to get a grip on its slimy skin. You keep your lips sealed as something presses against your mouth.
	<br><br>
	
	<<link [[Next|Farm Yard Struggle]]>><<set $struggle_start to 1>><</link>>
	<br>
<</if>>