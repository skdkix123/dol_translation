<<set $outside to 1>><<effects>>

<<set _color to ["black", "blue", "brown", "green", "pink", "purple", "red", "tangerine", "teal"][random(0,8)]>>
<<generalSend "wardrobe" "upper" 48 _color>>
<<generalSend "wardrobe" "lower" 44 _color>>
<<generalSend "wardrobe" "feet" 9 _color>>

<<He>> hands you the package and continues on <<his>> way, "Looking forward to seeing those photos!"
<br><br>
<<endevent>>
<<destinationeventend>>