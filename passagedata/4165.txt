<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $submissive gte 1150>>
"S-sorry for distracting you," you whisper. You place your hand on <<his>>leg.
<br><br>
<<elseif $submissive lte 850>>
"Out of breath already? I guess I make a good distraction." you say, bringing your face close to <<his>> own.
<br><br>
<<else>>
"You seem worked up. Was I really that much of a distraction?" you say as you lean into <<himstop>>
<br><br>
<</if>>
"N-no, it's not that! I-it's just," Robin stammers, but <<hes>> cut off as you stand up. You hold out your hand for <<him>> to take.
<br><br>

Robin takes your hand with more force than usual. <<He>> stands, but looks conflicted. You try to walk with <<himcomma>> but <<he>> doesn't budge.
<br><br>

In one swift motion, Robin pulls you back, turns, and pushes all of <<his>> body weight onto you. You both fall into the thick bushes behind the bench.
<<promiscuity1>>

<<link [[Next|Robin Park Tease 2]]>><</link>>