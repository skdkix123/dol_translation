<<set $outside to 1>><<set $location to "town">><<effects>>
With the <<wallet>> in hand you dive into the car and crawl beneath the back seat.
<<if $rng gte 51>>
	The <<person>> sits down and starts the engine, unaware that <<his>> <<wallet>> is missing or that <<he>> has a passenger.
	<br><br>
	You hide beneath the seat as the car bumps along the road. You don't wait long before <<he>> pulls to a stop. You hear a gate open and the car drives a bit more, before stopping once more. The driver leaves the car, this time shutting the door.
	<br><br>
	You peek out the window. You're in one of the compounds on Elk Street. A chain link fence towers around the edge, protecting several red brick buildings. You climb out of the car.
	<br><br>
	<<pass 5>>
	You open the <<walletstop>> It's empty.
	<br><br>
	<<endevent>>
	<<link [[Look around|Elk Compound]]>><</link>>
	<br>
<<else>>
	The <<person>> sits down, looks around, then climbs back out. <<He>> must think <<he>> left <<his>> <<wallet>> in the building.
	<br><br>
	You rise from your hiding place and escape the car before <<he>> returns.
	<br><br>
	<<connudatuswallet>>
	<<endevent>>
<</if>>