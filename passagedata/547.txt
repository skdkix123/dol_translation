<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<set $forest to 30>><<set $bus to "lakeshore">>

<<if $laketeenspresent is 1>>
You are at the shore of the lake. There are several people playing in the water. You can see the Swimmers Dock with a few teens sitting on it.
<<elseif $daystate is "night">>
	<<if $weekday is 7 or $weekday is 1>>
	You are at the shore of the lake. You can see faint light from a fire at the campsite across the water.
	<<else>>
	You are at the shore of the lake. Dark water ripples unseen.
	<</if>>
<<elseif $daystate is "dusk" or $daystate is "dawn">>
You are at the shore. Undulating waves splash against the sand and rocks. A bear eats fish on the opposite bank.
<<else>>
You are at the shore of the lake. You can see your reflection in its surface. The Swimmers Dock sits idle out on the water.
<</if>>
<<if $weather is "rain">>
The water is alive with motion as rain breaks its surface.
<<else>>
The water is calm.
<</if>>
<br><br>

<<if $exposed gte 1 and $laketeenspresent is 1>>
You keep low and stay among the trees to keep your <<lewdness>> from being seen.
<br><br>
<</if>>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte 10000>>
<<passoutlake>>
<<elseif $foresthunt gte 10>>
<<foresthunt>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0 and $laketeenspresent isnot 1>>
<<eventlake>>
<<else>>

<<lakereturnjourney>>

There's a rocky alcove where you could store your clothes.
<br>
<<lakeclothes>>

	<<if $laketeenspresent is 1 and $exposed lte 0 and $hour isnot 20>>
	<<link [[Hang out (0:30)|Lake Hang]]>><<pass 30>><<stress -6>><<status 1>><</link>><<gcool>><<lstress>>
	<br>
	<</if>>
<<link [[Swim (0:02)|Lake Shallows]]>><<pass 2>><</link>>
<br>
<<if $exposed gte 2>>
	<<if $exhibitionism gte 75 and $laketeenspresent is 1>>
		<<link [[Take a stroll (1:00)|Lake Stroll]]>><<pass 60>><<stress -6>><<arousal 1000>><<set $rng to random(1, 100)>><<set $phase to 2>><</link>><<exhibitionist5>><<garousal>><<lstress>>
		<br>
	<<elseif $exhibitionism gte 55 and $laketeenspresent isnot 1>>
		<<link [[Take a stroll (1:00)|Lake Stroll]]>><<pass 60>><<stress -6>><<arousal 1000>><<set $rng to random(1, 100)>><<set $phase to 3>><</link>><<exhibitionist4>><<garousal>><<lstress>>
		<br>
	<</if>>
<<else>>
	<<link [[Take a stroll (1:00)|Lake Stroll]]>><<pass 60>><<stress -6>><<set $phase to 1>><</link>><<lstress>>
	<br>
<</if>>
<br>
	<<if $dev is 1>>
	<<link [[North to firepit (0:10)|Lake Firepit]]>><<pass 10>><</link>>
	<br>
	<</if>>
<<link [[South to waterfall (0:10)|Lake Waterfall]]>><<pass 10>><</link>>
<br><br>
<<foresticon>><<link [[Forest (0:10)|Forest]]>><<pass 10>><</link>>
<br>
<<set $eventskip to 0>>
<</if>>