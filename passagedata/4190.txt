<<effects>>

<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<set $combat to 1>>
	<<molested>>
	<<controlloss>>

	<<set $vorestage to 1>>
	<<set $vorecreature to "snake">>
	<<set $vorestrength to 1>><<set $position to "doggy">>
	<<resetLastOptions>>
<</if>>

<<voreeffects>>
<<vore>>
<<voreactions>>

<<if $stress gte 10000>>
	<span id="next"><<link [[Next|Robin Forest Vore Finish]]>><</link>></span><<nexttext>>
<<elseif $vorestage lte 0>>
	<span id="next"><<link [[Next|Robin Forest Vore Finish]]>><</link>></span><<nexttext>>
<<elseif $vorestomach gte 5>>
	<span id="next"><<link [[Next|Robin Forest Vore Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>