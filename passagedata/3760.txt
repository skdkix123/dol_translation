<<set $outside to 1>><<set $location to "town">><<effects>>
You throw the camera behind <<himstop>> <<He>> runs after it. "If you've broken it, I'll break you," <<he>> says as it bounces along the ground. You climb the fence and haul yourself over to the other side while <<hes>> distracted. You're long gone by the time <<he>> turns around.
<br><br>
<<link [[Next|Connudatus Street]]>><<endevent>><</link>>
<br>