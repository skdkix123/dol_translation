<<effects>><<set $location to "sewers">><<set $outside to 0>><<set $bus to "sewerswebs">>
<<set $sewersevent -= 1>>
You are in the old sewers. The walls are covered in huge spider webs.
<br><br>
<<if $sewersantiquecandlestick isnot 1>>
	An antique candlestick is suspended in one.
	<br><br>
<</if>>
<<if $stress gte 10000>>
	It's too much for you. You pass out.
	<br><br>
	<<sewersend>>
	<<sewerspassout>>
<<elseif $sewerschased is 1 and $sewerschasedstep lte 0>>
	<<morgancaught>>
<<elseif $sewersevent lte 0 and $sewerschased isnot 1>>
	<<eventssewers>>
<<else>>
	<<if $sewerschased is 1 and $sewerschasedstep gte 0>>
		<<morganhunt>>
		<br><br>
	<</if>>
	<<if $sewersantiquecandlestick isnot 1>>
		<<link [[Take the candlestick|Sewers Candlestick]]>><</link>>
		<br>
	<</if>>
	<<link [[Tunnel filled with wood (0:05)|Sewers Wood]]>><<pass 5>><</link>>
	<br><br>
<</if>>