<<set $outside to 0>><<set $location to "town">><<effects>>
Doren puts on a pair of running shoes. "Don't worry, we're not gonna go hard."
<br><br>
Together you jog around town. <<He>> shows you several paths through alleyways you didn't know existed. "It's great to have some company," <<he>> says at one point.
<br><br>
<<He>> stops beside the toilets in the park. "Just need a quick break. Won't be long." You're thankful for a break yourself.
<<physique 3>>
<br><br>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>
	You're waiting outside when a <<generate2>><<person2>><<person>> walks up to you. "Hey <<girlcomma>> you all alone?" <<he>> says, resting an arm next to your head. <<He>> glances around to see if anyone's watching. "How about we find somewhere private?"
	<br><br>
	<<He>> tries to pull you into the toilets, just as Doren comes out. <<person1>>Doren takes one look at the <<person2>><<personcomma>> then punches <<him>> in the jaw.
	<br><br>
	The <<person>> falls backwards, clutching <<his>> face. Doren stands over <<him>> and grabs <<his>> collar. "stay the fuck away from <<phim>> you creep, or I'll twist your head right off. Come on, let's go before someone sees. Might look bad."
	<br><br>
<<else>>
	<<He>> didn't lie, and is back in less than a minute. "Come on <<lasscomma>> no time to waste!" You jog after <<himstop>>
	<br><br>
<</if>>
<<endevent>><<npc Doren>><<person1>>
You continue to jog through town, and end up back at Barb Street. You stop outside Doren's building. "Thanks for coming along," <<he>> pants. "You can come up and shower if you like. I don't mind waiting."
<<physique 3>>
<br><br>
<<link [[Shower (0:20)|Doren Shower]]>><<pass 20>><</link>>
<br>
<<link [[Say goodbye|Doren Shower Refuse]]>><</link>>
<br>