<<effects>>
<<if $submissive gte 1150>>
	"I-I'll do what you ask," you say. "J-Just don't hurt me. Please."
<<elseif $submissive lte 850>>
	"Look," you say. "I know how this works. I'll play along, lets just not waste any more of my time than we have to."
<<else>>
	"I'll cooperate," you say. "Just be gentle."
<</if>>
<br><br>
"You've got a good head on your shoulders," the <<person1>><<person>> says, pouring <<himself>> a glass of scotch. "Better than the last one."
<br><br>
<<pass 2>>
The pair whisper to themselves for a couple of minutes, with occasional furtive glances in your direction. They reach an agreement at last. "We've got some work for you."
<br><br>
<<link [[Next|Sold Work]]>><</link>>
<br>