<<set $outside to 0>><<set $location to "town">><<effects>>

<<set $seductiondifficulty to 4000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

You look Leighton in the eyes. "You must know much more about it than me," you whisper. "Maybe you can teach me in private?" Sirris looks away and blushes.
<br><br>

<<if $seductionrating gte $seductionrequired>>
	<<set $sciencefaircheat += 10>>
	<span class="green">Leighton leans close.</span> "Maybe you could use some-," <<he>> pauses. "Personal tuition." <<He>> glances around as if remembering where <<he>> is and coughs. "That's enough," <<he>> says. They move on to the next project.
	<br><br>

<<else>>
	<span class="red">Leighton laughs.</span> "Nice try," <<he>> says. They move on to the next project.
	<br><br>

<</if>>

<<link [[Next|Science Fair Finish]]>><</link>>
<br>