<<effects>>

You call the <<farm_text_many horse>>, and lead them from the field. They seem happy enough to leave.
<br><br>

<<set $danger to random(1, 10000)>>
<<if $danger gte (9900 - $allure)>>
	You lock each one in their stall for the night. The last decides to be difficult, and insists on remaining outside. <<farm_He horse>>'s too big to move by force.
	<br><br>
	
	<<link [[Encourage|Farm Horses In Encourage]]>><</link>><<tendingdifficulty 1 800>>
	<br>
	<<link [[Call Alex|Farm Horses In Alex]]>><<npcincr Alex dom 1>><<farm_horses -1>><</link>><<lrespect>><<gdom>>
	<br>
	<<if $bestialitydisable is "f" and $deviancy gte 15>>
		<<link [[Seduce|Farm Horses In Seduce]]>><</link>><<deviant2>>
	<</if>>
	<br>

<<else>>
	You lock each one in their stall for the night.
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<</if>>