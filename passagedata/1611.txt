<<set $outside to 0>><<set $location to "town">><<effects>>

<<if $player.gender_appearance is "f">>
The <<person>> laughs. "I meant the other one," <<he>> says. "But as you like. You're the star."
<<else>>
I bet it'll look cute on you," the <<person>> says.
<</if>>
<br><br>

<<upperwear 15>>
<<lowerwear 16>>

You get dressed in a large cupboard. The <<person1>><<person>> claps when <<he>> sees you. They've set up a tall mirror, and pushed a chair in front of it.
<br><br>

You sit still as the <<person>> and <<person2>><<person>> carefully apply makeup and trim your hair. It takes a while. "Sam wants you looking your best," the <<person2>><<person>> says. "A lot of people will be there."
<br><br>

<<pass 60>>

<<link [[Next|Chef Opening 4]]>><</link>>
<br>