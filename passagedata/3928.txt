<<effects>>

<<if $submissive gte 1150>>
"It's okay," you say. "You don't need to be nervous."
<<elseif $submissive lte 850>>
"You better not be going easy on me," you say. "I'd be insulted."
<<else>>
"You can do it," you say. "I've seen you play."
<</if>>
<br><br>

Kylar's shaking eases a little, but you still win with ease.
<br><br>
<<kylaroptions>>