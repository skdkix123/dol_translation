<<effects>>

The other couple are up first. The musicians begin a new piece, faster still than anything they've played yet. The couple manage to keep up.

<<if $danceskill gte 800>>
	They're good. Really good. But you think you're better.
<<elseif $danceskill gte 400>>
	They're good. Really good. Winning would be a challenge.
<<else>>
	They're good. Really good. You don't like your chances.
<</if>>
<br><br>

<<person3>><<if $pronoun is "m">>
	The <<person>> looks smart in <<his>> suit, but you notice they avoid any moves that would put any strain on it. Even when the dance calls for it.
<<else>>
	The <<persons>> skirt flares as <<he>> moves, but you notice they avoid any moves that might flare it too high. Even when the dance calls for it.
<</if>>
<br><br>

<<link [[Compete fairly|Avery Party Dance Fair]]>><</link>><<dancedifficulty 1 1000>>
<br>
<<if $promiscuity gte 15>>
	<<link [[Cheat|Avery Party Dance Cheat]]>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<promiscuous2>><<glove>><<gendear>>
	<br>
<</if>>