<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

Looking under the head's desk you find a small but solid box with a door. It's almost a safe. You pull, but the door doesn't move. It's locked. The lock looks very solid:
it will take some time to pick.
<br><br>
<<set $skulduggerydifficulty to 1000>>

<<link [[Pick the Lock (0:30)|Head's Stash 2]]>><<pass 30>><</link>><<skulduggerydifficulty>>
<br>
<<link [[Leave it alone|Head's Office]]>><<endevent>><</link>>
<br>