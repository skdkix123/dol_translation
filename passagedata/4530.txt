<<effects>>

You take a deep breath, and dash across the bridge.
	<br><br>
<<if $rng gte 81>>
	<<generate1>><<generate2>><<person1>>
	You're halfway along when a <<person>> and <<person2>><<person>> walk around a hedge, heading this way. You'll be seen.
	<br><br>

	<<link [[Jump off the bridge|Park Ex Bridge Jump]]>><<pain 6>><</link>><<gpain>>
	<br>
	<<link [[Cover yourself and continue|Park Ex Bridge Cover]]>><<stress 6>><</link>><<gstress>>
	<br>
<<else>>
	You make it to the other side and take cover within the trees.
	<br><br>
	<<destinationeventend>>

<</if>>