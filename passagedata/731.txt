<<effects>>

<<if $farm.beasts.pigs lte -5 and $rng gte 51>>

	<<if $farm_work.pig.monster is true>>
		You endure as the <<farm_text pig>> humps you. Unsatisfied, <<farm_he pig>> wrestles you to the ground.
	<<else>>
		You endure as the pig humps you. Its weight forces you to the ground.
	<</if>>
	<br><br>
	
	<<link [[Next|Farm Pigs Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>
	<<if $farm_work.pig.monster is true>>
		You endure as the <<farm_text pig>> humps you. <<farm_He pig>> soon tires, and steps away. You close the pen after <<farm_him pig>>
	<<else>>
		You endure as the pig humps you. It soon tires, or is otherwise satisfied, and drops to the ground. You close the pen after it.
		<br><br>
	<</if>>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>

<</if>>