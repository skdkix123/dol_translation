<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $submissive gte 1150>>
You don't say anything. You just stare at your feet. Bailey looks away from you. You realise that, for the first time since your arrival, you're no longer the centre of attention.
<<elseif $submissive lte 850>>
You glare at Bailey, but don't say anything. <<He>> looks away from you. You realise that, for the first time since your arrival, you're no longer the centre of attention.
<<else>>
You remain silent. Bailey seems satisfied. <<He>> looks away from you. You realise that, for the first time since your arrival, you're no longer the centre of attention.
<</if>>
<br><br>
<<endevent>>
<<link [[Look|Chef Opening 7]]>><</link>>