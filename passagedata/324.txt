<<effects>>

Much of grounds are untended, with wild poppies growing from the exterior fence all the way up to the building. You gather some seeds.
<br><br>

<span class="gold">You can now grow poppies.</span>
<br><br>

<<link [[Next|Asylum Garden]]>><</link>>
<br>