<<set $outside to 1>><<set $location to "forest">><<effects>>

"When I found the picnic basket I got attacked by a giant snake. It tried to eat me!"
<br><br>

Robin rushes to your side, looking panicked. "I'm so sorry," <<he>> says. "I shouldn't have let you go on your own." <<He>> makes a face that is both awed and exasperated when <<he>> sees the basket in your hands. "You got the basket. You're so amazing."
<br><br>

<<if $exposed gte 1>>
Robin gives you the rug to cover with and together you return to the orphanage.
<br><br>

<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>
<<else>>
Together you return to the orphanage.
<br><br>

<<robinoptions>>
<</if>>