<<effects>>

<<set $seductiondifficulty to 2000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>>
	<span class="gold">You feel more confident in your powers of seduction.</span>
	<br><br>
<</if>>
<<seductionskilluse>>

You lean over the counter. "Meet me outside," you whisper. "Behind the shop."
<<promiscuity1>>

<<if $seductionrating gte $seductionrequired>>

	The <<person>> glances out the window, and <span class="green">nods.</span> <<He>> disappears through a door behind <<him>>.
	<br><br>
	
	You pick up the basket, leave through the front, and stroll back to the farm.
	<br><br>
	
	<<link [[Next|Farm Shopping End]]>><</link>>
	<br>

<<else>>

	"Outside like a pair of dogs?" the <<person>> laughs. "<span class="red">No chance.</span> Get on the counter."
	<br><br>
	
	<<link [[Pay with money|Farm Shopping Pay]]>><</link>>
	<br>
	<<link [[Snatch and run|Farm Shopping Steal]]>><<crimeup 50>><</link>><<crime>><<athleticsdifficulty 1 600>>
	<br>
	<<if $promiscuity gte 15>>
		<<link [[Pay with your body|Farm Shopping Prostitution]]>><</link>><<promiscuous2>>
		<br>
	<<elseif $uncomfortable.prostituting is false>>
		<<link [[Pay with your body|Farm Shopping Prostitution]]>><<set $desperateaction to 1>><</link>><<promiscuous2>>
		<br>
	<</if>>
<</if>>