<<set $outside to 0>><<set $location to "hospital">><<effects>>

<<npc Harper>><<person1>>

You knock on the door, and hear Harper's voice on the other side. "Come in." You enter.
<br><br>

<<if $harperintro isnot 1>>
<<set $harperintro to 1>>
<<He>> sits at <<his>> desk in front of a computer. <<He>> looks away from the screen, "It's good that you've come." <<He>> motions for you to take a seat, you do so. "Don't worry, we won't be doing anything too invasive today."
<br><br>
<<set $pills to 1>>

You wonder what <<he>> means by "invasive". <<He>> interrupts your musing. "<span class="gold">I'd like to prescribe some medication. I'll have it sent to your home</span>." <<He>> starts filling out a form, but doesn't stop speaking. "There's something else I'd like to try. Please, sit back and relax."
<br><br>

<<He>> puts down the paper and swivels to face you. <<His>> voice has taken on a soothing quality. "I want you to breathe deeply." <<He>> holds up <<his>> pen. "Focus on the pen. There's nothing in the universe except this pen, my voice, and your own breath."
<br><br>

This continues for several minutes. Harper somehow picks up on whenever your mind starts to wander, and brings you back to focus. You start to feel something shift in your consciousness, as if you're entering a trance.
<br><br>

<<link [[This feels nice|Doctor Harper's Office Trance]]>><<pass 10>><<set $phase to 1>><<npcincr Harper dom 5>><<control 25>><<set $trance to 1>><<trauma -6>><<stress -12>><<awareness -10>><</link>><<ggcontrol>><<llawareness>><<ltrauma>><<lstress>>
<br>
<<link [[Resist|Doctor Harper's Office Trance]]>><<pass 10>><<stress 6>><<npcincr Harper dom -5>><</link>><<gstress>>

<<elseif $rng gte 51>>

"Please, sit down and relax." You do so, and <<he>> swivels to face you. <<His>> voice takes on a soothing quality. "I want you to breathe deeply." <<He>> holds up <<his>> pen. "Focus on the pen. There's nothing in the universe except this pen, my voice, and your own breath."
<br><br>

This continues for several minutes. Harper somehow picks up on whenever your minds starts to wander, and brings you back to focus. You start to feel something shift in your consciousness, as if you're entering a trance.
<br><br>

<<link [[This feels nice|Doctor Harper's Office Trance]]>><<pass 10>><<control 25>><<set $trance to 1>><<set $phase to 1>><<npcincr Harper dom 5>><<trauma -6>><<stress -12>><<awareness -10>><</link>><<ggcontrol>><<llawareness>><<ltrauma>><<lstress>>
<br>
<<link [[Resist|Doctor Harper's Office Trance]]>><<pass 10>><<stress 6>><<npcincr Harper dom -5>><</link>><<gstress>>

<<else>>

	<<if $harperexam is 0>>
	<<set $harperexam to 1>><<upperstrip>><<lowerstrip>>

		<<if !$worn.under_lower.type.includes("naked")>>

"Please strip down to your underwear and lie down on the bed." You hesitate. <<He>> notices your trepidation and smiles at you. "Don't worry, it's normal to be embarrassed, but there's no reason to be." Despite your anxiety, you remove your clothes and lie on the bed. Harper rolls <<his>> chair over to your side. "I'm going to examine you, just relax." <<He>> smiles. "I promise I'll be gentle."
<br><br>

		<<else>>
"Please strip down to your underwear and lie down on the bed." You hesitate. <<He>> notices your trepidation and smiles at you. "Don't worry, it's normal to be embarrassed, but there's no reason to be." You can't help but feel conscious of your lack of underwear.
<br><br>

Harper looks about to speak again when you blurt out.
<<if $worn.genitals.type.includes("naked")>>
	"I'm not wearing anything underneath this!"
<<else>>
	"I've only got a $worn.genitals.name underneath this!"
<</if>>
<br><br>

<<He>> looks at you blankly for a moment, then laughs. "I didn't realise you were that sort of person." You shift uncomfortably. "It's okay though, you need to get used to your doctor seeing those parts of you anyway. Come on, don't be shy."
<br><br>

You remove your clothing with shaking fingers, then lie on the bed with your hands covering your <<genitalsstop>> Harper rolls <<his>> chair over to your side. "I'm going to examine you, just relax." <<He>> smiles. "I promise I'll be gentle."
<br><br>

		<</if>>

<<He>> rubs <<his>> fingers against your shoulders, slowly working <<his>> way down your chest. <<He>> gives your nipples extra attention for some reason. It's hard not to squirm at the sensation.
<br><br>

<<He>> continues further down your body, inching closer to <<if !$worn.under_lower.type.includes("naked")>>your $worn.under_lower.name.<<elseif $worn.genitals.type.includes("chastity")>>your $worn.genitals.name.<<else>>where your hands protect your <<genitalsstop 1>> <<He>> looks at you and asks. "Could you move your hands for me? It's important I check beneath them." You move your hands aside, exposing yourself. You stare at the ceiling and try to stay calm.<</if>>

<br><br>

<<link [[Next|Doctor Harper's Office Exam]]>><<set $phase to 1>><</link>>
<br>

	<<elseif $harperexam is 1>>
	<<set $harperexam to 2>><<strip>>
"I need to be more thorough this time. Please remove your clothing and lie down on the bed." You hesitate. All your clothing? <<He>> notices your trepidation and smiles at you. "Don't worry, it's normal to be embarrassed, but there's really no reason to be."
<br><br>

You remove your clothing with shaking fingers, then lie on the bed with your hands covering your <<genitalsstop>> Harper rolls <<his>> chair over to your side. "I'm going to examine you, just relax." <<He>> smiles. "I promise I'll be gentle."
<br><br>

<<He>> rubs <<his>> fingers against your shoulders, slowly working <<his>> way down your chest. <<He>> gives your nipples extra attention for some reason. It's hard not to squirm at the sensation.
<br><br>

<<He>> continues further down your body, inching closer to where your hands protect your <<genitalsstop>> <<He>> looks at you and asks. "Could you move your hands for me? It's important I check beneath them." You move your hands aside, exposing yourself. You stare at the ceiling and try to stay calm.
<br><br>

<<link [[Next|Doctor Harper's Office Exam]]>><<set $phase to 1>><</link>>
<br>

	<<else>>
	<<strip>>

<<generate2>>You and the doctor aren't alone. A young <<person2>><<person>> sits in the corner of the room, looking slightly nervous. Harper introduces <<himstop>> "This is a med student, <<he>> will be helping me today."
<br><br>

Harper doesn't wait for a response. "Please remove your clothing and lie down on the bed." You feel embarrassed to be undressing in front of two people, but you assure yourself that it's fine. This is a medical check-up after all.
<br><br>

<<person1>>You remove your clothing with shaking fingers, then lie on the bed with your hands covering your <<genitalsstop>> Harper rolls <<his>> chair over to your side. "I'm going to examine you, just relax." <<He>> smiles. "I promise I'll be gentle." You can feel the <<person2>><<person>> staring at your exposed skin.
<br><br>

<<person1>>Harper commences <<his>> usual probing. As <<he>> reaches your pelvis, <<he>> asks "Could you move your hands for me?" You move your hands aside, exposing yourself. You stare at the ceiling and try to stay calm.
<br><br>

<<link [[Next|Doctor Harper's Office Exam]]>><<set $phase to 2>><</link>>
<br>

	<</if>>

<</if>>