<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>

You pull against the wall, and with a tearing sound go tumbling to the ground.
<<set $worn.lower.integrity -= 30>>

<<if $worn.lower.integrity lte 0>>

You look up and see your $worn.lower.name still stuck to the paint, the whole thing torn apart and ruined.
<<lowerruined>><<clothesruinstat>><<set $loweroff to 0>>

"Is everything okay in there?" you hear the <<person1>><<person>> ask. "I thought I heard-" <<he>> cuts off as <<he>> enters the room and sees you lying there, your <<undies>> on display. You blush as you cover your <<lewdness>> and stand up. "I'll get something for you to wrap up with," <<he>> says, clearly stifling laughter.
<br><br>

<<He>> leaves for a few moments, then returns with a towel. "Don't worry about that," <<he>> says, gesturing at the remains of your clothing. "I'll get it off later."
<br><br>

<<He>> leaves you to get back to work, no doubt aching to tell the <<person2>><<person>> all about what <<person1>><<he>> just witnessed.
<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
<br><br>

<<towelup>>

<<else>>

Your clothes didn't fare well, but at least they're still intact.
<br><br>

<</if>>

<<pass 2 hours>><<pass 2 hours>>After some arduous hours, you stand back to admire your finished handiwork.
<br><br>

You've earned £50.
<<set $money += 5000>>
<br><br>

<<link [[Next|Domus Street]]>><<endevent>><</link>>
<br><br>