<<set $outside to 1>><<effects>>
<<if $physique gte random(3000, 8000)>>
	You shake off their hands and run. You hear a whistle blow behind you. The <<person3>><<person>> <span class="green">dodges out of your way.</span> You outpace the officers, and they soon give up.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	You shake off their hands and run. You hear a whistle blow behind you. Looking back, <span class="red">you run straight into the <<person3>><<person>> and fall to the floor.</span> Before you can recover, they have you.
	<br><br>
	<<link [[Next|Street Police Search Lewd]]>><<set $phase to 0>><</link>>
	<br>
<</if>>