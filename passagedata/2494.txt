<<set $outside to 0>><<set $location to "pub">><<effects>>

<<if $phase is 0>><<set $money -= 50000>>

They take the money, careful not to let anyone else see. "What a shame we couldn't find you," the <<person>> says. "Be careful <<girlcomma>> this won't buy our incompetence for long." They turn and leave.
<br><br>

<<endevent>>

<<link [[Next|Pub]]>><</link>>
<br>

<<else>>
<<set $location to "brothel">>
They lead you out onto the street. They stay close to you, making sure you have no opportunity to escape. You arrive at the Brothel, and they push you down the staircase beside it. They push you along a corridor lined with doors on both sides until you come to an open one at the end. They push you through.
<br><br>

You enter a small room. Briar is there, flanked by two more thugs.<<endevent>> <<npc Briar>><<person1>>"I tried to warn you," <<he>> says. <<His>> thugs pick you up by the legs, dropping your head and chest to the ground.

	<<if $worn.lower.skirt is 1>>
	Your $worn.lower.name slip down your thighs, baring your <<undiesstop>>
	<</if>>

Briar pulls a lever and a hole opens in the wall a few feet off the ground.
<br><br>

The thugs push your legs through, then your <<bottomcomma>> until your back half hangs out the other side. Briar manipulates the lever a bit, and the hole tightens around your waist. <<He>> kneels in front of you and lifts your chin. "You're going to pay me back the only way you can." <<He>> stands up and leaves the room, taking <<his>> thugs with <<himstop>>
<<set $leftarm to "bound">><<set $rightarm to "bound">>
<br><br>
<<endevent>>
You feel something prod your behind.
<br><br>
<<set $punishmenthole to 10>>
<<link [[Next|Brothel Punishment Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>