<<set $outside to 1>><<set $location to "moor">><<effects>><<set $bus to "farmroad5">>

<<if $phase is 1>>
	<<set $phase to 0>>
	You walk deeper into the countryside. The hills become flatter, and the ground more barren. Brown bushes struggle through the earth. Strange, thorny growths strangle the trees.<<physique 3>>
<<elseif $phase is 2>>
	<<set $phase to 0>>
	You walk towards town. The fields give way to a barren, tortured plain. Brown bushes struggle through the earth. Strange, thorny growth strangle the trees.<<physique 3>>
<<else>>
	You are on the road that connects the town and surrounding farmlands. The road is surrounded by a barren plain. Brown bushes struggle through the earth. Strange, thorny growths strangle the trees.
<</if>>
<br><br>

<<set $danger to random(1, 10000)>>
<<if $stress gte 10000>>
	<<passoutfarmroad>>
<<elseif $danger gte (9900 - $allure) and $eventskip is 0>>
	<<if $rng gte 51>>
		You hear a terrible screech.<<gstress>><<stress>>
		<br><br>

		<<destinationfarmroad>>
	<<else>>
		<<hitchhike>>
	<</if>>
<<else>>
	<<if $exposed lte 0>>
		<<link [[Hitchhike (0:15)|Farm Hitchhike]]>><<pass 15>><</link>>
		<br>
	<</if>>
	<<link [[Walk deeper into the countryside (0:30)|Farm Road 6]]>><<pass 30>><<set $phase to 1>><<tiredness 3>><</link>><<gtiredness>>
	<br>
	<<link [[Walk towards town (0:30)|Farm Road 4]]>><<pass 30>><<set $phase to 2>><<tiredness 3>><</link>><<gtiredness>>
	<br>
<</if>>

<<set $eventskip to 0>>