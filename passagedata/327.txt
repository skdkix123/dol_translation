<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<if $phase is 1>>

You mock cough, "ahem." <<He>> looks at you, bewildered for a moment before realising what you're after. "Oh. Thanks," <<he>> goes back to eating.
<br><br>

<<link [[Request head pats|Eden Breakfast 2]]>><<npcincr Eden love 1>><<set $phase to 4>><<trauma -6>><<stress -12>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Chat|Eden Breakfast 2]]>><<trauma -6>><<stress -12>><<set $phase to 2>><</link>><<ltrauma>><<lstress>>
<br>
<<link [[Sit quietly|Eden Breakfast 2]]>><<set $submissive += 1>><<set $phase to 3>><</link>>
<br>
	<<if $promiscuity gte 55>>
	<<link [[Slip under the table|Eden Table Seduction]]>><</link>><<promiscuous4>>
	<br>
	<</if>>

<<elseif $phase is 2>>

You try to chat with Eden, but <<he>> seems more interested in the food. You can tell <<hes>> listening though. "You can have the leftovers," <<he>> says, pushing the plate towards you. "I need to get ready."
<br><br>

<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>

<<elseif $phase is 3>>

You sit quietly as Eden eats. <<He>> seems quite engrossed, until <<he>> glances at you. "You can have the leftovers," <<he>> says, pushing the plate towards you. "I need to get ready."
<br><br>

<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>

<<else>>

You rest your head on <<his>> shoulder. <<He>> takes the hint and starts stroking your hair.

	<<if $wolfgirl gte 4>>
	<<His>> fingers brush against your wolf ears and a small yelp escapes your lips. Drawn to your weak spot, <<he>> scratches behind your ear. Waves of pleasure jolt through your scalp, down your neck and through your spine. Your leg jerks involuntarily. "You like it there, don't you."
	<<garousal>><<lstress>><<arousal 600>><<stress -12>><<arousal 1000>>
	<br><br>

		<<if $arousal gte 10000>>
		<<orgasmpassage>>

		The spasms subside, but leave you tired and as weak as a puppy. "My fault," <<he>> says, bemused. "I went too hard on you." <<He>> kisses your forehead and holds you close.
		<br><br>

		<<else>>
		Just when you feel like it's too much for you, <<he>> stops. <<He>> wraps <<his>> arm around you and just holds you for a few minutes.

		<</if>>
	<</if>>

"I need to get ready," <<he>> says after a while. "You can finish the food."
<br><br>

<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
<br>

<</if>>