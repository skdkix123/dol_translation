<<set $outside to 0>><<set $location to "home">><<effects>>
<<set $stress -= 2000>>
<<npc Bailey>><<person1>>You're awoken by a sharp pain in your face. It's Bailey. <<He>> slaps you again. "I provide a perfectly good bed for you, yet you opt to sleep on the floor." <<He>> slams the door shut as <<he>> leaves.
<br><br>
<<set $pain += 20>>
<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>