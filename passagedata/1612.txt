<<set $outside to 0>><<set $location to "town">><<effects>>

"You look amazing," the <<person1>><<person>> says once they're finished.
<br><br>

A limo waits for you outside. A <<generate3>><<person3>><<person>> opens the door when <<he>> sees you. "People are arriving," <<he>> says. "Sam's waiting."
<br><br>
<<endevent>>

<<link [[Next|Chef Opening 5]]>><</link>>
<br>