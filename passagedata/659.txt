<<set $outside to 1>><<set $location to "forest">><<effects>>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

You sneak up to the cave. You think you hear the bear snore. <span class="green">You sneak behind it.</span> The rest of the pack inches closer. One of them snaps a twig, and the bear's eyes shoot open. You throw a rock at it, and it turns to face you. It rears up and roars, unaware of the wolves right behind it. They attack it from behind, biting at its limbs and clawing at its back.
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

Groggy and surrounded, the bear stumbles away from the cave and leaves the deer carcass unguarded. The black wolf keeps the choice parts for itself. You find some berries growing nearby which serve you better.
<<stress -12>><<lstress>>
<br><br>

	The pack lazes around, eating at their leisure. A small dispute breaks out and the black wolf leaves its place by the deer to quell it.
	<br><br>

<<wolfpackhuntoptions>>

<<else>>

You sneak up to the cave. You think you hear the bear snore. <span class="red">You're right beside it when it opens it's eyes and looks straight at you!</span> The wolves flee into the night and the bear rears over you.
<br><br>

<<link [[Next|Wolf Bear Rape]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>