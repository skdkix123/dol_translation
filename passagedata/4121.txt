<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $trauma gte ($traumamax / 7) * 3>>
You complain about some of your recent worries. Robin listens to what you have to say but you hold back on some of the more significant happenings. Despite that, just talking about the little things is enough to make life feel a little easier to deal with.
<br><br>
<<robinhug>>

<<else>>
You complain about some of your recent worries. Robin listens to what you have to say and the experience makes it all a little easier to deal with.
<br><br>
<<robinhug>>

<</if>>