<<set $outside to 1>><<set $location to "forest">><<effects>>
<<if $foresthunt gte 1>>
	<span class="red">You are being hunted.</span>
	<br><br>
<</if>>
<<if $arousal gte 10000>>
	<<orgasmforest>>
<</if>>
<<if $forest lte 0>>
	<<set $forest to 0>>
	<<forestmove>>
	<br><br>
	<<if $stress gte 10000>>
		<<passoutforest>>
	<<elseif $foresthunt gte 10>>
		<<foresthunt>>
	<<else>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $eventskip is 0>>
			<<if $danger gte (9900 - ($allure * $forestmod))>>
				<<eventforestoutskirts>>
			<<else>>
				<<eventforestsafe>>
			<</if>>
		<<elseif $eventskip gte 1>>
			You are in the forest outskirts. You can make out the town between the trees.
			<br><br>
			<<if $exposed lte 1>>
				<<if $forest_shop_intro is 1>>
					<<link [[Forest shop|Forest Shop]]>><</link>>
					<br><br>
				<<else>>
					<<link [[Strange Shop|Forest Shop]]>><</link>>
					<br><br>
				<</if>>
			<</if>>
			<<forestdeeper>>
			<br><br>
			<<forestsearch>>
			<br><br>
			Leave the forest
			<br>
			<<danubeicon>><<link [[Danube Street (0:10)|Danube Street]]>><<pass 10>><<set $foresthunt to 0>><</link>>
			<br>
			<<wolficon>><<link [[Wolf Street (0:10)|Wolf Street]]>><<pass 10>><<set $foresthunt to 0>><</link>>
			<br>
			<<nightingaleicon>><<link [[Nightingale Street (0:10)|Nightingale Street]]>><<pass 10>><<set $foresthunt to 0>><</link>>
			<br>
			<<if $temple_rank isnot undefined and $temple_rank isnot "prospective">>
				<<link [[Temple (0:10)|Temple Garden]]>><<pass 10>><<set $foresthunt to 0>><</link>>
				<br>
			<</if>>
			<br><br>
			<<if $historytrait gte 3>>
				<<link [[Take secret path deeper into the woods (0:10)|Forest]]>><<set $forestmod to 1.5>><<set $forest += 50>><<set $forestmove to "secretin">><<pass 10>><<if $foresthunt gte 1>><<set $foresthunt += 1>><</if>><</link>>
				<br>
			<</if>>
		<</if>>
	<</if>>
<<elseif $forest lte 20>>
	<<forestmove>>
	<br><br>
	<<if $stress gte 10000>>
		<<passoutforest>>
	<<elseif $foresthunt gte 10>>
		<<foresthunt>>
	<<else>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $eventskip is 0>>
			<<if $danger gte (9900 - ($allure * $forestmod))>>
				<<eventforestoutskirts>>
			<<else>>
				<<eventforestsafe>>
			<</if>>
		<<elseif $eventskip gte 1>>
			You are in the forest. The trees are quite sparse, you can't be too far from town.
			<br><br>

			<<if $smuggler_location is "forest" and $smuggler_timer is 0 and $daystate is "night" and $hour gte 18>>
				<span class="gold">You see a flickering through the trees. Like a flame.</span>
				<br><br>
			<</if>>
			<<if $smuggler_location is "forest" and $smuggler_timer is 0 and $daystate is "night" and $hour gte 18>>
				<<link [[Approach flame|Smuggler Forest]]>><</link>>
				<br><br>
			<</if>>
			<<forestdeeper>>
			<br><br>
			<<forestsearch>>
			<br><br>
			<<forestlessdeep>>
			<br><br>
		<</if>>
	<</if>>
<<elseif $forest lte 50>>
	<<forestmove>>
	<br><br>
	<<if $stress gte 10000>>
		<<passoutforest>>
	<<elseif $foresthunt gte 10>>
		<<foresthunt>>
	<<else>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $eventskip is 0>>
			<<if $danger gte (9900 - ($allure * $forestmod))>>
				<<eventforest>>
			<<else>>
				<<eventforestsafe>>
			<</if>>
		<<elseif $eventskip gte 1>>
			You are in the forest, surrounded by nature.
			<br><br>
			<<if $forest lte 30>>
				<<link [[Lake (0:10)|Lake Shore]]>><<pass 10>><</link>>
				<br>
			<<elseif $forest lte 40>>
				<<link [[Lake (0:10)|Lake Waterfall]]>><<pass 10>><</link>>
				<br>
			<<else>>
				<<link [[Lake (0:10)|Lake Fishing Rock]]>><<pass 10>><</link>>
				<br>
			<</if>>
			<br>
			<<forestdeeper>>
			<br><br>
			<<forestsearch>>
			<br><br>
			<<forestlessdeep>>
			<br><br>
			<<if $historytrait gte 3>>
				<<link [[Take secret path out of the woods (0:10)|Forest]]>><<set $forestmod to 1.5>><<set $forest -= 50>><<set $forestmove to "secretout">><<pass 10>><<if $foresthunt gte 1>><<set $foresthunt += 1>><</if>><</link>>
				<br>
			<</if>>
		<</if>>
	<</if>>
<<elseif $forest lte 99>>
	<<forestmove>>
	<br><br>
	<<if $stress gte 10000>>
		<<passoutforest>>
	<<elseif $foresthunt gte 10>>
		<<foresthunt>>
	<<else>>
		<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $eventskip is 0>>
			<<if $danger gte (9900 - ($allure * $forestmod))>>
				<<eventforestdeep>>
			<<else>>
				<<eventforestsafe>>
			<</if>>
		<<elseif $eventskip gte 1>>

			You are deep in the forest. The trees are tall and dark, their branches twisting together to form the canopy.
			<br><br>

			<<if $edenfreedom gte 1>>
				<<link [[Eden's Cabin (0:10)|Eden Clearing]]>><<pass 10>><</link>>
				<br>
			<<elseif $syndromeeden gte 1>>
				<<link [[Eden's Cabin (0:10)|Eden Clearing]]>><<pass 10>><</link>>
				<br>
			<</if>>
			<<if $syndromewolves gte 1>>
				<<link [[Wolf Cave (0:10)|Wolf Cave Clearing]]>><<pass 10>><</link>>
				<br>
			<</if>>
			<<if $pubtask is 1 and $pubtask2 isnot 1>>
				<<set $rng to random(1, 100)>>
				<<if $pubtasksetting is "bear" and $bestialitydisable is "f">>
					<<link [[Search for Landry's black box|Forest Bear Box]]>><</link>>
					<br>
				<<elseif $pubtasksetting is "pair">>
					<<link [[Search for Landry's black box|Forest Box]]>><</link>>
					<br>
				<<elseif $rng gte 51 and $bestialitydisable is "f">>
					<<link [[Search for Landry's black box|Forest Bear Box]]>><</link>>
					<br>
				<<else>>
					<<link [[Search for Landry's black box|Forest Box]]>><</link>>
					<br>
				<</if>>
			<</if>>
			<br>
			<<forestdeeper>>
			<br><br>
			<<forestsearch>>
			<br><br>
			<<forestlessdeep>>
			<br><br>
		<</if>>
	<</if>>
<<elseif $forest gte 100>>
<<set $forest to 100>>
<<forestmove>>
<br><br>

	<<if $stress gte 10000>>
	<<passoutforest>>
	<<elseif $foresthunt gte 10>>
	<<foresthunt>>
	<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
		<<if $eventskip is 0>>
			<<if $danger gte (9900 - ($allure * $forestmod))>>
			<<eventforestdeep>>
			<<else>>
			<<eventforestsafe>>
			<</if>>
		<<elseif $eventskip gte 1>>

You are deep in the forest. You can make out the asylum through the trees.
<br><br>

<<forestsearch>>
<br><br>

<<forestlessdeep>>
<br><br>

		<</if>>
	<</if>>

<</if>>
<<set $eventskip to 0>>