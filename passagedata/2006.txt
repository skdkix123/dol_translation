<<effects>>

<<if $submissive gte 1150>>
"I'm sorry," you say. "I couldn't watch you put yourself in danger."
<<elseif $submissive lte 850>>
"Have some self respect," you say. "You can do better than that."
<<else>>
"I'm sorry," you say. "I had to do what I felt was right."
<</if>>
<br><br>

<<He>> clenches <<his>> fists, but doesn't hit you again. <<He>> runs inside.
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>