<<effects>>

<<if $livestock_horse gte 2>>
	You hold out the basket of apples, and soon find yourself surrounded. One tries to sneak a second, until an elder chases them off with a snort.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>
<<else>>
	<<set $livestock_horse to 2>>
	You hold out the basket of apples. The horses stare in shock at such a bounty. They trot closer, but carefully, as if afraid it's a trick. A younger horse chances it first. It pulls an apple from the basket, where it falls to the ground. It takes a bite, and neighs happily.
	<br><br>
	The other horses crowd around, none wanting to be left out. There's a little jostling, but they're good at sharing the fruit evenly.
	<br><br>

	The apples soon vanish, but the horses have a lightness in their step that wasn't present before. More than one tries to lick your face.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>

<</if>>