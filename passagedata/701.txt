<<set $outside to 1>><<effects>>

You walk down a thin road near the docks, leading away from Mer Street. It feeds into a warehouse, but to one side is a sandy trail, hidden among tufts of spiky grass. It takes you into the hills that flank the town, snaking the tops of the cliffs facing the sea.
<br><br>

Few know the path is here.
<br><br>

<<link [[Next|Coast Path]]>><</link>>
<br>