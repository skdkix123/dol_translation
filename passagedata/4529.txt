<<set $outside to 1>><<set $location to "park">><<effects>>
<<if $rescue is 1 and $alarm is 1>>
	<<if $NPCList[0].stance is "top">>

	<<generate1>><<generate2>><<generate3>><<generate4>><<person1>>"It came from over there." You hear a <<if $pronoun is "m">>male<<else>>female<</if>> voice say. A <<person>> and three others appear from behind the bush. They stare at the dog humping you briefly, not sure what to do. The <<person>> is the first to speak. "Sorry to bother you. We thought we heard a scream. But it looks like you're just having some fun." <<He>> smirks.
	<br><br>

	<<if $submissive lte 850>>
	You glare at the <<personstop>> "Shut up, and get this thing off me!" The <<person>> looks dumbfounded at your response, but a <<person2>><<person>> laughs and pushes the dog off of you.
	<<else>>
	You stare at the ground as you speak. "Please help me." A <<person2>><<person>> steps forward and pushes the dog off of you.
	<</if>>
	<br><br>
	"Where are you clothes?" <<He>> says. "I have some towels you can have, but you shouldn't be walking around like that in the first place. Someone will hurt you." <<tearful>> you cover up and thank the <<personcomma>> before walking away. You hear them talking about you before you leave earshot.
	<<famebestiality 50>>
	<br><br>

	<<set $rescued += 1>>
	<<clothesontowel>>
	<<endcombat>>

	<<parkeventend>>
	<<else>>

	<<generate1>><<generate2>><<generate3>><<generate4>><<person1>>"It came from over there." You hear a <<if $pronoun is "m">>male<<else>>female<</if>> voice say. A <<person>> and three others appear from behind the bush. They pause briefly when they see you, before the <<person>> shouts at the dog, who whimpers and bounds away.
	<<fameexhibitionism 10>>
	<br><br>

	A <<person2>><<person>> steps forward. "Where are you clothes?" <<He>> says. "I have some towels you can have, but you shouldn't be walking around like that in the first place. Someone will hurt you." <<tearful>> you cover up and thank the <<personcomma>> before walking away. You hear them talking about you before you leave earshot.
	<br><br>

	<<set $rescued += 1>>
	<<clothesontowel>>
	<<endcombat>>

	<<parkeventend>>
	<</if>>
<<elseif $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>>

The beast grunts in satisfaction, and leaves you lying on the grass. <<tearful>> you look around to see if anyone saw you. You don't think they did.
<br><br>

<<clotheson>>
<<endcombat>>

<<parkeventend>>
<<else>>

The beast yelps and flees, its tail between its legs. <<tearful>> you look around to see if anyone saw you. You don't think they did.
<br><br>

<<clotheson>>
<<endcombat>>

<<parkeventend>>
<</if>>
<<set $eventskip to 1>>