<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<if $submissive gte 1150>>
	"Please don't tell anyone," you say. "What do I do to keep you quiet?"
<<elseif $submissive lte 850>>
	"I get it," you say. "What do you want?"
<<else>>
	"What do you want?" you ask.
<</if>>
<br><br>
<<if $rng gte 91>>
	"<span class="gold">£2000</span> should do," <<he>> says.
	<br><br>
	<<if $money gte 200000>>
		<<link [[Pay (£2000)|Chef Blackmail Pay]]>><<set $money -= 200000>><</link>>
		<br>
		<<link [[Say you can't afford it|Chef Blackmail Lie]]>><<set $chef_sus += 10>><</link>><<ggsuspicion>>
		<br>
		<<link [[Refuse|Chef Blackmail Refuse]]>><<set $chef_sus += 10>><</link>><<ggsuspicion>>
		<br>
	<<else>>
		<<link [[Say you can't afford it|Chef Blackmail Truth]]>><</link>>
		<br>
		<<link [[Refuse|Chef Blackmail Refuse]]>><<set $chef_sus += 10>><</link>><<ggsuspicion>>
		<br>
	<</if>>
<<elseif $rng gte 51>>
	"<span class="gold">£500</span> should do," <<he>> says.
	<br><br>
	<<if $money gte 50000>>
		<<link [[Pay (£500)|Chef Blackmail Pay]]>><<set $submissive -= 1>><<set $money -= 50000>><</link>>
		<br>
		<<link [[Say you can't afford it|Chef Blackmail Lie]]>><<set $submissive -= 1>><<set $chef_sus += 10>><</link>><<ggsuspicion>>
		<br>
		<<link [[Refuse|Chef Blackmail Refuse]]>><<set $chef_sus += 10>><<set $submissive += 1>><</link>><<ggsuspicion>>
		<br>
	<<else>>
		<<link [[Say you can't afford it|Chef Blackmail Truth]]>><</link>>
		<br>
		<<link [[Refuse|Chef Blackmail Refuse]]>><<set $chef_sus += 10>><<set $submissive += 1>><</link>><<ggsuspicion>>
		<br>
	<</if>>
<<else>>
	<<He>> points down the alley. "How about I taste some of your cream," <<he>> says. "Straight from the source."
	<br><br>
	<<link [[Accept|Chef Blackmail Rape]]>><<set $molestationstart to 1>><<set $submissive += 1>><</link>>
	<br>
	<<link [[Refuse|Chef Blackmail Refuse]]>><<set $chef_sus += 10>><<set $submissive -= 1>><</link>><<ggsuspicion>>
	<br>
<</if>>