<<widget "eventshistory">><<nobr>>
<<if $rng gte 91>>
	<<npc Winter>><<person1>>Winter stops beside your desk.
	<<if $worn.neck.collared is 1>>
		"What is that around your neck?" <<he>> asks.
		<br><br>

		<<link [[Answer|History Lesson Collar]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
		<br>
		<<link [[Conceal it|History Lesson Conceal]]>><<detention 3>><</link>><<gdelinquency>>
		<br>
	<<elseif $cat gte 6 or $wolfgirl gte 4 or $demon gte 6>>
		"What are these on your head?" <<he>> asks.
		<br><br>

		<<if $demon gte 6>>
			<<link [[Answer honestly|History Lesson Horns]]>><</link>>
		<<else>>
			<<link [[Answer honestly|History Lesson Ears]]>><</link>>
		<</if>>
		<br>
		<<link [[Make Excuse|History Lesson Excuse]]>><</link>>
		<br>
	<<elseif $history gte 600>>
		"Good," <<he>> nods, reading over your shoulder. "Good work"<<stress -2>><<lstress>>
		<br><br>

		<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<<else>>
		You sense <<him>> reading over your shoulder.
		<br>
		<<He>> tuts loudly. "I guess we need to go over that part again."
		<br><br>
		Someone nearby sniggers.
		<<stress 2>><<gstress>>
		<br><br>

		<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 76>>
	<<generates1>><<person1>>You catch a normally shy <<person>> checking you out from across the room.
	<br><br>
	<<if $exhibitionism gte 35 and $worn.lower.skirt is 1 and !$worn.under_lower.type.includes("naked")>>
		<<link [[Flash|History Flash]]>><</link>><<exhibitionist2>>
		<br>
	<<elseif $exhibitionism gte 55 and $worn.lower.skirt is 1>>
		<<link [[Flash|History Flash]]>><</link>><<exhibitionist4>>
		<br>
	<<else>>
		<<link [[Tease|History Tease]]>><</link>><<exhibitionist1>>
		<br>
	<</if>>

	<<link [[Ignore|History Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $rng gte 52>>
	Winter has the class practise sourcing information from the various bookshelves around the edge of the room. As you move between the shelves, you feel a tug on your $worn.lower.name and hear them tear. In the bustle of the classroom, someone must have closed a cupboard on <<if $worn.lower.plural is 1>>them<<else>>it<</if>> without you noticing.
	<<set $worn.lower.integrity -= 10>>
	<<integritycheck>>
	<<exposure>>

	<<if !$worn.lower.type.includes("naked")>>
		<<gstress>><<stress 2>>
		<br><br>
	<<else>>
		<br><br>
		However, things are worse than you first assumed. You look down to find your <<undies>> completely exposed, much to the amusement of your classmates. You hasten to find a towel to cover yourself with, before your humiliation becomes too much to bear.
		<<trauma 2>><<gtrauma>><<stress 2>><<gstress>>
		<br><br>
		<<towelup>>
	<</if>>
	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>
<<elseif $rng gte 27>>
	<<npc Winter>><<person1>>Winter asks you to come to the front of the class. "I have some historical garments I need modelled, they're in your size." <<He>> gestures to the screen at the corner of the room.
	<br><br>

	<<if $delinquency gte 400>>
		Behind the screen you find a revealing traditional outfit. You hold it up for a closer look. It won't leave much to the imagination; there's barely enough fabric to conceal your <<breasts>> and crotch. It looks like it'll barely fit you, and the fabric is so taut you'll need to undress before putting it on.
		<br><br>

		<<link [[Put it on|History Events Revealing Dress]]>><<stress 6>><<trauma 6>><</link>><<gstress>><<gtrauma>>
		<br>
		<<link [[Refuse to wear it|History Events Refusal]]>><<detention 2>><</link>><<gdelinquency>>
		<br>

	<<elseif $delinquency gte 200>>
		<<if $player.gender_appearance is "m">>
			Behind the screen you find a traditional girl's outfit.
			<<if $player.gender is "f" or $player.gender is "h">>
				The class thinks you're a boy, so if you wear this in front of them, they'll think you're crossdressing.
			<</if>>
			It looks like it'll barely fit you, and the fabric is taut so you'll need to undress before putting it on.
			<br><br>

			<<link [[Put it on|History Events Dress]]>><<stress 2>><<trauma 2>><</link>><<gstress>><<gtrauma>>
			<br>
			<<link [[Refuse to wear it|History Events Refusal]]>><<detention 1>><</link>><<gdelinquency>>
			<br>
		<<elseif $player.gender_appearance is "f">>
			Behind the screen you find a traditional boy's outfit.
			<<if $player.gender is "m" or $player.gender is "h">>
				The class thinks you're a girl, so if you wear this in front of them, they'll think you're crossdressing.
			<</if>>
			It looks like it'll barely fit you, and the fabric is taut so you'll need to undress before putting it on.
			<br><br>

			<<link [[Put it on|History Events Dress]]>><<stress 2>><<trauma 2>><</link>><<gstress>><<gtrauma>>
			<br>
			<<link [[Refuse to wear it|History Events Refusal]]>><<detention 1>><</link>><<gdelinquency>>
			<br>
		<</if>>
	<<else>>
		Behind the screen you find
		<<if $history gte 500>>
			<<print either("a 19th century American merchant's outfit","a 17th century Caribbean privateer outfit","some late Victorian merchant's garb","a replica WWI officer's uniform")>>
		<<elseif $history gte 300>>
			<<print either("a bright, old-fashion outfit","a Pirate costume","clothes like people wear in old photos","a soldier costume")>>
		<<else>>
			some <<print either("old outfit","traditional outfit","old-style clothes")>>
		<</if>>.
		<<if $cool lt 119 and $rng lte 33>>
			Baggy, but not baggy enough to fit over your normal clothes. You quickly change.
			It's an interesting segment, and you feel like you learned something. Winter has you change back before you get too warm.
			<<ghistory>><<historyskill 1>>
			<br><br>
			As you take a seat, you feel itchy. It quickly rises to an irresistible need to scratch, but scratching only makes it worse and soon your whole body
			feels like it's burning. You hear some students at the desk in the corner by the screen whispering and snickering, you see them watching you and smiling.
			<br><br>
			They put something in your clothes. Itching powder.
			<br><br>
			<<link [[Endure it|History Cleanup]]>><<endevent>><<pain 3>><<stress 20>><<set $phase to 3>><</link>><<gpain>><<ggstress>>
			<br>
			<<link [[Rush to toilet to clean off (0:10)|History Cleanup]]>><<endevent>><<detention 2>><<set $phase to 0>><<stress 10>><</link>><<gdelinquency>>
			<br>
			<<link [[Tell teacher and ask to go clean off (0:10)|History Cleanup]]>><<endevent>><<status -10>><<set $phase to 1>><</link>><<lcool>>
			<br>
			<<if $exhibitionism gte 35>>
				<<link [[Strip and clean off there|History Cleanup]]>><<endevent>><<set $phase to 2>><</link>>
				<<if $worn.under_lower.type.includes("naked") or ($worn.under_upper.type.includes("naked") and $breastsize gte 1)>><<gdelinquency>><</if>>
				<br>
			<</if>>
		<<else>>
			Baggy enough to easily fit over your other clothing. It's an interesting segment, and you feel like you learned something. Winter has you remove the outfit before you get too warm.<<ghistory>><<historyskill 1>>
			<br><br>

			<<link [[Next|History Lesson]]>><<endevent>><</link>>
		<</if>>
		<br>
	<</if>>

<<elseif $rng gte 4>>
	<<generates1>><<person1>>Winter leaves a <<person>> to hand out textbooks while leaving the class on an errand. When the <<person>> gets to you <<he>> pauses, <<his>> face curling in a smirk.
	<br><br>

	Rather than placing your book on your desk, <<he>> drops it on the floor beside you. "Oops, how clumsy of me. You'd better pick it up before the teacher gets back." The rest of the class turns to watch.
	<br><br>
	<<if $skulduggery gte 100>><<set $skulduggerydifficulty to 400>>
		<<link [[Trick them|History Events Trick]]>><</link>><<skulduggerydifficulty>>
		<br>
	<</if>>
	<<link [[Pick it up|History Events Pick]]>><<trauma 2>><<stress 2>><<status -10>><</link>><<gtrauma>><<gstress>><<lcool>>
	<br>
	<<link [[Leave it|History Events Leave]]>><<stress 2>><<detention 2>><</link>><<gdelinquency>><<gstress>>
	<br>

<<else>>
	<<npc Winter>><<person1>>Winter gives the class a thin smile as <<he>> unveils the pillory in the corner of the room. "We're fortunate to have such a rare antique! I'll need somebody to help me demonstrate its use." <<He>> looks around the room. "Any volunteers?"
	<br><br>
	<<if $history gte 800>>
		No one volunteers, of course. Winter looks at you. "To the front of the class, please."
		<br><br>
		<<link [[Refuse|History Lesson]]>><<detention 1>><<endevent>><</link>><<gdelinquency>>
		<br>
		<<link [[Go to the front of the class|History Lesson Pillory]]>><<historyskill>><<stress 2>><</link>><<ghistory>><<gstress>>
		<br>

	<<elseif $history gte 600>>
		No one volunteers, of course. Winter looks at you. "To the front of the class, please."
		<br><br>
		<<link [[Refuse|History Lesson]]>><<endevent>><<detention 1>><</link>><<gdelinquency>>
		<br>
		<<link [[Go to the front of the class|History Lesson Pillory]]>><<historyskill>><<stress 2>><</link>><<ghistory>><<gstress>>
		<br>

	<<else>>
		<<link [[Keep your head down|History Lesson]]>><<endevent>><</link>>
		<br>
		<<link [[Volunteer|History Lesson Pillory]]>><<historyskill>><<stress 2>><</link>><<ghistory>><<gstress>>
		<br>
	<</if>>
<</if>>
<</nobr>><</widget>>

<<widget "eventshistorysafe">><<nobr>>
<<if $rng gte 81>>
	<<npc Winter>><<person1>>Winter has the class read in silence, and glares whenever <<he>> hears furtive whispering.
	<br><br>
	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 71>>
	<<generates1>><<person1>><<set _coinflip to random(0,1)>>
	Winter calls a <<person>> to the front of the class and asks <<him>> to model some historical clothes.
	<<if $history gte 700>>
		The clothes look like <<print either("early-medieval servant's garb,", "Tudor-era court clothes for a noble family, possibly the St. Clairs,", "those worn by a medieval 'runner' or courier,", "a pre-industrial Naval uniform,", "stable-boy garb,")>> unless you are mistaken.
		<<if $rng % 2>>However, judging by the <<print either("stitching,","colouring","hems","cut","'Made in Indonesia' label")>> these are only replicas.<<else>>As far as you can tell, these are genuine historical garments.<</if>>
	<<elseif $history gte 300>>
		They look like the stuff from <<print either("those plays you do in English where people say 'Hark!'","that TV show about posh old people in a huge house.","that TV show with all the beards and axes.","those old War photos.","those Roman things where everyone wears sheets.")>>
		Winter gets unusually excited discussing the context of the clothes.
	<<else>>
		The clothes look really old-fashioned and Winter tells some great stories that really bring them to life.
	<</if>>
	<br><br>
	Overall it is a fun, enjoyable and educational session.
	<<ghistory>><<historyskill>><<lstress>><<stress -1>>
	<br><br>
	<<if _coinflip>>
		The <<person>> seems to enjoy being the centre of attention and plays along enthusiastically with Winter's tales.
	<<else>>
		The <<personcomma>> however, seems uncomfortable with being the centre of attention. <<He>> looks like <<he>> would rather be somewhere else.
	<</if>>
	<br><br>
	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 61>>
	Winter asks the class a question.
	<<if $history gte 400>>
		You think you know the answer.
		<br><br>
		<<link [[Raise your hand|History Lesson Answer]]>><<stress 2>><<historyskill>><</link>><<ghistory>><<gstress>>
		<br>
		<<link [[Remain silent|History Lesson]]>><<endevent>><</link>>
		<br>
	<<else>>
		You don't know the answer.
		<br><br>
		<<link [[Next|History Lesson]]>><<endevent>><</link>>
		<br>
	<</if>>

<<elseif $rng gte 51>>
	<<generates1>><<person1>><<set _coinflip to random(0,1)>>
	A <<person>> near you falls asleep in class.
	<<if $rng % 2>>
		<<He>> quietly dozes for a few minutes, waking up sharply when <<print either("Winter drops a pen.", "a girl's chair scrapes.","the class door opens.","a boy falls off his chair.","another student walks past.","the student behind gives a gentle nudge.")>>
		<<He>> looks around in panic for a moment. You give <<him>> a subtle thumbs up, mouthing 'It's okay.'
		<br>
		<<He>> nods back in thanks.
		<<gcool>><<status 1>>
	<<else>>
		Everything is fine until <<his>> head rolls back and <<he>> starts loudly snoring. Winter stops talking, glaring at the <<personstop>>
		<br>
		"Wake <<him>> up," Winter commands.
		<br>
		<<if _coinflip>>Another student wakes<<else>>You wake<</if>> the <<if $pronoun is "m">>boy<<else>>girl<</if>> up.
		<br><br>
		"Thank you for joining us," Winter smiles. "Perhaps you can tell the head what has you so tired in detention."
		<br>
		<<His>> cheeks flush.
	<</if>>
	<br><br>
	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 41>>
	<<npc Winter>><<person1>>
	Winter lectures to the class in <<his>> droning voice.
	<<if $tiredness lt 2000>>
		It's a job to stay awake.
		<br><br>
		<<link [[Next|History Lesson]]>><<endevent>><<historyskill>><</link>><<ghistory>>
		<br>
	<<else>>
		You do your best to stay awake, but you're so tired. You awaken several minutes later, fortunately Winter seems not to have noticed.
		<br><br>
		<<link [[Next|History Lesson]]>><<endevent>><<set $tiredness -= 200>><</link>><<ltiredness>>
		<br>
	<</if>>

<<elseif $rng gte 31>>
	Winter shows the class some excerpts from a historical movie.
	<<if $rng % 2>>
		It's a boring old film that everyone has seen. It's educational, but even Winter seems uninspired by this.
	<<else>>
		It's a brand new film with some amazing effects. Winter highlights some of the good history in the film, along with a few amusing inaccuracies.
		<br>
		It's a fun session.
		<<ghistory>><<historyskill>><<lstress>><<stress -1>>
	<</if>>
	<br><br>

	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 21>>
	Winter has the class practise sourcing information from the various bookshelves around the edge of the room.
	<br><br>

	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<elseif $rng gte 11>>
	Winter goes off on a tangent about the school, park and hospital being on the site of an ancient fortress. They mention rumours of underground lairs, murky dungeons and hidden escape tunnels.
	<br><br>

	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>

<<else>>
	Winter goes off on a tangent about the history of the local transportation system. You don't know if you've ever listened to anything so boring.
	<br><br>

	<<link [[Next|History Lesson]]>><<endevent>><</link>>
	<br>
<</if>>
<</nobr>><</widget>>