<<effects>>

You walk passed the bemused bouncer guarding the door, and peek outside. It's <<if $daystate is "night">>dark<<else>>getting dark<</if>>, but neon light bathes the whole street. It'll be hard to avoid being seen. You don't care. <<flaunting>> you step outside. <<covered>>
<<if $ex_day isnot 1>>
<<set $ex_day to 1>><<exhibitionism2>>
<<else>>
<br><br>
<</if>>

<<if $rng gte 51>>
	<<generate1>><<person1>>
	An arm grasps your shoulder. "Where do you think you're going <<girl>>?" asks the bouncer, a <<personstop>> "You can't go out naked. You'll get hurt, and get us in trouble."
	<br><br>

	<<He>> tries to steer you inside.
	<br><br>

	<<link [[Run|Strip Club Leave Run]]>><</link>><<athleticsdifficulty 1 400>>
	<br>
	<<link [[Go inside|Strip Club Leave Inside]]>><</link>>
	<br>
<<else>>
	<<link [[Next|Connudatus Street]]>><<set $eventskip to 1>><</link>>
	<br>
<</if>>