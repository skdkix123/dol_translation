<<effects>>
<<endevent>><<npc Remy>><<person1>>
<<unset $farm_phase>>
Remy pulls you back to the small lane. <<He>> doesn't turn toward the cattle field, but walks in the opposite direction instead, past the barn and across a thin paved road. There's another field here, but the gate is more mundane, and bordered by regular hedgerows.
<br><br>

"You're going to be useful," Remy says, tugging you toward an old plough. "One way or another." Two farmhands grasp your arms. They pull you beneath, forcing you to bend a little. They tie your shoulders to the metal with thick rope.
<br><br>

Remy sits on the plough and leans back. <<He>> unclips <<his>> whip.
<br><br>

<<link [[Pull|Livestock Plough]]>><<sub 1>><<pass 60>><<transform cow 1>><<npcincr Remy dom 1>><<tiredness 18>><</link>><<ggtiredness>>
<br>
<<link [[Refuse|Livestock Plough Refuse]]>><<def 1>><<npcincr Remy dom -1>><<pain 6>><</link>><<gpain>>
<br>