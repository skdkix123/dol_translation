<<set $outside to 0>><<effects>>

You enter the small office responsible for the Connudatus market. A <<generate1>><<person1>><<person>> greets you.
<br><br>
"<span class="gold">£15</span> for the day," <<he>> says. "We close at Nine o'clock. We'll need to see a sample of what you intend to sell, too. We'll also need proof of citizenship, unless you're selling flowers or other produce."
<br><br>

<<set _plant_keys to Object.keys($plants)>>
<<for _t to 0; _t lt _plant_keys.length; _t++>>
	<<if $plants[_plant_keys[_t]].amount gte 1>>
		<<set _plant_temp to clone($plants[_plant_keys[_t]].plural)>>
	<</if>>
<</for>>
<<if _plant_temp is undefined>><<pass 5>>
	<span class="purple">You don't have anything appropriate to sell.</span>

	<br><br>
	<<link [[Leave|Connudatus Street]]>><<endstall>><<endevent>><<clotheson>><</link>>
	<br>
<<else>>
	<<set $money -= 1500>><<set $stall_rented to 1>>
	You show <<him>> one of your _plant_temp. It seems to satisfy. You sign the paperwork, hand over the money, and leave to find your stall.
	<br><br>
	<<endevent>>
	You lay your goods over the table, trying to arrange them in a pleasing way. There's a little roof to protect you from any rain, and a stool should you legs get tired. Other traders are setting up either side of you.
	<br><br>

	You finish up just as the first customers trickle onto the street.
	<br><br>

	<<stall_actions>>
<</if>>