<<effects>>
You slap Remy's face, too quick for <<him>> to dodge. One of the farmhands jabs the cattle prod forward, but Remy grasps and pushes it away. "Not necessary," <<he>> says, rubbing <<his>> face. <<He>> jerks the leash, forcing you a few steps forward.
<br><br>
"It's just grumpy," <<he>> says. "I'm sure its behaviour will improve. Or we will need to use the prods." <<He>> pulls again, and you stumble along behind <<himstop>>
<br><br>
<<link [[Next|Livestock Job]]>><</link>>
<br>