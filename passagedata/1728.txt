<<set $outside to 1>><<set $location to "compound">><<effects>>
<<if $submissive gte 1150>>
	"I-I-," you stammer. "I kicked my ball over the fence. I just wanted it back." You look down in mock shame.
<<elseif $submissive lte 850>>
	You turn to face <<him>> and place your hands on your hips. "Excuse me?" you say. "I have every right to be here. You'll stop bothering me if you value your job."
<<else>>
	"I'm delivering something," you say. "Go ask your boss."
<</if>>
<br><br>
<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
<</if>>
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<<set $compoundbluff to 1>>
	<<if $submissive gte 1150>>
		<<He>> sighs. "Stupid <<girlcomma>> you shouldn't enter places you don't understand. Go find your ball, but then leave straight away, alright?" <span class="green">You nod and skip away.</span>
	<<elseif $submissive lte 850>>
		You've unnerved <<himstop>> "Wait a moment," <<he>> says. "Just let me confirm that with my supervisor." <<He>> turns away to talk into <<his>> radio. That's all you need. <span class="green">You slip away while <<his>> back is turned.</span>
	<<else>>
		"You really shouldn't be skulking around then," <<he>> says. "Just do your job and leave. It's not safe for you around here." <span class="green">You nod and walk away.</span>
	<</if>>
	<br><br>
	<<endevent>>
	<<link [[Next|Elk Compound]]>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	"Nice try," <<he>> says. "But you're not wriggling away." <span class="red"><<He>> grips your shoulder more tightly.</span>
	<br><br>
	<<link [[Next|Elk Compound Underground]]>><</link>>
	<br>
<</if>>