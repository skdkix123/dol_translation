<<set $outside to 0>><<set $location to "town">><<effects>>

You turn back to find the <<person>> resting <<his>> arm above Robin's head. <<Hes>> standing awfully close.
<br><br>

"Why don't we go somewhere private,
<<if $halloween_robin_costume is "witch">>
girl," <<he>> says to Robin. "Show me your witchcraft."
<<elseif $halloween_robin_costume is "vampire">>
boy," <<he>> says to Robin. "I have some delicious blood for you to suck."
<<else>>
<<if $NPCName[$NPCNameList.indexOf("Robin")].pronoun is "m">>boy<<else>>girl<</if>>," <<he>> says to Robin. "Show me what you're hiding under that sheet."
<</if>>
<br><br>
<<person1>>
Robin stares at <<his>> feet, too embarrassed to say anything.
<br><br>

<<if $promiscuity gte 35 and $robinromance is 1>>
<<link [[Propose threesome|Robin Trick Threesome]]>><</link>><<promiscuous3>>
<br>
<</if>>
<<link [[Pull Robin away|Robin Trick Pull]]>><<npcincr Robin love 1>><</link>><<glove>>
<br>
<<if $robinromance is 1>>
<<link [[Slap|Robin Trick Slap]]>><<npcincr Robin dom -1>><<npcincr Robin love 1>><</link>><<ldom>>
<br>
<</if>>
<<link [[Ignore|Robin Trick Ignore]]>><</link>>
<br>