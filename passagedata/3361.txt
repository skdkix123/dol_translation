<<effects>>

<<if $physique gte random(1, $physiquemax)>>
	You lift the box above your head, and throw it against the ground. <span class="green">It smashes open.</span> Inside are binders full of paperwork, including balance sheets. They belong to a local corporation. Someone in the underworld might be interested in this.
	<<set $blackmoney += 100>><<crimeup 100>>
	<br><br>

	<<destinationeventend>>
<<else>>
	You lift the box above your head, and throw it against the ground. <span class="red">It just bounces and makes an awful lot of noise.</span> You try again, but the box is just too sturdy.
	<br><br>

	<<destinationeventend>>
<</if>>