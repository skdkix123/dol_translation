<<set $outside to 1>><<set $location to "town">><<effects>>
You walk behind the spa, and find the oak the note mentioned. It stands at the edge of the forest. This is where you were told to strip. There's no one around.
<br><br>
<<if $exhibitionism gte 35>>
	<<link [[Strip|Danube Oak Strip]]>><<set $phase to 0>><</link>><<exhibitionist3>>
	<br>
<<else>>
	<<link [[Strip|Danube Oak Strip]]>><<set $phase to 1>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
	<br>
<</if>>
<<link [[leave|Danube Street]]>><</link>>
<br>