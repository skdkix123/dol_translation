<<effects>><<set $outside to 0>>

You're bound in the darkness. The engine roars into life, and you feel the car move.
<br><br>

<<link [[Wait (0:30)|Maths Boot Wait]]>><<pass 30>><<stress 6>><<trauma 6>><</link>><<gtrauma>><<gstress>>
<br>
<<link [[Struggle (0:30)|Maths Boot Struggle]]>><<pass 30>><<tiredness 6>><<stress 18>><<trauma 6>><</link>><<gtrauma>><<ggstress>><<gtiredness>>
<br>