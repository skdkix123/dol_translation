<<set $outside to 1>><<set $location to "beach">><<water>><<effects>><<set $bus to "seabeach">>
You are swimming in the sea along the beach.
<<if $daystate is "day">>
	<<if $weather is "clear">>
		It is awash with visitors, children build sandcastles and play in the water while their parents bask in the sun. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		The clouds have driven away most would-be visitors, but there are still people strolling along the water's edge.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "dawn">>
	<<if $weather is "clear">>
		It is a popular destination for joggers, some have dogs with them. A few families are setting up windbreakers. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		It is a popular destination for joggers, some have dogs with them. Fog blocks your view of the ocean.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "dusk">>
	<<if $weather is "clear">>
		Families are leaving as the sun sets. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		It is mostly deserted, but some people are strolling along the water's edge.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "night">>
	<<if $weather is "clear">>
		It appears deserted, save for a group of teenagers who are drinking around a fire.
	<<elseif $weather is "overcast">>
		It appears deserted.
	<<elseif $weather is "rain">>
		It appears deserted.
	<</if>>
<</if>>
<br><br>
<<if $seaswim is 1>>
	<<set $seaswim to 0>>
	You spend some time swimming.
	<<physique 3>><<swimmingskilluse>>
<</if>>
<<if $exposed gte 1>>
	You keep your <<lewdness>> hidden beneath the surface of the water.
	<<if $daystate isnot "night">>
		You can't leave the water here, people would see you!
	<</if>>
	<br><br>
<</if>>
<<if $stress gte 10000>>
	<<passoutbeach>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		<<eventsseabeach>>
	<<else>>
		<<if $exposed lte 0 or $daystate is "night">>
			<<link [[Leave the water (0:02)|Beach]]>><<pass 2>><</link>>
			<br>
		<<elseif $exposed is 1 and $exhibitionism gte 15>>
			<<link [[Leave the water (0:02)|Beach Exposed]]>><<pass 2>><<set $phase to 1>><</link>><<exhibitionist2>>
			<br>
		<<elseif $exposed gte 2 and $exhibitionism gte 75>>
			<<link [[Leave the water (0:02)|Beach Exposed]]>><<pass 2>><<set $phase to 0>><</link>><<exhibitionist5>>
			<br>
		<</if>>
		<<link [[Practise swimming (0:30)|Sea Beach]]>><<pass 30>><<stress -6>><<tiredness 6>><<set $seaswim to 1>><<slimeEventEnd>><</link>><<gswimming>><<lstress>><<gtiredness>>
		<br>
		<<link [[Swim out to sea (0:10)|Sea]]>><<pass 10>><<tiredness 2>><<set $sea to 0>><</link>><<gtiredness>><<swimmingdifficulty 1 100>>
		<br><br>
		<<searocks>><<swimmingdifficultytext0>>
		<br>
		<<seadocks>><<swimmingdifficultytext0>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>