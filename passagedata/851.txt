<<effects>>

You turn and run alongside Alex. The boar is close behind, and you hear it gaining.

<<if $athletics gte random(800, 1000)>>
	It catches up to Alex, grabs <<his>> <<if $pronoun is "f">>skirt<<else>>shorts<</if>> in its mouth, and drags <<him>> to the ground.
	<br><br>
	
	The 
	<<if $pronoun is "f">>
		skirt rips free, exposing Alex's black and red boyshorts.
	<<else>>
		shorts rip free, exposing Alex's black and red boxers.
	<</if>>
	The boar veers and runs deeper into the forest, the fabric still in its maw.
	<br><br>
	
	"Fuck," Alex says. "Is it gone?" <<He>> looks around, then pushes <<himself>> to <<his>> feet. <<He>> hasn't noticed <<his>> missing <<if $pronoun is "f">>skirt<<else>>shorts<</if>>.
	<br><br>
	
	<<link [[Stare|Farm Race Stare]]>><<npcincr Alex love -1>><<npcincr Alex dom -1>><<npcincr Alex lust 1>><</link>><<promiscuous1>><<llove>><<ldom>><<glust>>
	<br>
	<<link [[Look away|Farm Race Look]]>><</link>>
	<br>
	<<link [[Laugh|Farm Race Laugh]]>><<npcincr Alex dom -1>><</link>><<ldom>>
	<br>
<<else>>
	<<if $worn.lower.name is "naked">>
		It catches up to you, and bashes you aside. The world spins as you tumble to the ground.<<gpain>><<gstress>><<pain 4>><<stress 6>>
		<br><br>
		
		You get your bearings in time to see the boar disappear into the forest. "Are you okay?" Alex asks, crouching beside you. <<He>> helps you up, and laughs. "Good. That was almost scary. Come on. Let's get back to work."
		<br><br>
		
		<<link [[Next|Farm Work]]>><<endevent>><</link>>
		<br>
	<<else>>
		<<set $worn.lower.integrity -= 100>>
		<<if $worn.lower.integrity lte 0>>
			It catches up to you, grasps your $worn.lower.name in its maw, and pulls you to the ground.<<gpain>><<gstress>><<pain 4>><<stress 6>>
			<br><br>
			
			<span class="pink">Your clothes rip free as you fall, exposing your <<undies>>.</span> The boar veers and runs deeper into the forest, your $worn.lower.name still in its mouth.
			<br><br>
			<<integritycheck "no_text">><<exposure>>
			"A-Are you okay?" Alex asks, crouching beside you. <<Hes>> blushing, and doesn't know where to look. You climb to your feet. <<covered>> <<glust>><<npc Alex lust 1>>
			<br><br>
			
			Alex leads the way back to the farmhouse. <<He>> disappears inside, and emerges with some towels.<<towelup>>
			<br><br>
			
			<<link [[Next|Farm Work]]>><<endevent>><</link>>
			<br>		
			
		<<else>>
			It catches up to you, grasps your $worn.lower.name in its maw, and pulls you to the ground.<<gpain>><<gstress>><<pain 4>><<stress 6>>
			<br><br>
			
			The fabric tears free as you fall. The boar veers and runs deeper into the forest.
			<br><br>
			
			"Are you okay?" Alex asks, crouching beside you. <<He>> helps you up, and laughs. "Good. That was almost scary. Come on. Let's get back to work."
			<br><br>
			
			<<link [[Next|Farm Work]]>><<endevent>><</link>>
			<br>
		<</if>>
	<</if>>
<</if>>