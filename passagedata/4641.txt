<<effects>>

You turn and run from the shop, heart thumping in your chest. You run into an alley and peek around the corner. You weren't followed.
<br><br>

<<link [[Next|High Street]]>><<endevent>><<set $eventskip to 1>><</link>>
<br>