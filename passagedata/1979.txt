<<effects>>
<<if $submissive gte 1150>>
	"You should take the money from bad people," you say. "Steal it."
	<br><br>
	<<Hes>> taken aback by your bluntness. "But stealing is wrong!" <<he>> says.
	<br><br>
	"I know it's scary," you say. "But you need to be able to look after yourself."
<<elseif $submissive lte 850>>
	"You should stop begging from your fellows here," you say. "We're struggling enough. You should steal it."
	<br><br>
	<<Hes>> taken aback by your bluntness. "But stealing is wrong!" <<he>> says.
	<br><br>
	"Stealing from baddies isn't wrong," you say.
	<br><br>
<<else>>
	"You're not going to make enough money like that," you say. "You should just steal it."
	<br><br>
	<<Hes>> taken aback by your bluntness. "But stealing is wrong!" <<he>> says.
	<br><br>
	"Not from here," you say. "From the people who want to hurt you."
	<br><br>
<</if>>
<<He>> looks away, lost in thought.
<br><br>
<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>