<<effects>>

<<if $phase gte 10000>>
	<<set $seductiondifficulty to 8000>>
<<elseif $phase gte 5000>>
	<<set $seductiondifficulty to 4000>>
<<else>>
	<<set $seductiondifficulty to 2000>>
<</if>>
<br><br>

<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>><<seductionskilluse>>
<br><br>

You tell <<him>> how much access to your body is worth.
<<promiscuity3>>

<<if $seductionrating gte $seductionrequired>>

<span class="green"><<He>> nods.</span> "Fine."
<br><br>

You follow <<him>> into a nearby alley. <<He>> shoves you against the wall. A dumpster hides you from the view of the street.
<br><br>

<<link [[Next|Street Bodywriting Sex]]>><<set $sexstart to 1>><</link>>
<br>

<<else>>

<span class="red"><<He>> shakes <<his>> head.</span> "I'm not paying that for a street whore."

	<<if $rng gte 81>>
		<span class="red"><<He>> wraps <<his>> arm around your mouth and tries to pull you into the alley anyway.</span> "I'll give you what you're worth."
		<br><br>

		<<if $leftarm is "bound" and $rightarm is "bound">>
			With your arms bound, you're defenceless.
			<br>
		<<else>>
			<<link [[Smack|Street Bodywriting Smack]]>><<def 1>><</link>><<physiquedifficulty>>
			<br>
		<</if>>
		<<link [[Go quietly|Street Bodywriting Quiet]]>><<sub 1>><</link>>
		<br>
	<<else>>
		<<He>> turns and walks away.
		<br><br>

		<<endevent>>
		<<destinationeventend>>
	<</if>>
<</if>>