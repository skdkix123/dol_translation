<<effects>>

<<if $submissive gte 1150>>
	"I-I'm sorry," you say. "I don't want to."
<<elseif $submissive lte 850>>
	"I said no," you say. "That's the end of it."
<<else>>
	"I'm sorry," you say. "I just don't want to."
<</if>>
<br><br>

"Suit yourself," Avery says, anger tinging <<his>> voice. "Try not to show me up."
<br><br>

<<link [[Next|Avery Party 4]]>><</link>>
<br><br>