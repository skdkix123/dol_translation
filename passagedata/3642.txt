<<effects>>
You step forward as quick as you can. You feel the coals heat beneath you.
<<pain 1>>
<br><br>
<<if ($willpower / 10) gte (($pain - 100) + random(1, 100))>>
	<span class="green">A little sting isn't enough to stop you.</span>
	<<gwillpower>><<willpower 1>>
	<br><br>
	<<link [[Continue|Temple Firewalk 4]]>><</link>><<gpain>>
	<br>
	<<link [[Jump off|Temple Firewalk Jump]]>><</link>>
	<br>
<<else>>
	<span class="red">You feel your feet burning. Panicked, you leap off the coals onto the cool dirt.</span>
	<<ggwillpower>><<willpower 10>>
	<br><br>
	<<link [[Next|Temple Firewalk Jump]]>><</link>>
	<br>
<</if>>