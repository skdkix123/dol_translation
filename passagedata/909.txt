<<effects>>


Once inside, Alex insists on making the drinks before proceeding.
<br><br>

<<endevent>><<npc Remy>><<person1>>

"Thank you for your hospitality," Remy says after sipping <<his>> tea.
<br>
<<endevent>><<npc Alex>><<person1>>"Just get on with it," Alex says. "I've got work to do."
<br>
<<endevent>><<npc Remy>><<person1>>"As you wish. I'm here to offer assistance. I couldn't help but-"
<br>
<<endevent>><<npc Alex>><<person1>>"I already have the assistance I need," Alex interrupts, gesturing at you. "We're reclaiming the land, bit by bit. It's going well. And you're not getting a piece. That's what you're really here for, isn't it."
<br><br>

<<link [[Next|Farm Stage 5 2]]>><</link>>
<br>