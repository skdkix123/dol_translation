<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>
You approach a <<generate1>><<person1>><<person>> standing on a raised platform. <<He>> jots something on <<his>> clipboard while observing the workers.
<br><br>

<<if $submissive gte 1150>>
"Excuse me," you say. "S-sorry to bother you. Are you hiring?"
<<elseif $submissive lte 850>>
"Hey," you say. "I'm looking for work. Who do I talk to?"
<<else>>
"Hello," you say. "Are you hiring?"
<</if>>
<br><br>

<<He>> doesn't look up from <<his>> clipboard, but points <<his>> pen in the direction of a small building a little closer to the water.
<br><br>

The door is open. A <<generate2>><<person2>><<person>> looks up from <<his>> desk as you enter. "Ah," <<he>> says, pushing <<his>> glasses up <<his>> nose. "Looking for work?" You nod. <<He>> stands and squeezes between <<his>> desk and the wall until <<he>> stands in front of you. <<He>> holds out <<his>> hand.
<br><br>

<<link [[Next|Docks Ask 2]]>><</link>>
<br>