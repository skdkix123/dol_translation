<<effects>>

It stings. You wonder how these idiots would like this done to them, and think up humiliating designs for each of them. It helps, if only a little.
<br><br>

Over time, though, you grow accustomed to the sting, and even a bit bored as it drags on. At last, Remy steps away from your <<bottom>> to examine <<his>> handiwork. You wonder what it looks like.
<br><br>

The <<person2>><<person>> and <<person3>><<person>> untie your arms from the table, and Remy tugs you to your feet. <<He>> leads you through the doors and back to the barn proper.<<unbind>>
<br><br>
<<add_bodywriting left_bottom cattle_brand tattoo>>
<<link [[Next|Livestock Intro Light Cell]]>><<endevent>><</link>>