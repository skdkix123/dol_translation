<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You get back to work. You keep an eye on the <<person>> and <<his>> admires. They're more lecherous than ever, but <<he>> seems in control. After a while <<he>> leaves for the dressing room, despite their protests.
<br><br>

You're pouring a <<person2>><<person>> a beer towards the end of your shift when you hear a voice. "Thanks love," it says. You look up to see the <<person1>><<person>> smiling at you across the counter, now dressed in everyday clothes. "They ate that routine right up. Tipped generously," <<he>> reaches into a pocket. "Only fair you share some," <<he>> places some money on the counter and leaves without another word.
<br><br>
<<if $phase is 2>>
	<<set $tipmod to 0.8>>
<<elseif $phase is 1>>
	<<set $tipmod to 0.6>>
<<else>>
	<<set $tipmod to 0.4>>
<</if>>
<<tipset "serving">>
<<set $tip += 500>>
<<if $drinksservedstat gte 10000>>
<<set $tip += 200>>
<</if>> <<tipreceive>>
<br><br>

<<link [[Next|Strip Club]]>><<endevent>><<pass 30>><</link>>