<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $submissive gte 1150>>
	"Are you alright?" you ask, smirking. "I think Bailey could have managed that one."
<<elseif $submissive lte 850>>
	"I did it," you say. "I'm the champion now. I claim this console as my prize."
<<else>>
	"Wow, that was really easy," you say, smirking.
<</if>>
<br><br>
Robin sits back up. "You got lucky," <<he>> says as <<he>> snatches the controller back.
<br><br>
<<robinoptions>>