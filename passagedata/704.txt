<<set $outside to 1>><<effects>>

Tucked away in a hidden corner of the meadow is a thin path leading into a thicket, nestled between two hills. It takes you north, and emerges at the top of the cliffs facing the sea.
<br><br>

Few know the path is here.
<br><br>

<<link [[Next|Coast Path Farmland]]>><</link>>
<br>