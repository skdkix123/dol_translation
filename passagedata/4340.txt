<<effects>>
<<if $enemywounded is 1 and $enemyejaculated is 0>>
	The lizard hisses and flees into the darkness.
<<elseif $enemywounded is 0 and $enemyejaculated is 1>>
	The lizard leaves you lying on the cement.
<<elseif $enemywounded gte 2 and $enemyejaculated is 0>>
	Feeling that you're more trouble than you're worth, the lizards flee into the darkness.
<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
	The lizards leave you spent and shivering in the dark.
<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
	The lizards leave you spent and shivering in the dark. One seems to be limping.
<</if>>
<br><br>
<<tearful>> you gather yourself.
<br><br>
<<clotheson>>
<<endcombat>>
<<set $stress -= 1000>>
<<destinationeventend>>