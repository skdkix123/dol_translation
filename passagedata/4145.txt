<<set $outside to 0>><<set $location to "home">><<effects>>
<<set $hacker_tasks.pushUnique("bailey")>>

<<set $rng to random(0,100)>>
<<if $pronoun is "m">>"Sir,"<<else>>"Miss Bailey,"<</if>>
<<if $rng gte 67>>
	you shout. "There's a man in the garden trying to drag away one of the children!"
	<br>
	"What!?" Bailey shouts. <<He>> charges out of the office in the direction of the garden.
<<elseif $rng gte 33>>
	you whisper. "One of the older boys is in the dormitory right now telling the others you don't look after us properly. That we should all run away,
	or fight you, or... I... I don't want to snitch, but I'm scared he'll mislead the little ones."
	<br>
	"You did the right thing," Bailey says. "Leave it to me."
	<br>
	<<He>> marches out in the direction of the dormitory.
<<else>>
	you pant. "One of the girls is out in the street shouting about how you're abusing us. That this place should be shut down. But where will we live?"
	<br>
	Bailey scowls. "Don't worry. I'll sort this out."
	<br>
	<<He>> marches out into the streets.
<</if>>
<br><br>

<<skulduggerycheck>>
<br><br>
<<if $skulduggerysuccess is 1>>
	Bailey is gone for long enough for you to install the device and escape.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>
	<<if $hacker_tasks.length gte 2>>
		<span class="green">You have completed the hacker's tasks - you now need to talk to Landry.</span>
		<br><br>
	<</if>>
	<<link [[Next|Orphanage]]>><<endevent>><</link>>
	<br>

<<else>>
	You install the device successfully but as you turn to escape, a scowling Bailey blocks the door.
	<br>
	"Why do you little idiots love to waste my time?"
	<br>
	<<He>> grabs your arm.
	<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
	<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
	<</if>>

	<<link [[Next|Bailey Beating]]>><<set $molestationstart to 1>><</link>>
	<br>

<</if>>