<<effects>>

You have a pleasant time chatting with the <<person>>.

<<if $rng gte 81>>
	<<He>> tells you <<hes>> taken up lessons so <<he>> can spend more time in the countryside.
<<elseif $rng gte 61>>
	<<He>> talks about <<his>> life out in the countryside.
<<elseif $rng gte 41>>
	<<He>> talks about <<his>> work and home life.
<<elseif $rng gte 21>>
	<<He>> talks about <<his>> hobbies.
<<else>>
	<<He>> encourages you to stay in school.
<</if>>
<br><br>

<<link [[Next|Riding School Lesson]]>><<endevent>><</link>>
<br>