<<effects>>
<<pass 180>>
<<endevent>>
<<npc Remy>><<person1>>
You're slumped, panting, above the straw-strewn floor. Next thing you know, Remy stands in front of the milking machine. <<He>> examines the glass canister attached to you.
<br><br>
<<if $livestock_milk is undefined>><<set $livestock_milk to 0>><</if>>
<<if $livestock_semen is undefined>><<set $livestock_semen to 0>><</if>>
<<set $fluid_forced_stat += ($livestock_milk + $livestock_semen)>>
<<if $livestock_milk + $livestock_semen gte 5000>><<earnFeat "Pride of the Farm">>
<<npcincr Remy love 3>>"Phenomenal productivity," <<he>> says, gazing in awe at the <span class="green">vast</span> quantity of white fluid in front of <<himstop>> "I know of no other <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>> that could produce so much."
<<gglove>>
<<elseif $livestock_milk + $livestock_semen gte 3000>>
<<npcincr Remy love 2>>"You're a very good <<girl>>," <<he>> says, staring at <span class="teal">great</span> quantity white fluid in front of <<himstop>> "Only trouble is," <<he>> smiles. "It might be a bit heavy."
<<glove>>
<<elseif $livestock_milk + $livestock_semen gte 1000>>
<<npcincr Remy love 1>>"With produce like this," <<he>> says, looking at the <span class="lblue">large</span> quantity of white fluid in front of <<himstop>> "You're one of the most valuable members of our herd."
<<glove>>
<<elseif $livestock_milk + $livestock_semen gte 500>>
	"Very good," <<he>> says, looking at the <span class="blue">decent</span> amount of white fluid in front of <<himstop>> "You're a valuable member of our herd."
<<elseif $livestock_milk + $livestock_semen gte 300>>
	"Good," <<he>> says, looking at the <span class="purple">small</span> amount of white fluid in front of <<himstop>> "I bet you can do better, though."
<<elseif $livestock_milk + $livestock_semen gte 100>>
	"Not much for a <<if $player.gender_appearance is "m">>bull<<else>>cow<</if>>," <<he>> says, looking at the <span class="pink">tiny</span> amount of white fluid in front of <<himstop>> "But better than nothing."
<<elseif $livestock_milk + $livestock_semen gte 1>>
	"A pitiable amount," <<he>> sighs, looking at the <span class="red">dribble</span> of white fluid in front of <<himstop>> "But it means there's something to work with, at least."
<<else>>
	"Nothing?" <<he>> sighs. "Don't worry. You'll be a productive member of our herd yet."
<</if>>
<<He>> opens the gate kneels beside you, resting one hand on your shoulder. The treatment left your body so sensitive that even this sends a shiver of arousal down your spine.
<br><br>

<<if $breastfeedingdisable is "f">>
	<<if $penisexist is 1>>
		<<He>> reaches beneath you, and tugs free the pump stuck to your pelvis. It's almost enough to send you over the edge. The pumps stuck to your <<breasts>> come next.
	<<else>>
		<<He>> reaches beneath you, and tugs free the pumps stuck to your <<breastsstop>> It's almost enough to send you over the edge.
	<</if>>
<<else>>
	<<He>> reaches beneath you, and tugs free the pump surrounding your <<penisstop>> It's almost enough to send you over the edge.
<</if>>

<<if $livestock_obey gte 80>>
	"You're such a good <<girl>>," <<he>> says, scratching you behind the ear. "Time for bed." <<He>> stands and walks to the gate. <<He>> smiles at you as <<he>> shuts the cage.
<<elseif $livestock_obey gte 20>>
	<<He>> wraps <<his>> fingers around your collar and pulls you close. "You're going to be a good <<girl>> tonight, okay?" <<he>> whispers. "No keeping the other cattle up." <<He>> releases you, stands, and walks over to the gate. It shuts with a clank.
<<else>>
	<<He>> grasps your ear and pulls you close. "It's time for bed. I better not hear you got up to mischief overnight. Understood?" <<He>> releases you, stands, and walks to the gate without waiting for a response.
<</if>>
<br><br>

Remy and the farmhands finish freeing the cattle from the machine. They turn off the lights as they leave, plunging the barn into darkness.
<br><br>

<<endevent>>

<<unset $livestock_semen>>
<<unset $livestock_milk>>
<<unbind>>

<<link [[Next|Livestock Cell]]>><</link>>
<br>