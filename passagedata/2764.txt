<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You nod, and the headteacher smiles. "Lock the door," <<he>> says. You do so. <<He>> stands and walks in front of <<his>> desk. "Now <<girlcomma>> I want you to smile. That's all."
<br><br>

<<He>> takes a picture. Then another from a different angle. This continues for several minutes. <<He>> asks you to strike specific poses, but nothing lewd. "There," <<he>> says. <<He>> sounds satisfied. "That'll be all. For today. I want you to return every school day. Whatever time you wish, but if you miss a day, I'll make sure you regret it." <<He>> opens the door and ushers you out.
<br><br>

<<endevent>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>