<<effects>>
<<if $phase is 1>>
	You agree to the transaction, and find a secluded spot in a nearby alley to conduct the exchange.
	<<if $worn.lower.skirt is 1>>
		The <<person>> stares at you, eyes wide, as you lift the sides of your skirt and slip your $worn.under_lower.name down to your knees. You lift one leg out, then the other, and just like that you are naked beneath your skirt.
		<br><br>
	<<else>>
		You ask the <<person>> to turn around while you take them off. When <<his>> back is turned, you remove your $worn.lower.name and slip your $worn.under_lower.name off your legs. Feeling exposed, you quickly put your $worn.lower.name back on.
		<br><br>
	<</if>>
	You hand <<him>> the garment, which <<he>> takes with trembling hands. "It's still warm," <<he>> says, holding it against <<his>> face.
	<<if $rng lte 50 and $daystate is "night">>
		<<underlowerruined>>
		<<He>> is about to hand you the money, when <<he>> hesitates. <<He>> goes back into <<his>> <<if $pronoun is "m">>wallet<<else>>purse<</if>> and pulls out more notes.
		<br><br>
		"£270 for the rest," <<he>> says. "For all your clothes. That's £300 for everything."
		<br>
		<<He>> holds the money in front of you.
		<br><br>
		<<if $exhibitionism gte 75>>
			<<link [[Agree|Street Panties All]]>><<set $phase to 1>><</link>><<exhibitionist5>>
		<</if>>
		<br>
		<<link [[Refuse|Street Panties All]]>><<set $phase to 2>><</link>>
		<br>
	<<else>>
		<<if ($vaginasemen + $vaginaoutsidesemen + $anussemen + $bottomsemen) gte 3>>
			The $worn.under_lower.name stick to <<his>> face. <<He>> smiles. "And covered in cum and juices."
			<br><br>
			<<He>> peels them off <<his>> face,
		<<elseif $arousal gte 6000>>
			<<He>> notices the obvious damp patch and gives it a deep sniff. "Smells like arousal."
			<br><br>
			<<He>>
		<<else>>
			<<He>>
		<</if>>
		hands you the money and departs.
		<<exhibitionism3>>
		<br><br>
		<<underlowerruined>>
		<<set $money += 3000>><<fameexhibitionism 1>>
		<<endevent>>
		<<destinationeventend>>
	<</if>>
<<elseif $phase is 2>>
	You give the <<person>> a coy smile. "But I'd need to be wearing some first." You walk away, leaving <<him>> speechless.
	<<exhibitionism2>>
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<elseif $phase is 3>>
	You hasten your pace to escape the pervert.
	<br><br>
	<<stress 3>>
	<<endevent>>
	<<destinationeventend>>
<<elseif $phase is 4>>
	You give the <<person>> a coy smile. "But I'm not wearing any, <<print ["look.", "see?"].pluck()>>"
	<br><br>
	With a quick glance about and a wink
	<<if $worn.lower.skirt is 1>>
		you lift your skirt above your hips
	<<else>>
		you pull the front of your waistband down
	<</if>>
	and give the <<person>> a fleeting glimpse of your <<genitalsstop>>
	<<if $player.gender_appearance isnot $player.gender>>
		The <<person>> does a double take, while <<he>> definitely wasn't expecting such a flagrant act of exhibitionism <<he>> also clearly was not expecting you to be a <<if $player.gender is "m">>boy<<elseif $player.gender is "h">>hermaphrodite<<else>>girl<</if>>.
	<<else>>
		The <<person>> gawks at you, stunned by your flagrant act of exhibitionism on a public street in broad daylight.
	<</if>>
	Satisfied <<he>> saw enough you walk away, leaving the utterly speechless <<person>> in your wake.
	<<exhibitionism4>>
	<<endevent>>
	<<destinationeventend>>
<</if>>