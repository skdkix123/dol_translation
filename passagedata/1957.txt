<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
<<set $rng to random(0,100)>><<generates2>>
<<if $rng gte 65 and ($famesex + $famerape + $fameprostitution) gte 400>>
	<<person1>>Just inside the door the <<person>> pulls you aside.
	<br>
	"Listen," <<he>> says in almost a whisper. "I'm going to be honest with you here. I've seen you around. You're pretty well known for being... forward. Sexually I mean."
	<br><br>
	<<He>> glances nervously at an internal door.
	<br>
	<<person2>>"See my <<if $pronoun is "m">>son<<else>>daughter<</if>> is a good-looking kid, and horny as hell, but... <<hes>>
	<<if $pronoun is $player.gender>>gay, I think. And confused? Conflicted? I don't know, but <<he>> will never
	<<else>>painfully shy. If left to <<if $pronoun is "m">>him<<else>>her<</if>>self, <<he>> will never
	<</if>>make the first move. <<He>> needs someone to show <<him>> the ropes."
	<br><br>
	<<person1>>The <<person>> points at a door.
	<br>
	<<person2>>"Through there. Seduce <<himstop>> Help <<himstop>> I'll give you £20 now and another £100 if you show <<him>> a good time. Deal?"
	<br><br>
	<<link [[Agree|Domus Shy Seduce]]>><<set $money += 2000>><</link>>
	<br>
	<<link [[Refuse and leave|Domus Street]]>><<endevent>><</link>>
	<br>
<<else>>
	<<person1>>The <<person>> takes you through to a living room where a shy-looking <<person2>><<person>> sits uncomfortably stroking a dog.
	<br>
	"I brought you someone to talk to. Looks about your age."
	<br>
	The <<person>> looks up.
	<<if $seductionskill gte 600 and $rng gte 67>>
		<<pass 60>>
		Blushes. Looks down.
		<br>
		"Talk. What's that thing you young people are all into nowadays. That... y'know? It was on that internet thing. It was on the news that time."
		<br>
		The <<person>> continues to stare at a patch of carpet.
		<br>
		"You know, right?" <<person1>>the <<person>> looks at you.
		<br>
		You shrug.
		<br><br>
		Time starts to drag. Out of boredom you try flirting with the <<person2>><<personstop>> It doesn't work, but it does get <<his>> attention.
		After a particularly blunt pass the <<person1>><<person>> is starting to look <<print either("uncomfortable, ","aroused, ","quite awkward, ")>>
		but the <<person2>><<person>> opens up. <<He>> starts telling you about school, and how it used to be okay, but now <<hes>> scared to talk
		to anyone because <<hes>> terrified they'll figure out <<hes>> into <<if $pronoun is "f">>girls.<<else>>boys.<</if>> That just isn't okay in <<his>> extended family. And maybe not at home either. <<He>> looks at <<his>> <<if $pronoun1 is "m">>father.<<else>>mother.<</if>>
		<br>
		The <<person1>><<person>> doesn't say anything, but also doesn't leave.
		<br><br>
		The <<person2>><<person>> tells you <<if $player.gender_appearance is $pronoun>><<he>> thinks you're cute and that it's a relief to finally be
		able to say that. <<else>>that <<he>> thinks you're cute and <<he>> appreciates the attention, but now <<he>> can finally be honest, you're just not
		what <<he>> is looking for.<</if>>
		<br><br>
		<<He>> starts telling you about <<his>> hobbies and interests. <<He>> talks about books, games and internet groups.
		<<He>> says it's a relief to finally talk to someone. <<He>> hasn't even felt able to talk to <<his>> parents.
		<br>
		The <<person1>><<person>> doesn't say anything, but looks somehow sad.
		<br><br>
		Shortly after this, the <<person1>><<person>> leads you out of the room.
		<br>
		"Thank you." <<he>> says.<<person2>>
		<<if $rng %2>>
			"Wow. I didn't see that coming. My own <<if $pronoun is "f">>daughter.<<else>>son.<</if>> Still, that was a breakthrough. <<He>> hasn't spoken like this in years. Thank you."
			<br><br>
		<<else>>
			"We've suspected for years. But now <<hes>> finally found the courage to say it. Such a relief. Thank you so much."
		<</if>>
		<br>
		<<person1>><<He>> passes you £40 as <<he>> leads you out.
		<<set $money += 4000>>
		<br><br>
		<<link [[Next|Domus Street]]>><<endevent>><</link>>
		<br>
	<<elseif $deviancy gte 35 and $rng lte 33 and $bestialitydisable is "f">>
		<<pass 60>>
		Blushes. Looks down.
		<br>
		"Talk. What's that thing you young people are all into nowadays. That... y'know? It was on that internet thing. It was on the news that time."
		<br>
		The <<person>> continues to stroke the dog while staring at a patch of carpet.
		<br>
		You notice something unusually intimate in the way <<he>> strokes the dog.
		<br>
		"That's a gorgeous dog," you say. "Is it okay if I stroke <<if $rng % 2>>him?"<<else>>her?"<</if>>
		<br><br>
		As you chat, you start to fondle and grope the dog. The <<person1>><<person>> doesn't notice, but the <<person2>><<persons>> eyes widen.
		<<He>> grows increasingly excited.
		<br><br>
		"Everyone says they love animals, but so few actually do," you say.
		<br>
		The <<person>> nods excitedly, while the <<person1>><<person>> yawns.
		<br><br>
		You and the <<person2>><<person>> spend the rest of the hour discussing your shared love of animals.
		<br><br>
		As you leave the <<person1>><<person>> stops you.
		<br>
		"I don't know how," <<he>> says. "But you got through to <<person2>><<himstop>>"
		<br><br>
		He hands you £30.
		<<set $money += 3000>>
		<br><br>
		<<link [[Next|Domus Street]]>><<endevent>><</link>>
		<br>
	<<elseif $hairlength gte 500 and ($rng % 4)>><<pass 5>>/*75% chance if here with long hair*/
		Stops. Stares at you for a long moment.
		<br><br>
		"Your hair," <<he>> whispers.
		<br>
		"What?"
		<br>
		"Your hair is so long and beautiful," <<he>> whispers. "Just like... Can I braid it. Please. Will you let me braid it?"
		<br>
		The <<person1>><<person>> looks at you expectantly.
		<br><br>
		<<link [[Accept|Domus Braid]]>><<set $phase to 1>><</link>>
		<br>
		<<link [[Refuse|Domus Braid]]>><<set $phase to 0>><</link>>
		<br>
	<<else>>
		<<pass 60>>
		Blushes. Looks down.
		<br>
		"Talk. What's that thing you young people are all into nowadays. That... y'know? It was on that internet thing. It was on the news that time."
		<br>
		The <<person>> continues to stare at a patch of carpet.
		<br>
		"You know, right?" <<person1>>the <<person>> looks at you.
		<br>
		You shrug.
		<br><br>
		The hour seems to take several years. Probably decades. Slow, awkward, excruciating decades,
		as the <<person>> struggles to drag the occasional, unwilling word from the <<person2>><<personstop>>
		You try to keep up the conversation, but it's painfully awkward.
		<br><br>
		<<person1>>Finally, at the end of the hour, <<he>> takes you back to the door. Shrugs.
		<br>
		"Well," <<he>> sighs, handing you £10. "Thanks for trying."
		<<set $money += 1000>>
		<br><br>
		<<link [[Next|Domus Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>