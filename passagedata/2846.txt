<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You step out from your hiding place. The <<persons>> jaw drops when <<he>> sees you.
<<if $phase is 0>>
<<exhibitionism5>>
<<else>>
<<exhibitionism2>>
<</if>>

<<if $rng gte 51>>

"Y-you aren't supposed to be here," <<he>> stammers. "Y-you need to come with me."
<br><br>

<<link [[Comply|Library Study Comply]]>><</link>>
<br>
<<link [[Refuse (0:20)|Library Study Refuse]]>><<pass 20>><</link>>
<br>

<<else>>

"What a treat," <<he>> says. <<He>> thumbs <<his>> baton as <<he>> steps closer, then lunges for you.
<br><br>

<<link [[Next|Library Exposed Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>