<<temple_effects>><<effects>>

You remove the pendant from your neck, and hand it to the <<monkstop>> <<He>> wraps <<his>> hand in several layers of cloth before taking it, and holds it away from <<his>> body.
<br><br>

"You can enter," <<he>> says. You do so.
<br><br>

The door clicks shut behind you. You're in a small hexagonal room, the edges ringed with steps leading nowhere. A single window decorates each wall, far above. A cushion sits in the middle.
<br><br>

The air is thick with the smell of a sweet incense. You can't see where its being burned.
<br><br>

<<endevent>>
<<link [[Next|Temple Prayer]]>><</link>> | <span class="blue">???</span>
<br>