A duel begins between Raul and William. Raul has the upper hand, but the moment he is about to kill William, Janet stabs him with a dagger.
<br><br>
He gets furious, strikes her back, accidentally piercing her with a sword. He is shocked realising what just happened. William uses the opportunity and finishes his opponent with his blade.
<br><br>
The history ends with both would-be lovers looking into one another's eyes for the last time, then the darkness overwhelms them.
<br><br>
You slowly close the book. You feel moved, but relaxed.
<<stress -12>><<llstress>>
<br>
It's time to get back to reality.
<br>
How long have you been here?
<br><br>
<<link [[Put the book away|School Library]]>><</link>>

/*indev*/