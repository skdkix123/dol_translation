<<set $outside to 0>><<set $location to "cabin">><<effects>>

<<if $phase is 0>>

You check out Eden's body. <<His>> muscles look very defined. <<He>> notices your gaze. "You can't avoid a body like this if you live like I do," <<he>> sounds almost ashamed.
<br><br>

<<link [[Reassure|Eden Bath 2]]>><<set $phase to 3>><<npcincr Eden love 1>><<npcincr Eden dom -1>><</link>>
<br>
<<link [[Look away and pick up the sponge|Eden Bath 2]]>><<set $phase to 1>><<npcincr Eden love 1>><</link>>
<br>
<<link [[Look away and relax|Eden Bath 2]]>><<set $phase to 2>><<stress -12>><</link>><<lstress>>
<br>

<<elseif $phase is 1>>

You pick up the sponge and wave it. Eden turns <<his>> back to you and you get to scrubbing.

<<if $exhibitionism lt 55>>
You feel more comfortable about your nudity with <<his>> back turned.
<</if>>
<br><br>

The water starts to get cold after a while. "You can get out now, I'd like to stretch out anyway." You see no reason to complain.
<br><br>

<<link [[Next|Eden Cabin]]>><<clotheson>><<endevent>><</link>>

<<elseif $phase is 2>>

You lean back and relax, letting the warmth seep into your muscles.
<br><br>

The water starts to get cold after a while. "You can get out now, I'd like to stretch out anyway." You see no reason to complain.
<br><br>

<<link [[Next|Eden Cabin]]>><<clotheson>><<endevent>><</link>>

<<elseif $phase is 3>>

	<<if $submissive gte 1150>>
	"You look amazing," you say.
	<<elseif $submissive lte 850>>
	"Don't be silly," you say. "People would kill for a body like yours."
	<<else>>
	"You look good," you say.
	<</if>>

<br><br>

<<He>> smiles, "Thanks."
<br><br>

<<link [[Wash Eden|Eden Bath 2]]>><<set $phase to 1>><<npcincr Eden love 1>><</link>>
<br>
<<link [[Relax|Eden Bath 2]]>><<set $phase to 2>><<stress -12>><</link>><<lstress>>
<br>

<</if>>