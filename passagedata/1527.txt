<<effects>>

"Keep your disgusting hands away from me," you say. "Or I'll rip them off."
<br><br>

<<He>> steps back. "I'm only having some fun," <<he>> says. "That's what you're here for, right?"
<br><br>

You stare at each other. <<He>> breaks <<his>> gaze first, and turns to find someone else to bother.
<br><br>

<<link [[Next|Brothel]]>><<endevent>><</link>>
<br>