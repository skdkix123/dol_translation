<<set $outside to 0>><<effects>>
<<if $tentacleEntrance is "mirror">><<set $location to "home">>
	You wake up on the floor next to your mirror. That was quite the nightmare.
	<br><br>
	<<tentacleworldend>>
	<<link [[Next|Bedroom]]>><</link>>
<<else>>
	<<set $location to "asylum">>
	You wake in darkness. You're back in the asylum, in the tiny room.<<endevent>><<npc Harper>><<person1>>
	<br><br>
	<<tentacleworldend>>
	<<link [[Next|Asylum Room 2]]>><</link>>
<</if>>
<br>