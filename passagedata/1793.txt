<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>
<<if $phase is 1>>
	<<if $rng gte 1>>

<<He>> pauses for a moment, considering. "Our help hasn't arrived, but you look... fit," <<he>> says. "I'm throwing a party and need someone to distribute refreshments. You'll get £20 for two hours work." <<He>> rummages in something to <<his>> left, out of your view, before producing a small waist apron. "£60, if you wear this."
<br><br>

You look more closely at the apron. It's such a slight thing it would be little protection against spillages, and would barely cover more than your crotch.
<br><br>

"It wouldn't do much to protect your other clothes, which is why you shouldn't wear any." <<He>> smiles. "Hence the extra pay. So what do you think?"
<br><br>

		<<if $exhibitionism gte 55>>
		<<link [[Agree to work in the apron|Danube Apron]]>><</link>><<exhibitionist4>>
		<br>
		<<else>>
		You aren't lewd enough to take up <<his>> scandalous offer.
		<br><br>
		<</if>>
<<link [[Agree to work in normal clothes|Danube Party]]>><</link>>
<br>
<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
<br>

	<<else>>

	"There's nothing," <<he>> says. "Now if you'll excuse me, I have a party to get back to." <<He>> shuts the door.
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<elseif $phase is 2>>
	<<if $rng gte 1>>

"You're an insolent little thing," <<he>> says. "But as it happens I do need something. There's a family heirloom in my attic I need retrieved. I can't do it myself, it's too dusty up there. It will take up to an hour and I'll give you £15 for finding it."
<br><br>

<<link [[Accept|Danube Attic]]>><</link>>
<br>
<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
<br>

	<<else>>

	"Insolent whelp," <<he>> says. "Begone."
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<elseif $phase is 3>>
	<<if $rng gte 1>>

	The <<person>> smiles. "There's nothing I need," <<he>> says. "But I'll give you £15 to join me for tea. It will take about an hour."
	<br><br>

	<<link [[Accept|Danube Tea]]>><</link>>
	<br>
	<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
	<br>

	<<else>>

	"There's nothing I need," <<he>> says, "But don't be afraid to call again, sweetie."
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<elseif $phase is 4>>
	<<if $rng gte 1>>

	<<person1>><<He>> stares down <<his>> nose at you. "There is something," <<he>> says, "We have a problem with weeds growing and strangling our other plants. We would give you £50 for four hours work."
	<br><br>

	<<link [[Enter|Danube Garden]]>><<tending 1>><</link>><<gtending>>
	<br>
	<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
	<br>

	<<else>>

	"No," <<he>> says, and slams the door shut.
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<elseif $phase is 5>>
	<<if $rng gte 1>>

	<<His>> demeanour changes. "I'm hosting a tea party and there's a spare seat," <<he>> says with enthusiasm. "I'll give you £10 if you fill in for the missing guest. It will take forty minutes."
	<br><br>

	<<link [[Accept (0:40)|Danube Doll]]>><</link>>
	<br>
	<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
	<br>

	<<else>>

	<<He>> wordlessly slams the door shut.
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<elseif $phase is 6>>
	<<if $rng gte 1>>
	<<set $rng1 to random(1, 4)>>
	<<He>> looks at you with interest. "Our <<person1>><<if $pronoun is "m">>son<<else>>daughter<</if>> is preparing for a
	<<if $rng1 is 1>>science<<elseif $rng1 is 2>>maths<<elseif $rng1 is 3>>English<<elseif $rng1 is 4>>history<</if>>
	exam. Perhaps you could tutor <<him>> for an hour? We'll pay between £10 and £100. Depends how good you are."
	<br><br>

	<<link [[Accept (1:00)|Danube Tutor]]>><</link>>
	<br>
	<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
	<br>

	<<else>>

	<<He>> shakes <<his>> head. "There's nothing we need, thanks."
	<br><br>

	<<link [[Try another house|Danube House Knock]]>><<endevent>><</link>>
	<br>
	<<link [[Stop|Danube Street]]>><<endevent>><</link>>
	<br>

	<</if>>
<<else>>

<<He>> smiles. "You look thirsty," <<he>> says. "And my chest is so heavy." <<He>> cups <<his>> $NPCList[0].breastdesc. "Take the burden off me and I'll give you £5."
<br><br>

<<if $awareness gte 200>>
<<He>> wants you to drink from <<his>> breast.
<br><br>
<<else>>
You aren't sure what <<he>> means. <<He>> recognises your confusion. "I want you to suckle from my breast," <<he>> adds.
<br><br>
<</if>>

<<if $promiscuity gte 35>>
<<link [[Accept (0:15)|Danube Breast]]>><<pass 15>><<breastfed>><</link>><<promiscuous3>><<lpurity>>
<br>
<<else>>
You are not promiscuous enough to accept <<his>> offer.
<br><br>
<</if>>

<<link [[Refuse|Danube Street]]>><<endevent>><</link>>
<br>

<</if>>