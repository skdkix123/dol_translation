<<effects>>

<<if $willpower gte random(1, $willpowermax)>>
	You continue wakling. You're nagged by the fear that you're going the wrong way. It's hard to tell with the sky obscured by the canopy. <span class="green">You remain focused on your path,</span> and soon find the haze thinning. The dizziness subsides.<<willpower 1>><<gwillpower>>
	<br><br>
	
	<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	You contine walking, but the dizziness does not subside. You're nagged by the fear that you're going the wrong way. It's hard to tell with the sky obscured by the canopy. <span class="red">You change direction,</span> hoping to escape the haze, but it doesn't help. You turn again, and again. The dizziness becomes more intense, and you find it harder to think. You're lost.<<ggwillpower>><<willpower 3>>
	<br><br>

	You keep walking, the haze becomes sweeter, so much that you can taste it in the air.
	<br><br>
	
	You come across a large plant, taller and thicker than you. It's surrounded by vines and flowers. Clouds of white dust emerge from an aperture on top.
	<br><br>
	
	<<link [[Next|Forest Pitcher]]>><</link>>
	<br>
<</if>>