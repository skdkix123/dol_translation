<<effects>>

<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte 10>>
	You push Alex's arm away from you, but <<he>> doesn't give up. <<He>> lays a hand on your thigh, and kisses your neck.
	<br><br>
	
	<<link [[Push away again|Farm Rest Push 2]]>><<npcincr Alex love -1>><<npcincr Alex lust -1>><<npcincr Alex dom -1>><</link>><<llove>><<llust>><<ldom>>
	<br>
	<<if $promiscuity gte 15>>
		<<link [[Allow|Farm Rest Allow]]>><<npcincr Alex dom 1>><<npcincr Alex love 1>><</link>><<promiscuous2>><<gdom>><<glove>>
	<</if>>
<<else>>
	You push Alex's arm away from you. <<He>> shifts away. "Sorry," <<he>> says. "I misread the situation."
	<br><br>
	
	<<farm_relax_end>>
<</if>>