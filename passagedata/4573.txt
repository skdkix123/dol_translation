<<effects>>

<<generatey1>><<person1>>
You take a deep breath, then run out from behind the tree and into the street. <<if $ex_flyover is undefined>><<exhibitionism5>><<set $ex_flyover to 1>><<else>><br><br><</if>>

You're halfway up the staircase <span class="red">when you hear a voice behind you.</span> "Nice view," <<he>> says. <<covered>> You look over your shoulder, and see a <<person>> leaning against the same tree you hid behind. <<He>> stomps on a cigarette as <<he>> walks closer, looking up at you. <<He>> holds a phone, the camera pointed right at you.
<<gstress>><<stress 6>><<garousal>><<arousal 6>>
<br><br>

<<link [[Next|Flyover Ex Naked Caught]]>><</link>>
<br>