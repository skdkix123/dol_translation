<<effects>>

"This is my <<if $pronoun is "m">>dad's<<else>>mum's<</if>> farm," <<he>> says as <<he>> leads you outside. "There are several, and this one's left up to me. It's a bit much without help. Hard enough to keep up with the animals. Some of the fields are going to waste."
<br><br>

You arrive at the chicken coop. "These girls are easy enough. They just need to be fed once a day, and let into the yard for at least an hour or so."
<br><br>

<<link [[Next|Farm Intro 4]]>><</link>>
<br>