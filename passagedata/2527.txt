<<set $outside to 0>><<schooleffects>><<effects>>

<<if $phase is 0>>
	<<if $submissive gte 1150>>
		"I-I was cursed," you say. "Please don't hate me."
	<<elseif $submissive lte 850>>
		"I was cursed by foul sorcery," you say. "Stay back, or you'll be cursed too!"
	<<else>>
		"I was cursed," you say. "By evil magic."
	<</if>>
	<br><br>
	Some of the boys step back. The <<person>> stands <<his>> ground, but <<his>> voice quivers. "C-curses aren't real," <<he>> says. "Right?" <<He>> glances around for support, but no one gives it.
	<br><br>
	<<He>> throws you your clothes, as if afraid of a disease. "You should dress like a boy if you're gonna come in here," <<he>> says. "And stay away from me."
	<br><br>
<<else>>
	<<if $submissive gte 1150>>
		"A strange experiment made me like this," you say. "Please don't hate me."
	<<elseif $submissive lte 850>>
		"I'm a scientific experiment gone right," you say.
	<<else>>
		"I'm a scientific experiment gone wrong," you say.
	<</if>>
	<br><br>
	The boys laugh. "They can't do that," the <<person>> says, looking around <<him>> for support. "Can they?" One of the boys shrugs, and the <<person>> turns back to you. <<He>> throws you your clothes. "It doesn't matter. We're not letting you in here looking like a girl. It's distracting."
	<br><br>
<</if>>

<<clotheson>>
<<endevent>>
<<link [[Next|School Pool Entrance]]>><</link>>
<br>