<<effects>>
<<npc Remy>><<person1>>

<<if $monsterchance gte 1 and $hallucinations gte 1 or $monsterchance gte 1 and $monsterhallucinations is "f">>
	Remy leads you halfway down the bar, past dozens of "cows" in their stalls.
<<else>>
	Remy leads you halfway down the barn, past dozens of cows in their stalls.
<</if>>
<<He>> stops beside one. It's empty.
<br><br>

"This time," <<he>> says, opening the gate. "You had better be a good <<girl>>. <<He>> holds your leash close to your neck, pulling you past <<him>> and into the cage. <<He>> shuts the gate behind you.
<br><br>

<<link [[Next|Livestock Cell]]>><</link>>
<br>