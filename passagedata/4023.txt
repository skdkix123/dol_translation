<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $submissive gte 1150>>
	"I'm sorry," you say. "It was the only way I could win."
<<elseif $submissive lte 850>>
	"Sorry," you say. "That was uncalled for."
<<else>>
	"Sorry," you say. "I know you wouldn't do that to me."
<</if>>
<br><br>
"It's okay," Robin says. "It just surprised me."
<br><br>
<<robinoptions>>