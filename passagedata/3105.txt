<<set $outside to 0>><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "face">>
	<<else>>
		<<shopbuy "face" "reset">>
	<</if>>
<<else>>
	<<shopbuy "face" "reset">>
<</if>>
<<clothingShop "face">>
<br>

<<link [[Back to shop|Clothing Shop]]>><<unset $clothes_choice>><</link>>