<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<beastejaculation>>

Satsfied, the wolf leaves you be. You hear its feet patter against the ground.
<br><br>

<<tearful>> you try to get your bearings.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $eventskip to 1>>
<<destinationbondage>>

<<elseif $enemyhealth lte 0>>

You hear the patter of the wolf's feet against the dirt as it flees.
<br><br>

<<tearful>> you try to get your bearings.
<br><br>

<<clotheson>>
<<endcombat>>

<<set $eventskip to 1>>
<<destinationbondage>>

<<else>>
	<<if $syndromeeden gte 1>>

		The wolf's ears prick. It backs away from you. You hear it's feet against the dirt as it makes a panicked getaway.
		<br><br>

		<<clotheson>>
		<<endcombat>>

		<<npc Eden>>
		<<person1>>

		"What do we have here?" says a familiar voice. It's Eden.

		<<blindfoldremove>>

		<<if $edenfreedom gte 2 and $edendays gte 8>>
		"I trusted you," <<he>> says. "This is what happens if you don't obey me. It's for your own protection." <<He>> pulls you to your feet. "Let's go home."
		<br><br>

		<<link [[Next (0:30)|Eden Recaptured]]>><<pass 30>><</link>>
		<br>

		<<elseif $edenfreedom is 1 and $edendays gte 2>>

		"I trusted you," <<he>> says. "This is what happens if you don't obey me. It's for your own protection." <<He>> pulls you to your feet. "Let's go home."
		<br><br>

		<<link [[Next (0:30)|Eden Recaptured]]>><<pass 30>><</link>>
		<br>

		<<elseif $edenfreedom gte 1>>

		"I knew you'd end up like this. The town is dangerous, though I didn't expect to find a wolf so close to it," <<he>> says, helping you to your feet. "Let's go home."
		<br><br>

		<<He>> takes you back to <<his>> cabin.
		<br><br>
		<<endevent>>
		<<link [[Next (0:30)|Eden Cabin]]>><<pass 30>><</link>>
		<br>
		<</if>>
	<<else>>

		"Is everyone okay over there-" you hear someone begin. They're interrupted by the growling of the wolf.
		<br><br>
		"Sh-shit!" the voice continues. "Don't eat me!" You hear the rapid falls of their feet as they run away. The wolf makes no attempt to chase, content with its current prize.
		<br><br>

		<<link [[Next|Bondage Wolf Rape]]>><</link>>
		<br>

	<</if>>

<</if>>