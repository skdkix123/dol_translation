<<set $outside to 0>><<set $location to "strip_club">><<effects>>

You grab a cloth and pretend to clean the bar, moving towards the leaning <<person1>><<personstop>> Once close, you reach over the counter and <span class="lewd">rip the <<if $pronoun is "m">>shirt<<else>>top half of their dress<</if>> right off their back.</span> It comes away easy, no doubt designed to be easily removed.
<<promiscuity2>>

The onlookers hoot and cheer as the <<person>> <<if $pronoun is "m">>pretends to try to get <<his>> top back.<<else>> covers <<his>> $NPCList[0].breastsdesc and pretends to try to get <<his>> top back.<</if>> You step back and hold it up in the air, well out of the <<persons>> reach.
<br><br>

<span class="gold">That should make them part with more cash.</span>
<br><br>

<<link [[Return to Work|Bartending VIP Strip Work]]>><<set $phase to 0>><</link>>
<br>
<<if $promiscuity gte 35>>
<<link [[Take it further|Bartending VIP Strip 2]]>><</link>><<promiscuous3>>
<br>
<</if>>