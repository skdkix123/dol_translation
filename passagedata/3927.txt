<<effects>>
You ignore the pair. They say a few more disparaging things about Kylar, but they don't hang around long.
<br><br>
<<if $phase is 0>>
You lose the game. Kylar doesn't say anything.
<br><br>
<<endevent>><<npc Kylar>><<person1>>
<<kylaroptions>>

<<else>>
You win the game, but there's no sense of achievement. Kylar doesn't say anything.
<br><br>
<<endevent>><<npc Kylar>><<person1>>
<<kylaroptions>>

<</if>>