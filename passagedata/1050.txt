<<effects>>

You walk up to the fence. The vertical gaps are far too thin to squeeze through, and it towers into the air.
<br><br>

You see movement nearby. A mouse scurries past, hiding within tufts of grass. It disappears into a small hole at the base of the fence. It appears on the other side a moment later.
<br><br>

<<link [[Next|Livestock Field]]>><</link>>
<br>