<<effects>>

A bundle falls down the stairwell.

<<if $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked")>>
	It's a checkered shirt.<<upperwear 33>>
	<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte $npcdomhigh and $NPCName[$NPCNameList.indexOf("Alex")].lust gte 10>>
		A smaller bundle followers. A pair of denim shorts.<<lowerwear 31>>
	<<else>>
		A another bundle thumps to the ground in front of you. A pair of jeans.<<lowerwear 22>>
	<</if>>
<<elseif $worn.upper.type.includes("naked")>>
	It's a checkered shirt.<<upperwear 33>>
<<else>>
	<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte $npcdomhigh and $NPCName[$NPCNameList.indexOf("Alex")].lust gte 10>>
		It's a pair of denim shorts.<<lowerwear 31>>
	<<else>>
		It's a pair of jeans.<<lowerwear 22>>
	<</if>>
<</if>>
<<if $upperwetstage gte 1 or $lowerwetstage gte 2>>
	A rolled up towel follows. "Something to dry with," Alex shouts.
	<<dry>>
<</if>>
<br><br>


You get dressed in the kitchen. You hear Alex come down the stairs. <<He>> asks if you're decent before entering the kitchen.

<<if $NPCName[$NPCNameList.indexOf("Alex")].dom gte $npcdomhigh and $NPCName[$NPCNameList.indexOf("Alex")].lust gte 10>>
	<<His>> eyes traverse your body. "A little tight, maybe," <<he>> says. "Come on. We need to get back to work."
<<else>>
	"Fits well," <<he>> says. "Come on. We need to get back to work."
<</if>>
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>