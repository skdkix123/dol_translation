<<effects>><<set $rescued += 1>><<npcincr Bailey love -1>>
You hear Bailey marching down the corridor outside, come to investigate your scream. "You better be dying in there." The <<person1>><<person>> climbs on the bed and bolts through the open window just as Bailey storms in.
<br><br>
<<clotheson>>
<<endcombat>>
<<npc Bailey>><<person1>>"I don't know what this town's coming to," Bailey says as <<he>> closes the window. <<He>> turns to you. "Don't disturb me again unless it's important." <<He>> slams the door shut as <<he>> leaves.
<br><br>
[[Next|Bedroom]]