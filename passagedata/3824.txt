<<effects>>
<<generate3>><<person3>>
"Such a fine pair of couples. I think it's time for an impromptu dance competition," the <<person2>><<person>> announces. "One couple dances. Then the other. Everyone gets a vote on the winner. How about it?"
<br><br>

The guests like the idea. So does Avery, and the other couple. Particularly one of them, a <<person3>><<personcomma>> who gives you a sly smile.
<br><br>

<<link [[Dance|Avery Party Dance Accept]]>><<npcincr Avery love 1>><<set $endear += 10>><</link>><<glove>><<gendear>>
<br>
<<link [[Refuse|Avery Party Dance Competition Refuse]]>><<set $endear -= 5>><</link>><<lendear>>
<br>