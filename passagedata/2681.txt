<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $phase is 1>>
	You raise your hand. Sirris points at you, and you give your answer. <<He>> smiles as you speak. "Well done! It's clear you've been doing your homework."
	<br><br>
<<else>>
	You keep quiet. You might be wrong anyway.
	<br><br>
<</if>>
<<link [[Next|Science Lesson]]>><<endevent>><</link>>