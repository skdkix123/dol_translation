<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>
You successfully pick the lock and enter the abode.
<<if $skulduggery lt 400>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>
<br><br>
You sneak around the mansion and look for anything of value.
<<if $rng gte 91>>
	You find little of value that could be easily carried away. While searching the basement, you come across an ancient-looking door. It is locked.
	<br><br>
	<<set $skulduggerydifficulty to 700>>
	<<link [[Try to open it|Danube House Wine Cellar]]>><</link>><<skulduggerydifficulty>>
	<br>
	<<link [[Leave|Danube Street]]>><</link>>
	<br>
<<elseif $rng gte 81>>
	<<if $bestialitydisable is "f">>
		A pair of yellow eyes pierce the darkness, and a large cat emerges. It prepares to pounce.
		<br><br>
		<<if $cat gte 6>>
			<<link [[Meow|Danube House Meow]]>><</link>> | <span class="blue">Cat</span>
			<br>
		<</if>>
		<<if $deviancy gte 15>>
			<<link [[Try to calm it|Danube House Cat]]>><<set $sexstart to 1>><<generate1>><<generate2>><</link>><<deviant2>>
			<br>
		<</if>>
		<<link [[Fight|Danube House Cat]]>><<set $molestationstart to 1>><<generate1>><<generate2>><<set $phase to 0>><</link>>
		<br>
		<<link [[Run|Danube House Run]]>><</link>><<athleticsdifficulty>>
		<br><br>
	<<else>>
		A pair of yellow eyes pierce the darkness, and a large cat emerges. It prepares to pounce.
		<br><br>
		You flee out the house, the cat following. It doesn't chase you beyond the front door, content to watch you from the door.<<beastescape>>
		<br><br>
		<<link [[Next|Danube Street]]>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 71>>
	<<generate1>><<person1>>You find a <<person>> dozing on a large sofa. <<He>> <<if $rng % 2>>is wearing expensive jewellery.<<else>>has fallen asleep holding some kind of tablet computer.<</if>>
	<br><br>
	<<set $skulduggerydifficulty to 400>>
	<<link [[Rob them|Danube House Rob]]>><</link>><<skulduggerydifficulty>><<crime>>
	<br>
	<<link [[Leave|Danube Street]]>><<endevent>><</link>>
	<br>
<<elseif $rng gte 61>>
	You find a safe with a combination lock.
	<br><br>
	<<link [[Try to open it|Danube House Safe]]>><</link>>
	<br>
	<<link [[Leave|Danube Street]]>><</link>>
	<br>
<<elseif $rng gte 51>>
	<<if $daystate is "night">>
		<<if $hour gte 21>> /* at midnight hour wraps to zero, so after midnight should no longer be true*/
			<<generate1>><<person1>>
			A huge noise is coming from the garden. As you creep toward the garden, you hear shouting, laughing and loud music. A house party.
			<br><br>
			A door smashes open and a <<person>> staggers in, vomiting in the sink. Mostly in the sink.
			<br><br>
			<<exposure>>
			<<if $exposed gte 1>>
				Before you have a chance to hide, the <<person>> turns and looks at you, steadying <<himself>> on the counter. Taking in your appearance, <<he>> smiles.
				<br>
				"Shit, you mush be the stripper!" <<He>> leers. "I didn't think we... Who called you? Well, fuck it. You're here right?"
				<br>
				You smile.
				<br>
				<<if $beauty gte ($beautymax / 7) * 4>>
					"You're a nice piece of ass." <<He>> blinks and wipes <<his>> mouth with <<his>> hand. <<He>> looks troubled for a moment. "I'm not at my best, okay? Don't judge. I've had a little too much to drink"
				<<else>>
					"You're not bad," <<he>> smiles. <<if $breastsize lte 3>>"I would've chose one with proper breasts but<<else>>"Great breasts at least, and<</if>> it was pretty last minute."
				<</if>>
				<br>
				You nod, smiling.
				<br>
				"Okay," <<he>> says. "Well the birthday <<if $player.gender_appearance is "m">>gal<<else>>boy<</if>> is out back." <<He>> pauses and steadies <<himself>> on the counter. "You wanna surprise them, so you should go out that way." <<He>> points toward a door behind you. "Wait, have you been paid yet?"
				<br><br>
				<<link [[Not yet (Take £80 and Leave)|Danube Street]]>><<set $money += 8000>><<crimeup 80>><<endevent>><</link>><<crime>>
				<br>
				<<link [[Yeah, fully paid. (Take nothing and Leave)|Danube Street]]>><<endevent>><</link>>
				<br>
			<<else>>
				Before you have a chance to hide, the <<person>> turns and looks at you as <<he>> steadies <<himself>> on the counter. <<He>> looks at the tower of pizza boxes stacked beside you, and smiles.
				<br>
				"Pizza delivery? Again? Aren't you, like, the fourth one?" <<He>> looks at the sink and laughs. "Well sure. Fuck it. I'm hungry again now."
				<br>
				<<He>> rummages through <<his>> pockets and holds out a few twenties. "There, that should cover it. And the tip too? It's pretty late."
				<br><br>
				<<link [[Take money and Leave (£60)|Danube Street]]>><<set $money += 6000>><<crimeup 60>><<endevent>><</link>><<crime>>
				<br>
				<<link [["They're Complimentary" (Leave)|Danube Street]]>><<endevent>><</link>>
				<br>
			<</if>>
		<<else>>
			<<generate1>><<person1>>
			You come across the dying throes of a house party. Beer cans, pizza boxes and semi-dressed, fully-smashed people litter the floors, tables and sofas. The air is heavy with an acrid smoke. A few people are conscious, but barely.
			Somewhere upstairs you hear what sounds like a gang-bang.<<if $awareness gte 300>> After everything you've seen, you just hope it's consensual.<</if>> Amid all the mess, one table is tidy. And full of drug paraphernalia.
			<br><br>
			Looking closer, most of the plastic bags and foil-wraps are empty, but several have drugs inside.
			<br>
			"Roll it up, slut," Someone nearby slurs. <<He>> looks wasted.
			<br><br>
			There are definitely enough drugs left over to be worth something, and no one here can ID you or report it stolen.
			<br><br>
			<<link [[Take it|Danube Street]]>><<endevent>><<set $blackmoney += 50>><</link>>
			<br>
			<<link [[Leave it|Danube Street]]>><<endevent>><</link>>
			<br>
		<</if>>
	<<else>>
		You come across the aftermath of a house party. Everyone's gone, but beer cans, pizza boxes and ash trays are everywhere. Acrid smoke still lingers in the air.
		<br><br>
		Amid all the mess, one table has been tidied up. It looks like someone collected the leftover drugs and put in all together here.
		There's enough here to be worth something - and it's not like they can report it stolen.
		<br><br>
		<<link [[Take it|Danube Street]]>><<endevent>><<set $blackmoney += 25>><</link>>
		<br>
		<<link [[Leave it|Danube Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 41>>
	<<generate1>><<person1>>You search an ornate fountain in the middle of a hall. You peer into the water looking for any valuables inside, but there's a large drain at the bottom that would've sucked any small items away. You're about to move on when you hear footsteps approaching. There's not much time to hide.
	<br><br>
	<<set $skulduggerydifficulty to 600>>
	<<link [[Hide in the fountain|Danube House Fountain]]>><</link>><<swimmingdifficulty 1 800>>
	<br>
	<<link [[Try to bluff your way out|Danube House Bluff]]>><</link>><<skulduggerydifficulty>>
	<br>
<<elseif $rng gte 31>>
	<<generate1>><<person1>>
	The mansion is silent, empty. No one appears to be home. Many doors are closed, many rooms are locked. One door is wide open and brightly lit. Approaching carefully, you peek in to see a bedroom.
	In the centre of the room is a bed. On that, completely naked, fully spread-eagled and dildo-gagged a <<person>> lies tightly bound to the bed.
	<br><br>
	Noticing you in the doorway, <<he>> suddenly looks at you.
	<<if $awareness gte 300>>
		You're not sure, but you thought you saw excitement in that first look. Now you see... apprehension? Fear? Anger?
		<br><br>
		<<link [[Remove the gag|Danube House S&M]]>><<set $phase to 0>><</link>>
		<br>
		<<link [[Leave the gag|Danube House S&M]]>><<set $phase to 1>><</link>>
		<br>
		<<link [[Leave|Danube Street]]>><<endevent>><</link>>
		<br>
	<<else>>
		You feel like crying. This town is hard - you know this already - but coming face to face with a helpless victim of kidnapping or trafficking and such fresh sexual abuse fills you with horror.
		<<gstress>><<stress 2>>
		<br><br>
		<<link [[Help this poor victim|Danube House S&M]]>><<set $phase to 2>><</link>>
		<br>
		<<link [[Leave|Danube Street]]>><<endevent>><</link>>
		<br>
	<</if>>
<<else>>
	<<set $rng to random(1, 100)>>
	<<if $rng gte 91>>
		You don't find much, just some loose change worth £2.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 200>><<crimeup 2>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 81>>
		You don't find much, just some loose change worth £4.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 400>><<crimeup 4>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 71>>
		You find £10 hidden in a drawer.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 1000>><<crimeup 10>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 61>>
		You find a nice-looking brooch in a drawer.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $blackmoney += 10>><<crimeup 10>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 51>>
		You find £20 beneath a cushion.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 2000>><<crimeup 20>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 41>>
		You find a necklace on a stool beside the coat stand.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $blackmoney += 20>><<crimeup 20>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 31>>
		You find £30 lying on the kitchen worktop.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 3000>><<crimeup 30>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 21>>
		You find a set of silverware.
		<br>
		<<link [[Take it|Danube Street]]>><<set $blackmoney += 30>><<crimeup 30>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<elseif $rng gte 11>>
		You find a pair of gold cufflinks in a coat pocket.
		<br><br>
		<<link [[Take them|Danube Street]]>><<set $blackmoney += 40>><<crimeup 40>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<<else>>
		You find £60 in a pot beside the front door.
		<br><br>
		<<link [[Take it|Danube Street]]>><<set $money += 6000>><<crimeup 60>><</link>><<crime>>
		<br>
		<<link [[Leave|Danube Street]]>><</link>>
		<br>
	<</if>>
<</if>>