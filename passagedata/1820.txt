<<effects>>

The roots lose interest in you, and disappear into the soil. <<tearful>> you untangle yourself from the plants and vines surrounding you.

<<clothesontowel>>

<<pass 2 hours>>

You continue your work. Now wary, you stay far from the denser foliage. After another two hours the <<person>> seems satisfied, and hands you £50.
<<physique 6>>
<br><br>
<<endcombat>>

<<link [[Next|Danube Street]]>><<set $money += 5000>><</link>>
<br>