<<effects>>

You wrap your arms around <<his>> shoulders.
<<if $submissive gte 1150>>
"I-it's not your fault," you say. "It's mine. Please don't be ashamed."
<<elseif $submissive lte 850>>
"It's fine," you say. "You didn't know."
<<else>>
"It's okay," you say. "It wasn't your fault."
<</if>>
<br><br>

Robin hugs you back and smiles. "Thank you," <<he>> says. "I won't be that stupid again."
<br><br>

<<robinoptions>>