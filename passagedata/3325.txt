<<set $outside to 1>><<effects>>
<<if $athletics gte random(1, 1000)>>
	You run, leaping over <<if $bus is "park">>bins and through bushes<<else>>bins and parked cars<</if>>. You hear the pair pursue, <span class="green">but their footsteps soon fade.</span>
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<<else>>
	You run, <span class="red">but you're not fast enough.</span> The pair catch up and tackle you to the ground.
	<br><br>
	<<link [[Next|Street Fame Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<</if>>