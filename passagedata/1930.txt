<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>

The <<person1>><<person>> leads you to the second floor and into a small, empty room. A tin of paint sits on a step ladder, beside a collection of brushes and a roller.
<br><br>

"That's everything you need," <<he>> says. "We'll be downstairs."
<br><br>

<<pass 2 hours>>You get to work painting the room.

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>

You finish painting one of the walls and try to stand back to admire your handiwork, but feel a force pull on your thigh. Looking down you see your $worn.lower.name stuck to the wall. You try to prise yourself free, but it's really stuck there.
<br><br>

<<link [[Be more forceful|Domus Painting Force]]>><</link>>
<br>

<<else>>

<<pass 2 hours>><<pass 2 hours>>After some arduous hours, you stand back to admire your finished handiwork.
<br><br>

You've earned £50.
<<set $money += 5000>>
<br><br>

<<link [[Next|Domus Street]]>><<endevent>><</link>>
<br><br>

<</if>>